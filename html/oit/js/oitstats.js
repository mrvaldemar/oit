function loadingScreen(hiddenElement,watchElement){
	hiddenEl=typeof hiddenElement=="string"?document.querySelector(hiddenElement):hiddenElement;
	if(hiddenEl){
		var loading=document.querySelector('#loading');
		if(!loading){			
			createTag(hiddenEl.parentNode, 'img', {src:'/oit/img/loading.gif', id:'loading', height:'291'});
			simulate(top.document.querySelector('body'),'resize');
		}
		display=hiddenEl.style.display;
		if(hiddenEl.querySelector(watchElement)&&loading){
			loading.parentNode.removeChild(loading);
			hiddenEl.style.display="block";
			simulate(top.document.querySelector('body'),'resize');
		}else{
			setTimeout(function(){loadingScreen(hiddenEl,watchElement)},500);
		}
	}else{
		setTimeout(function(){loadingScreen(hiddenElement,watchElement)},500);
	}
}

// console.log(document.querySelector('body'));
loadingScreen(".h5p-content","#resultado");

function sendData(data,url) {
	var XHR = new XMLHttpRequest();
	var urlEncodedData = "";
	var urlEncodedDataPairs = [];
	var name;
	for(name in data) {
		urlEncodedDataPairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
	}
	urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');
	XHR.addEventListener('load', function(event) {
		// console.log('toobien');
	});
	XHR.addEventListener('error', function(event) {
		// console.log('touel');
	});
	XHR.open('POST', url);
	XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	XHR.send(urlEncodedData);
}

function mostrarRespuesta(puntaje,contenedor,maxScore,personaje){
	var parent=document.getElementById('resultado');
	// var resultado=xapidata['statement']['result']['score'];
	console.log(puntaje);
	var aprobo=0.6<=puntaje.raw/puntaje.max;

	killAllChildren(parent);
	var mensajefeedback={};
	if (aprobo){
		parent.className+=" verde";
		mensajefeedback.h2="¡Muy bien!";
		mensajefeedback.p="Ha hecho un buen trabajo.";
		let imagen=createTag(parent, 'img',{className:"hidden-sm-down",src:"/oit/img/footer/"+personaje+"/footer_bien_per.png"});
	}else{
		parent.className+=" rojo";
		mensajefeedback.h2="¡Lo sentimos!";
		mensajefeedback.p="Su puntaje ha sido muy bajo.";
		let imagen=createTag(parent, 'img',{className:"hidden-sm-down",src:"/oit/img/footer/"+personaje+"/footer_mal_per.png"});
	}
	let div=createTag(parent, 'div');

	var feedback=createTag(div, 'div',{className:'oit-feedback'});

	var mensaje=createTag(feedback, 'div',{className:'oit-mensaje'});
	var feedbackh1=createTag(feedback, 'h1',{innerText:puntaje.raw+"/"+puntaje.max});
	createTag(feedback, 'p',{innerText:"respuestas correctas"});

	var mensajeh2=createTag(mensaje, 'h2',{innerText:mensajefeedback.h2});
	var mensajep=createTag(mensaje, 'p',{innerText:mensajefeedback.p});

	var innerText='Volver a lecciones';
	var url="/course/view.php?id="+contenedor.dataset.courseId;
	var botonVolver= createTag(div, 'a', 
		{className:'h5p-joubelui-button oit-volver',
		innerText:innerText,
		onclick:function(){
			window.top.location=url;
		},
		href:"javascript:void(0)"});

}

function crearCajaResultados(contenedor,disabled,personaje){
	let parent=createTag(contenedor, 'div',{id:"resultado"});
	//let imagen=createTag(parent, 'img',{src:"/oit/img/retroalimentacion/per_neutro"+personaje+".png"});;
	let imagen=createTag(parent, 'img',{className:"hidden-sm-down",src:"/oit/img/footer/"+personaje+"/footer_menu_per.png"});;
	let div=createTag(parent, 'div');

	let texto="Para que se active el botón de guardar es necesario que haya enviado las respuestas de todas las actividades de la lección.";
	let feedback=createTag(div, 'p',{className:"oit-feedback",innerHTML:texto});
	className=disabled?"disabled":"";
	var result=createTag(div, 'a',{id:"evaluar",className:"h5p-joubelui-button "+className,innerText:"Guardar",href:"javascript:void(0)"});
	let iframe=top.document.getElementsByTagName('iframe')[0];
	if(iframe){
		// let height = +iframe.style.height.replace( "px", '');
		// iframe.style="height:"+(height+319)+"px;"
		iframeLoaded(iframe);
	}
	return result;
}

function getXAPIData(instance,puntaje){
	if(typeof instance.getXAPIData!=='undefined') return instance.getXAPIData();

	let event = instance.createXAPIEventTemplate('answered');
	event.setScoredResult(puntaje.raw,puntaje.max, instance);
	return event.data;
}

var habilitarGuardado=function(){
		let boton=document.getElementById('evaluar');
	let numeroChecks=document.querySelectorAll('.h5p-joubelui-button[title="Submit"], .h5p-question-check-answer,.h5p-check-button').length;
	console.log(numeroChecks);
	if(numeroChecks<1){
		boton.className=boton.className.replace(' disabled','');
	}
}

var fcn=function(){	
	if(typeof H5P =="undefined"){
		window.setTimeout(fcn,500);

		return;
	}

	let instance=H5P.instances[0];
	let hvpcontent=document.getElementsByClassName('h5p-content')[0];
	let openedTime=+new Date();
	let disabled=false;
	let maxScore=10;
	if(instance==null){
		window.setTimeout(fcn,500);
		return;
	}
	switch(instance.constructor.name){
		case "C": 								//FlashCards
		iframe=top.document.getElementsByTagName('iframe')[0];
		break;
		case "ImageMultipleHotspotQuestion": 	//Hotspots
		break;
		case "ImagePair": 						//Emparejar
		var machete=true;
		iframe=top.document.getElementsByTagName('iframe')[0];
		break;
		default: 								//El resto
		disabled=true;
		iframe=top.document.getElementsByTagName('iframe')[0];
		break;
	}

	let url="/mod/hvp/ajax.php";
	let clasePersonaje=document.getElementsByClassName('competencias')[0];
	if(clasePersonaje!=null){
		clases=clasePersonaje.className.split('personaje-');
		personaje=clases!==undefined?clases[1][0]:1;
	}
	crearCajaResultados(hvpcontent,disabled,personaje).onclick=function(event){
		let puntaje ={}
		if(machete){
			instance.displayResult();
			puntaje=JSON.parse(document.querySelector('div.footer-container > div.feedback-container > div.feedback-text').innerText);
		}else{
			puntaje={raw:instance.getScore(),max:instance.getMaxScore()};
		}

		let xapidata=getXAPIData(instance,puntaje);

		if(!event.target.className.includes('disabled')){
			H5P.setFinished(instance.contentId,puntaje.raw,puntaje.max,openedTime,1);
			let data = {
				contextId:+location.search.split('=')[1],
				action:"xapiresult",
				tipo: 'guardar',
				skipToken: 1,
				oitRequest: 1,
				xAPIResult: JSON.stringify(xapidata)
			};
			sendData(data,url);
			mostrarRespuesta(puntaje,typeof iframe!=='undefined'?iframe:hvpcontent,maxScore,personaje);
		}
	}

	if(disabled){
		let checks=document.querySelectorAll('.h5p-joubelui-button[title="Submit"], .h5p-question-check-answer,.h5p-check-button');
		for (let i = 0; i < checks.length; i++) {
			(function(el){
				el.onclick=function(){
					// let xapidata=instance.getXAPIData();
					window.setTimeout(habilitarGuardado,200);
				}	
			})(checks[i]);
		}
	}
	// let iframes=document.getElementsByTagName('iframe');
	// for (var i = iframes.length - 1; i >= 0; i--) {
	// 	(function(el){
	// 		el.onload=iframeLoaded(el);
	// 	})(iframes[i]);
	// }
}
function resize(){
	simulate(top.document.querySelector('body'),'resize');
}
window.setTimeout(fcn,4000);
window.setTimeout(resize,4500);
