var setBootstrapTooltips=()=>{
	if(window.jQuery&&(typeof $().emulateTransitionEnd == 'function')){
		$(document).ready(()=>{$('[data-toggle=\"tooltip\"]').tooltip();});
	}else{
		setTimeout(setBootstrapTooltips(),500);
	}
}
setBootstrapTooltips();