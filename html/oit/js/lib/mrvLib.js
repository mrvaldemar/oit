/**
 * Elimina todos los hijos del elemento
 *
 * @param      {HTMLNode}  node
 */
function killAllChildren(node){
 	while(node.firstChild) { 
 		node.removeChild(node.firstChild);
 	}
 }

function setBootstrapTooltips(options){
 	if(window.jQuery&&(typeof $().emulateTransitionEnd == 'function')){
 		$(document).ready(function(){
 			$('[data-toggle="tooltip"]').tooltip(options);
 		});
 	}else{
 		setTimeout(function(){setBootstrapTooltips(options)},1000);
 	}
 }
function setFloatingElement(element,marginTop,width){
 	(function(element,marginTop,width){
 		window.onscroll=function(){
 			var el=typeof element=="string"?document.getElementById(element):element;
	  // console.log(typeof element);
	  var top=el.getBoundingClientRect().top;
	  var marginTop=72;
	  var dy=top-el.parentNode.getBoundingClientRect().top;
	  top=top<marginTop?marginTop:top;
	  if(top<=marginTop){
	  	if(dy>=0){
	  		el.style.top=top+'px';
	  		el.style.width=width;
	  		el.style.position='fixed';  
	  	}else{
	  		el.style.position='static';
	  	}
	  }else{
	  	el.style.position='static';
	  }
	}
})(element,marginTop,width);
}

function setToolTip(parentSelector,tooltipSelector){
	var parent=document.querySelector(parentSelector);
	var tooltip=document.querySelector(tooltipSelector);
	(function(){
		parent.addEventListener("mouseenter",function(event){   
			var rect=parent.parentNode.getBoundingClientRect();
			document.onmousemove =function(e){
				var x = 18+e.clientX-rect.left;
				var y = 11+e.clientY-rect.top;
				tooltip.style.top=y+"px";
				tooltip.style.left=x+"px";
			}
		}, false);

		parent.addEventListener("mouseleave",function(event){ 
			document.onmousemove =function(e){};
		});
	})(parent,tooltip);
}
/**
 * Crea un elemento HTML como hijo de parent y devuelve el Elemento creado.
 *
 * @param      {HTMLNode}  parent       Elemento padre
 * @param      {String}  tag            Tag del elemento a crear
 * @param      {Object}  attributes     Atributos del elemento a crear
 * @return     {HTMLNode}               Elemento creado
 */
function createTag(parent, tag, attributes){
 	var el = document.createElement(tag);

 	if (!('assign' in Object)) {
 		if(typeof attributes =="object"){
 			let keys=Object.keys(attributes);

 			for (var j = 0; j < keys.length; j++) {
 				el[keys[j]]=attributes[keys[j]];
 			}
 		}
 	}else{
 		Object.assign(el,attributes);
 	}

 	parent.appendChild(el);
 	return el;
 }

function iframeLoaded(iframe) {
 	if(iframe) {
 		width = iframe.parentNode.offsetWidth + "px";
 		iframe.style = "display:block; width:"+width+";";
 		height = iframe.contentWindow.document.body.scrollHeight + "px";
 		iframe.style = "display:block; width:"+width+"; height:"+height+";";
 	}
 }
/**
 * Extiende las propiedades de un objeto a otro
 *
 * @param      {Object}  destination  Destino
 * @param      {Object}  source       Objeto fuente
 * @return     {Object}  Objeto extendido
 */
function extend(destination, source) {
 	for (var property in source)
 		destination[property] = source[property];
 	return destination;
 }

/**
 * Simula un evento de tipo HTMLEvents o MouseEvents y devuelve el elemento despues de aplicarsele el evento
 *
 * @param      {<type>}  element    Elemento al que aplicar evento
 * @param      {string}  eventName  Nombre del evento puede ser
 *                                      HTMLEvents
 *                                          load
 *                                          unload
 *                                          abort
 *                                          error
 *                                          select
 *                                          change
 *                                          submit
 *                                          reset
 *                                          focus
 *                                          blur
 *                                          resize
 *                                          scroll
 *                                      MouseEvents
 *                                          click
 *                                          dblclick
 *                                          mouse
 *                                              down
 *                                              up
 *                                              over
 *                                              move
 *                                              out
 * @return     {<type>}             Elemento despues del evento
 */
function simulate(element, eventName){

 	var eventMatchers = {
 		'HTMLEvents': /^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$/,
 		'MouseEvents': /^(?:click|dblclick|mouse(?:down|up|over|move|out))$/
 	}
 	var defaultOptions = {
 		pointerX: 0,
 		pointerY: 0,
 		button: 0,
 		ctrlKey: false,
 		altKey: false,
 		shiftKey: false,
 		metaKey: false,
 		bubbles: true,
 		cancelable: true
 	}

 	var options = extend(defaultOptions, arguments[2] || {});
 	var oEvent, eventType = null;

 	for (var name in eventMatchers){
 		if (eventMatchers[name].test(eventName)) { eventType = name; break; }
 	}

 	if (!eventType)
 		throw new SyntaxError('Evento desconocido');

 	if (document.createEvent){
 		oEvent = document.createEvent(eventType);
 		if (eventType == 'HTMLEvents'){
 			oEvent.initEvent(eventName, options.bubbles, options.cancelable);
 		}else{
 			oEvent.initMouseEvent(eventName, options.bubbles, options.cancelable, document.defaultView,
 				options.button, options.pointerX, options.pointerY, options.pointerX, options.pointerY,
 				options.ctrlKey, options.altKey, options.shiftKey, options.metaKey, options.button, element);
 		}
 		element.dispatchEvent(oEvent);
 	}else{
 		options.clientX = options.pointerX;
 		options.clientY = options.pointerY;
 		var evt = document.createEventObject();
 		oEvent = extend(evt, options);
 		element.fireEvent('on' + eventName, oEvent);
 	}
 	return element;
 }

 if (!('remove' in Element.prototype)) {
 	Element.prototype.remove =function() {
 		if (this.parentNode) {
 			this.parentNode.removeChild(this);
 		}
 	};
 }

// if (!('assign' in Object)) {
//   Object.assign=function(target,...sources){
//     var obj=target;
//     for (var i =0; i < sources.length; i++) {
//       if(typeof sources[i] =="object"){
//         let keys=Object.keys(sources[i]);
//         for (var j = 0; j < keys.length; j++) {
//           obj[keys[j]]=sources[i][keys[j]];
//         }
//       }
//     }
//     return obj
//   }
// }