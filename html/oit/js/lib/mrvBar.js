ProgressBar=function(id){
	this.el=document.getElementById(id);
}

ProgressBar.prototype.render = function() {

	killAllChildren(this.el);
	
	var style={
		height:this.size.height,
		width:this.size.width,
		border:this.border,
		overflow:'hidden',
		position:'relative',
		borderRadius:this.borderRadius
	}

	Object.assign(this.el.style,style);
	
	for (var i =0 ; i<this.data.length ; i++) {
		let obj=createTag(this.el,'div');

		style={	
			display:"inline-block",
			height:"100%",
			backgroundColor:this.color[i]
			// ,borderRadius:"0 "+this.borderRadius+" "+this.borderRadius+" 0"
		}

		let width=(this.el.offsetWidth)*(this.data[i])/this.total;
		if(i!==0){
			width+=(this.el.offsetWidth)*(this.data[i-1])/this.total;
		}
		style.zIndex=this.data.length-i;
		style.width=width+"px";
		style.position="absolute";
		style.left=0;
		Object.assign(obj.style,style);
	}
};