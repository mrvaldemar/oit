<?php
require "../config.php";
require_once($CFG->dirroot."/oit/lib/rankingrenderer.php");

$PAGE->set_cacheable(false);

//Obtener parametros necesarios
$courseid = required_param('id', PARAM_INT);
$filtroDepartamento = required_param('departamento', PARAM_TEXT);

//Obtener nombre del curso actual
$coursename=$DB->get_record('course',array('id'=>$courseid),'fullname')->fullname;

//Configuracion de titulos y url de la pagina 
$PAGE->set_url('/oit/paneldetalle.php');
$titulo="$coursename";
$PAGE->set_title($titulo);
$PAGE->set_heading($titulo);

//Obtener parametros GET
$PAGE->navbar->add('Panel', new moodle_url('/oit/panel.php'));
$PAGE->navbar->add('Panel', new moodle_url('/oit/panel.php'));

$a=new RankingRenderer();


echo html_writer::tag('style',
	"div#nav-drawer{display:none!important;}
	#page.container-fluid{margin-left: 112px!important;}");
echo $OUTPUT->header();
$a->out($courseid);

echo $OUTPUT->footer();