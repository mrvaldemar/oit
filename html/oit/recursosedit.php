<?php
require "../config.php";
require_once($CFG->dirroot."/oit/lib/rankingrenderer.php");

$nombreArchivo = required_param('nombre', PARAM_TEXT);
// $PAGE->set_cacheable(false);

$mform = new recurso_item_form(new moodle_url('/oit/recursosedit.php',array('nombre'=>$nombreArchivo)));
if($mform->is_cancelled()) {
	header('Location: /oit/recursos.php');
	die;
}

if($dbdata=$DB->get_record('recurso',array('nombre'=>$nombreArchivo))){
	$tags=$DB->get_records('recurso_curso',array('id_recurso'=>$dbdata->id),'id_curso');
	$dbdata->tags=[];
	foreach ($tags as $tag) {
		$dbdata->tags[]=$tag->id_curso;
	}
	// var_dump($dbdata);
	$mform->set_data($dbdata);
}

$mensaje='';
if ($data = $mform->get_data()){
	unset($data->submitbutton);
	$data->nombre=$nombreArchivo;
	$guardo=false;
	$tags=$data->tags;
	unset($data->tags);
	if($DB->record_exists('recurso',array('nombre'=>$data->nombre))){
		$data->id=$DB->get_record('recurso',array('nombre'=>$data->nombre),'id')->id;
		$guardo=$DB->update_record('recurso',$data);
		
		$DB->delete_records('recurso_curso',['id_recurso'=>$data->id]);

		foreach ($tags as $tag) {
			$DB->insert_record('recurso_curso',['id_curso'=>$tag,'id_recurso'=>$data->id]);
		}
	}else{
		$guardo=$DB->insert_record('recurso',$data);
		
		foreach ($tags as $tag) {
			$DB->insert_record('recurso_curso',['id_curso'=>$tag,'id_recurso'=>$guardo]);
		}

	}
	var_dump($guardo);
	if($guardo){
		header('Location: /oit/recursos.php',true,301);
		die;
	}else{
		$mensaje=html_writer::tag('p',"Ocurrio un problema en la base de datos, vuelva a intentarlo");
	}
}

$title="Recursos";
$PAGE->set_url('/oit/recursos.php');
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo file_get_contents("$CFG->dirroot/oit/plantillas/primer_nivel.html");
echo $mensaje;
$mform->display();

echo $OUTPUT->footer();