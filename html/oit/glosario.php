<?php

require "../config.php";
require_once($CFG->dirroot."/oit/lib/utils.php");
require_once($CFG->dirroot."/oit/lib/forms.php");

//Se obtienen los parametros necesarios
$actualizar = optional_param('update',0, PARAM_INT);
$eliminar = optional_param('erase',0, PARAM_INT);
$agregar = optional_param('add',0, PARAM_INT);
$editar = optional_param('edit',0, PARAM_INT);
$filtro = optional_param('filtro','Todos', PARAM_TEXT);
$confirmarEliminar = optional_param('confirmerase',0, PARAM_INT);

$PAGE->set_cacheable(false);

//Configuracion de titulos y url de la pagina 
require_login();
$title="Glosario";
$PAGE->set_url('/oit/glosario.php');
$PAGE->set_title($title);
$PAGE->set_heading($title);

//Se instancia el formulario del glosario
$mform=new glosario_form($CFG->wwwroot.$_SERVER['REQUEST_URI']);

//Si el formulario es cancelado volver a la pagina base
if($mform->is_cancelled()) {
	header('Location: /oit/glosario.php',true,301);
	die;
}

//Si se requiere editar una entrada setear los campos del formulario con los datos de la entrada
if($editar){
	$mform->set_data($DB->get_record('glosario',array('id'=>$editar)));
}

//Si llega datos del formulario
if ($data = $mform->get_data()){
	unset($data->submitbutton);
	$guardo=false;

	//Si la palabra no existe ingresarla como nueva
	if(!$DB->record_exists('glosario',array('palabra'=>$data->palabra))){
		$guardo=$DB->insert_record('glosario',$data);

	//Si no y si se quiere editar actualizar la entrada
	}elseif($editar){
		$data->id=$editar;
		$guardo=$DB->update_record('glosario',$data);
	}

	//Si se guardo exitosamente volver a la pagina base, si no mostrar un error
	if($guardo){
		$params=$filtro?"?filtro=$filtro":'';
		header("Location: /oit/glosario.php$params",true,301);
		die;
	}else{
		$mensaje=html_writer::tag('p',"Ocurrio un problema en la base de datos, vuelva a intentarlo");
	}
}

//Si se quiere eliminar borrar el registro
if($confirmarEliminar){
	$DB->delete_records('glosario',array('id'=>$confirmarEliminar));
	$params=$filtro?"?filtro=$filtro":'';
	header("Location: /oit/glosario.php$params",true,301);
	die;
}


//Cargar las plantillas utilizadas
$plantilla=file_get_contents("$CFG->dirroot/oit/plantillas/glosario/item.html");
$plantillaEdit=file_get_contents("$CFG->dirroot/oit/plantillas/glosario/editar.html");
$plantillaFiltro=file_get_contents("$CFG->dirroot/oit/plantillas/glosario/itemfiltro.html");

//Traer los registros del glosario segun el filtro que se tenga
if($filtro!='Todos'){
	$glosario=$DB->get_records_sql("SELECT * FROM {glosario} WHERE palabra LIKE '$filtro%' ORDER BY palabra");
}else{
	$glosario=$DB->get_records('glosario',null,'palabra');
}

$letras=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','Ñ','O','P','Q','R','S','T','U','V','W','X','Y','Z','Todos');

//Renderizar cabezal con estilos de primer nivel
echo $OUTPUT->header();
echo file_get_contents("$CFG->dirroot/oit/plantillas/primer_nivel.html");

//Si se requiere agregar o editar mostrar formulario
if($agregar||$editar){
	$mform->display();
}elseif($eliminar){
	echo OITUtils::plantillarender(
		file_get_contents("$CFG->dirroot/oit/plantillas/glosario/confirmar.html"),
		array('id'=>$eliminar,'filtro'=>$filtro)
	);
}else{	
	//Si se es administrador de sitio mostar boton de nueva entrada
	echo is_siteadmin()?html_writer::tag('a','Nueva Entrada',array('href'=>'/oit/glosario.php?add=1','class'=>'btn btn-primary')):'';

	//Mostrar filtro de letras
	echo html_writer::start_tag('ul',array('class'=>'pagination paginacion-glosario'));
	foreach ($letras as $letra) {
		echo OITUtils::plantillarender($plantillaFiltro,array('letra'=>$letra,'class'=>$letra===$filtro?'active':''));
	}
	echo html_writer::end_tag('ul');

	//Renderizar tabla con palabras si hay resultados, si no mostrar mensaje
	echo html_writer::start_tag('table',array('id'=>'tabla_glosario','class'=>'generaltable flexible'));
	echo html_writer::start_tag('tbody');
	if($glosario){
		foreach ($glosario as $palabra) {
			$palabra->editar=is_siteadmin()?OITUtils::plantillarender($plantillaEdit,array('id'=>$palabra->id,'filtro'=>$filtro)):'';
			$palabra->anchor=OITUtils::normalize($palabra->palabra);
			echo OITUtils::plantillarender($plantilla,(array)$palabra);
		}
	}else{
		echo html_writer::tag('p','No hay nada que mostrar');
	}
	echo html_writer::end_tag('tbody');
	echo html_writer::end_tag('table');
}

//Renderizar footer
echo $OUTPUT->footer();