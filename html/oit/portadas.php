<?php

require "../config.php";
require_once($CFG->dirroot."/oit/lib/utils.php");
require_once($CFG->dirroot."/oit/lib/forms.php");

require_login();

$curso = optional_param('curso','', PARAM_TEXT);

$title="Administración portadas";
$PAGE->set_url('/oit/portadas.php');
$PAGE->set_title($title);
$PAGE->set_heading($title);


//Si llega un request de archivo guardarlo en la carpeta
if ($_FILES['archivo']&&($curso!=='')){
	$target = "$CFG->dirroot/oit/portadas/$curso.jpg";
	var_dump(file_exists($target),is_writeable($target));
	if(file_exists($target)&&is_writeable($target)){
		unlink($target);
	}
	if(move_uploaded_file($_FILES['archivo']['tmp_name'], $target)){
		header('Location: /course/index.php',true,301);
	}
}

$cursos=$DB->get_records('course',array('format'=>'topics'),'sortorder ASC','id,shortname');
$opciones='';
foreach ($cursos as $curso) {
	$opciones.=html_writer::tag('option',$curso->shortname,array('value'=>OITUtils::normalize($curso->shortname)));
}

echo $OUTPUT->header();
echo file_get_contents("$CFG->dirroot/oit/plantillas/primer_nivel.html");

//Si es administrador mostrar formulario de carga de archivo
if(is_siteadmin()){
	echo OITUtils::plantillarender(
		file_get_contents("$CFG->dirroot/oit/plantillas/portadas/formulario_subida.html"),
		array('opciones'=>$opciones)
	);
}




//Renderizar footer

echo $OUTPUT->footer();
