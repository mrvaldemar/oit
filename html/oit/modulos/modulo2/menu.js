(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"menu_atlas_", frames: [[0,0,501,101],[0,103,501,101]]}
];


// symbols:



(lib.logo_ministerio = function() {
	this.spriteSheet = ss["menu_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.logo_ministerio_1 = function() {
	this.spriteSheet = ss["menu_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.an_Video = function(options) {
	this._element = new $.an.Video(options);
	this._el = this._element.create();
	var $this = this;
	this.addEventListener('added', function() {
		$this._lastAddedFrame = $this.parent.currentFrame;
		$this._element.attach($('#dom_overlay_container'));
	});
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,400,300);

p._tick = _tick;
p._handleDrawEnd = _handleDrawEnd;
p._updateVisibility = _updateVisibility;



(lib.logooit = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.instance = new lib.logo_ministerio();
	this.instance.parent = this;
	this.instance.setTransform(-84,-64,0.579,0.579);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#373435").ss(1,0,0,22.9).p("AlfAAIK/AA");
	this.shape.setTransform(-136.4,-36.7,0.888,0.888);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#373435").s().p("AgSAWQgIgHABgOQAAgNAHgJQAHgIAMgBQALAAAHAIQAIAHAAAPQgBAMgGAJQgHAKgNgBQgMABgGgJgAgMgOQgEAHAAAIQAAAJAEAGQAFAGAHAAQAKAAADgHQAEgHAAgIQAAgIgDgFQgEgIgKAAQgHAAgFAHg");
	this.shape_1.setTransform(-117.1,-4.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#373435").s().p("AgGAzIgCAAIAAgJIAIgBQAAgBAAgFIAAg/IAJAAIAABAQAAAGgCAEQgDAFgIAAgAAAgmIAAgMIAJAAIAAAMg");
	this.shape_2.setTransform(-121.8,-4.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#373435").s().p("AgVAZQgFgEAAgIQAAgIAFgEQAFgDAIgBIAOgCQAEgBAAgCIABgEQAAgFgDgCQgEgCgFAAQgIAAgDAEQgCACAAAFIgJAAQAAgLAGgEQAIgEAIAAQAJAAAHAEQAFADAAAJIAAAfIABADIADABIABAAIACgBIAAAIIgDAAIgEAAQgFAAgCgDIgCgFQgDAEgFACQgEADgHAAQgIAAgFgFgAAHABIgGABIgFABIgHACQgFADAAAFQAAAEADADQADACAEAAQAFAAAEgCQAIgEAAgJIAAgHIgEABg");
	this.shape_3.setTransform(-125.4,-4.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#373435").s().p("AgJAlQgDgCgDgEIAAAHIgJAAIAAhNIAJAAIAAAcQADgEAFgCQAEgCAEAAQALAAAHAHQAGAHAAAOQAAANgGAJQgHAJgLAAQgGAAgEgDgAgKgGQgFAFAAAKQAAAIACAFQAEAKAJAAQAIAAAEgHQAEgGAAgKQAAgJgEgFQgEgGgIAAQgFAAgFAFg");
	this.shape_4.setTransform(-131.5,-5.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#373435").s().p("AgVAZQgFgEAAgIQAAgIAFgEQAFgDAIgBIAOgCQADgBACgCIAAgEQAAgFgDgCQgEgCgFAAQgIAAgDAEQgBACgBAFIgJAAQAAgLAHgEQAGgEAJAAQAJAAAGAEQAHADgBAJIAAAfIABADIACABIACAAIACgBIAAAIIgEAAIgDAAQgEAAgDgDIgCgFQgCAEgGACQgEADgHAAQgIAAgFgFgAAGABIgFABIgFABIgIACQgEADAAAFQAAAEADADQADACAEAAQAFAAAEgCQAIgEAAgJIAAgHIgFABg");
	this.shape_5.setTransform(-137.5,-4.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#373435").s().p("AgNAdIAAg4IAJAAIAAAKQABgDAEgEQAEgEAGAAIABAAIACAAIAAAKIgCAAIgBAAQgIAAgDAFQgDAEAAAGIAAAgg");
	this.shape_6.setTransform(-142.1,-4.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#373435").s().p("AgFAnIAAhEIgaAAIAAgJIA/AAIAAAJIgbAAIAABEg");
	this.shape_7.setTransform(-147.1,-5.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#373435").s().p("AgEAnIAAhNIAJAAIAABNg");
	this.shape_8.setTransform(-154.5,-5.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#373435").s().p("AgSAXQgHgIAAgOQAAgNAHgJQAIgIALAAQAGAAAGADQAFADADAEQADAEABAGIABAMIgpAAQABAIADAGQAEAFAHAAQAIAAAEgFQADgDABgEIAJAAIgCAHIgFAHQgEAEgHABIgHABQgKAAgIgHgAAQgEQgBgGgCgEQgEgHgJAAQgFAAgFAFQgEAFAAAHIAeAAIAAAAg");
	this.shape_9.setTransform(-158.8,-4.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#373435").s().p("AgRAgQgHgIAAgNQAAgMAGgJQAGgJAMAAQAGAAAEADIAGAFIAAgcIAJAAIAABNIgIAAIAAgIQgEAGgEACQgFACgFAAQgJAAgHgIgAgKgFQgEAFAAAKQAAAKAEAGQAEAGAHAAQAHAAAEgGQAEgGAAgKQAAgKgEgFQgEgGgHAAQgGAAgFAGg");
	this.shape_10.setTransform(-165,-5.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#373435").s().p("AgEAnIAAhNIAJAAIAABNg");
	this.shape_11.setTransform(-106.8,-16.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#373435").s().p("AgVAZQgFgEAAgIQAAgIAFgEQAFgDAIgBIAOgCQAEgBAAgCIABgEQAAgFgDgCQgEgCgFAAQgHAAgEAEQgCACAAAFIgJAAQAAgLAGgEQAIgEAIAAQAJAAAHAEQAFADABAJIAAAfIAAADIADABIABAAIACgBIAAAIIgDAAIgDAAQgGAAgCgDIgBgFQgDAEgGACQgFADgGAAQgIAAgFgFgAAGABIgFABIgFABIgHACQgFADAAAFQAAAEADADQADACAEAAQAFAAAEgCQAIgEAAgJIAAgHIgFABg");
	this.shape_12.setTransform(-110.9,-15.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#373435").s().p("AAOAdIAAgjQAAgFgCgDQgDgGgGAAIgFABQgEABgDAEQgCADgBADIgBAIIAAAdIgJAAIAAg4IAJAAIAAAIQAEgFAEgCQAFgCAFAAQAMAAAEAIQACAFAAAJIAAAjg");
	this.shape_13.setTransform(-117.1,-15.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#373435").s().p("AgTAXQgGgIAAgOQgBgNAIgIQAHgJAMAAQAKgBAIAIQAIAIAAAOQAAANgHAIQgHAJgNAAQgLAAgIgHgAgMgOQgDAGAAAJQAAAJADAGQAFAHAHAAQAJAAAEgIQADgHAAgIQAAgIgCgFQgEgIgKAAQgHAAgFAHg");
	this.shape_14.setTransform(-123.2,-15.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#373435").s().p("AgEAnIAAg4IAJAAIAAA4gAgEgbIAAgLIAJAAIAAALg");
	this.shape_15.setTransform(-127.3,-16.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#373435").s().p("AgRAWQgGgIgBgMQAAgOAIgJQAIgIAJAAQAKAAAGAFQAHAEABAMIgKAAQAAgFgEgEQgCgDgIAAQgHAAgEAJQgDAFAAAIQAAAIADAGQAEAGAHAAQAGAAADgDQAEgEABgGIAKAAQgCALgHAFQgGAFgKAAQgKAAgHgIg");
	this.shape_16.setTransform(-131.2,-15.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#373435").s().p("AgVAZQgFgEAAgIQAAgIAFgEQAFgDAIgBIAOgCQADgBACgCIAAgEQAAgFgDgCQgEgCgFAAQgHAAgEAEQgCACAAAFIgJAAQAAgLAGgEQAIgEAIAAQAJAAAGAEQAGADABAJIAAAfIAAADIACABIACAAIACgBIAAAIIgEAAIgCAAQgFAAgDgDIgBgFQgEAEgFACQgFADgGAAQgIAAgFgFgAAGABIgFABIgFABIgIACQgEADAAAFQAAAEADADQADACAEAAQAFAAAEgCQAIgEAAgJIAAgHIgFABg");
	this.shape_17.setTransform(-136.9,-15.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#373435").s().p("AAOAdIAAgjQAAgFgCgDQgDgGgGAAIgFABQgEABgDAEQgCADgBADIgBAIIAAAdIgJAAIAAg4IAJAAIAAAIQAEgFAEgCQAFgCAFAAQAMAAAEAIQACAFAAAJIAAAjg");
	this.shape_18.setTransform(-143.1,-15.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#373435").s().p("AgNAdIAAg4IAJAAIAAAKQABgDAEgEQAEgEAGAAIABAAIACAAIAAAKIgCAAIgBAAQgIAAgDAFQgDAEAAAGIAAAgg");
	this.shape_19.setTransform(-147.6,-15.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#373435").s().p("AgSAXQgHgIAAgOQAAgNAHgJQAIgIALAAQAGAAAGADQAFADADAEQADAEABAGIABAMIgpAAQABAIADAGQAEAFAHAAQAIAAAEgFQADgDABgEIAJAAIgCAHIgFAHQgEAEgHABIgHABQgKAAgIgHgAAQgEQgBgGgCgEQgEgHgJAAQgFAAgFAFQgEAFAAAHIAeAAIAAAAg");
	this.shape_20.setTransform(-152.7,-15.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#373435").s().p("AgCAhQgCgDgBgGIAAglIgIAAIAAgHIAIAAIAAgQIAJAAIAAAQIAJAAIAAAHIgJAAIAAAlQAAABAAAAQAAABABAAQAAABAAAAQABABAAAAIAEABIABAAIACAAIAAAHIgDABIgEAAQgGAAgCgEg");
	this.shape_21.setTransform(-157.3,-16);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#373435").s().p("AAOAdIAAgjQAAgFgCgDQgDgGgGAAIgFABQgEABgDAEQgCADgBADIgBAIIAAAdIgJAAIAAg4IAJAAIAAAIQAEgFAEgCQAFgCAFAAQAMAAAEAIQACAFAAAJIAAAjg");
	this.shape_22.setTransform(-161.8,-15.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#373435").s().p("AgFAnIAAhNIAKAAIAABNg");
	this.shape_23.setTransform(-166.2,-16.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#373435").s().p("AAOAdIAAgjQAAgFgCgDQgDgGgGAAIgFABQgEABgDAEQgCADgBADIgBAIIAAAdIgJAAIAAg4IAJAAIAAAIQAEgFAEgCQAFgCAFAAQAMAAAEAIQACAFAAAJIAAAjg");
	this.shape_24.setTransform(-106.8,-26.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#373435").s().p("AgSAhQgIgIAAgNQABgOAHgIQAHgJALAAQALAAAIAIQAHAHAAANQAAAOgGAJQgHAJgNAAQgLAAgHgIgAgLgDQgFAFAAAKQAAAIAFAGQADAHAIAAQAJAAAEgIQADgGABgKQgBgHgCgEQgEgJgKAAQgIAAgDAIgAgJgZIAJgPIALAAIgMAPg");
	this.shape_25.setTransform(-112.9,-27.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#373435").s().p("AgEAnIAAg4IAJAAIAAA4gAgEgbIAAgLIAJAAIAAALg");
	this.shape_26.setTransform(-117,-27.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#373435").s().p("AgQAWQgIgIABgMQAAgOAHgJQAIgIAKAAQAJAAAHAFQAFAEABAMIgJAAQAAgFgDgEQgEgDgGAAQgIAAgFAJQgCAFAAAIQAAAIADAGQAEAGAHAAQAGAAAEgDQADgEABgGIAJAAQgBALgHAFQgGAFgKAAQgKAAgGgIg");
	this.shape_27.setTransform(-120.9,-26.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#373435").s().p("AgVAZQgFgEAAgIQAAgIAFgEQAFgDAIgBIAOgCQADgBABgCIABgEQAAgFgDgCQgEgCgFAAQgHAAgEAEQgBACgBAFIgJAAQAAgLAHgEQAGgEAJAAQAJAAAGAEQAHADgBAJIAAAfIABADIACABIACAAIACgBIAAAIIgEAAIgDAAQgEAAgDgDIgCgFQgDAEgFACQgFADgGAAQgIAAgFgFgAAHABIgGABIgFABIgIACQgEADAAAFQAAAEADADQADACAEAAQAFAAAEgCQAIgEAAgJIAAgHIgEABg");
	this.shape_28.setTransform(-126.6,-26.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#373435").s().p("AgXAcIAAgHIAhgoIgeAAIAAgIIArAAIAAAHIghAoIAiAAIAAAIg");
	this.shape_29.setTransform(-132.5,-26.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#373435").s().p("AgEAnIAAg4IAJAAIAAA4gAgEgbIAAgLIAJAAIAAALg");
	this.shape_30.setTransform(-136.4,-27.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#373435").s().p("AAOAdIAAgjQAAgFgCgDQgDgGgGAAIgFABQgEABgDAEQgCADgBADIgBAIIAAAdIgJAAIAAg4IAJAAIAAAIQAEgFAEgCQAFgCAFAAQAMAAAEAIQACAFAAAJIAAAjg");
	this.shape_31.setTransform(-140.6,-26.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#373435").s().p("AgVAZQgFgEAAgIQAAgIAFgEQAFgDAIgBIAOgCQAEgBAAgCIABgEQAAgFgDgCQgEgCgFAAQgIAAgDAEQgCACAAAFIgJAAQAAgLAGgEQAIgEAIAAQAJAAAHAEQAFADAAAJIAAAfIABADIADABIABAAIACgBIAAAIIgDAAIgEAAQgFAAgCgDIgCgFQgDAEgFACQgEADgHAAQgIAAgFgFgAAHABIgGABIgFABIgHACQgFADAAAFQAAAEADADQADACAEAAQAFAAAEgCQAIgEAAgJIAAgHIgEABg");
	this.shape_32.setTransform(-146.5,-26.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#373435").s().p("AgPAlQgGgEgBgJIAKAAQAAAEACACQADADAHAAQAKAAAEgHQACgFAAgMQgDAFgEABQgEADgGAAQgJAAgHgHQgHgGAAgOQAAgPAHgIQAHgIAKAAQAGAAAFADIAFAFIAAgHIAJAAIAAAzQAAALgDAGQgGAMgQAAQgJAAgGgEgAgMgWQgCAFAAAIQAAAJAEAFQAEAFAGAAQAKAAAEgJQACgFAAgHQAAgLgEgEQgFgGgGAAQgJAAgEAKg");
	this.shape_33.setTransform(-152.9,-25);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#373435").s().p("AgNAdIAAg4IAJAAIAAAKQABgDAEgEQAEgEAGAAIABAAIACAAIAAAKIgCAAIgBAAQgIAAgDAFQgDAEAAAGIAAAgg");
	this.shape_34.setTransform(-157.2,-26.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#373435").s().p("AgcAeQgJgMAAgSQAAgOAIgMQAKgOATAAQAVAAAKANQAHAKAAAQQAAARgJALQgKAOgTABQgSgBgKgLgAgTgWQgHAHAAAQQAAANAGAJQAHAJANAAQAPAAAGgKQAHgLgBgMQABgNgIgIQgHgJgNAAQgLAAgIAJg");
	this.shape_35.setTransform(-163.5,-27.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#8CA2CD").s().p("AgFgDIAAgDIALANg");
	this.shape_36.setTransform(-115.9,-89.6,0.888,0.888);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#245B9F").s().p("AgDAGQgYgNgPgQQAKACAUACQARACAJAHQAKAGAHAKQAJAOADAEQgUgDgagPg");
	this.shape_37.setTransform(-121.9,-93.9,0.888,0.888);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#245B9F").s().p("AgdAFQAIgLAJgGQAIgFASgDIAdgEQgdAig4APIANgUg");
	this.shape_38.setTransform(-150.6,-93.7,0.888,0.888);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#245B9F").s().p("AgfgIQAKgXAngTQgXAqgIAPQAQgJANgFIAhgJQgRAYgaAPIg1AcQAJguAHgNg");
	this.shape_39.setTransform(-155,-90.4,0.888,0.888);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#245B9F").s().p("AgDAYQgSgLgcgcQAUACANAFQANAFAPAKIANALIgNgOIgcg3QAEAEARALQAPAJAJANQAIAMAEATQAFAaAEAJQgpgUgMgIg");
	this.shape_40.setTransform(-117.5,-90.6,0.888,0.888);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#245B9F").s().p("AgFAfQgSgPgTghQALAGAZALQATAJAHAQQgGgPgGgfQgFgegGgMQAHAEANAQQAMAPAGAMQAJAQgCAXIgFApIgqghg");
	this.shape_41.setTransform(-113.2,-85.3,0.888,0.888);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#245B9F").s().p("AgpAYQgDgXAIgQQAEgJAPgSQAOgSAGgFQgFAQgFAZIgJApQASgRAIgDQAIgFAZgKQghAxgMAJQgcATgIAHQABgJgEghg");
	this.shape_42.setTransform(-159.4,-85,0.888,0.888);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#245B9F").s().p("AgThKQgBARADAYQAFAeAAAKIAZgbQARgTAOgKQgPA1gXAaQgTAVgDAYQgzhXAwg+g");
	this.shape_43.setTransform(-162.4,-77,0.888,0.888);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#245B9F").s().p("AAOA/QgDgKgFgIQgFgHgGgGIgLgMQgHgIgIgWIgMgkQAMAHAQASQARASALAHQAAgJACgMIADgUQACgKAAgKIgBgTQAfBAgHAbIgMAaQgIASgEASQgDgEgCgKg");
	this.shape_44.setTransform(-110.1,-77.5,0.888,0.888);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#245B9F").s().p("AAPA6QgSgHgQgRQgXgYgHg3IANARQAGAKAKAHQALAHAIAHIANANQgEgVAEgZQAEgZALgSQACAPAHATQAEASgCAUIgCAnQAAALANAZQgLgKgXgGg");
	this.shape_45.setTransform(-162.4,-60.9,0.888,0.888);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#245B9F").s().p("AgqA9QAEgHACgHIgCg0QgBgqAPgaQAVAygKApQAKgLAOgLQALgJAbgjQgEAwgYAhQgKANgYAJQgbALgJAIIAHgNg");
	this.shape_46.setTransform(-110.2,-61.1,0.888,0.888);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#245B9F").s().p("AgZAQQgQgkAAg1IAcAoQANAWAAAXQAWgiATgyQADBGgFAOQgHAVguApQADgZgOghg");
	this.shape_47.setTransform(-109,-69.1,0.888,0.888);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#245B9F").s().p("AghASQgIgRgBgcQgCgfAIgOIAHAWQADAMAIANQARAaACAPQACgXALgYQAMgZARgRIgCAoQgCAUgIATIgNAhQgEAMACAXQgpgmgIgSg");
	this.shape_48.setTransform(-163.8,-68.7,0.888,0.888);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#245B9F").s().p("AhWA8QAfACALgEQAKgEAGgPQAehIAbgmQAAArgDALQgEAPgTAdIBUgkIgbAiQgPATgWANQgVANggABIgHABQgeAAgTgMg");
	this.shape_49.setTransform(-117.1,-54.9,0.888,0.888);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#245B9F").s().p("AgUA6QgZgNgNgRIgdgjIADgEQABAAATAMIAgAKQARAHANAKQgSgQgFgaQgCgIgCgvQAdAmAbBFQAGAOAKAEQALADAigBQgTAMgjAAQgiAAgUgMg");
	this.shape_50.setTransform(-155.3,-54.6,0.888,0.888);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#245B9F").s().p("AgTBGQAFABABgRIAAgrIABgpQAAgbgDgNQgZAEgDAPQgFAUgFADQgEgDAAgEIgCgOIABgfIB0AAQAGAogKAMIgLgaQgFgOgWgBIgBB4QABATAEAAQAHAAADAKIg7ABQAEgMAGABg");
	this.shape_51.setTransform(-127.9,-72.6,0.888,0.888);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#245B9F").s().p("AgiBrQACgIADgBQAIgBAHgEIACimQAAgSgDgDQgGgCgEgFIgIgIIAkgBIAZAAQACAAAFAFQgFAIgGABQgGAAAAABQgDAGAABYIgBAwQAAAoAEAFQAFAJAGgDQACgCAEALQgKADgZABIgFAAQgXAAgGgEg");
	this.shape_52.setTransform(-136.2,-72.8,0.888,0.888);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#245B9F").s().p("AgTBTQgagJgNgdQgOggALgsQAIghAZgOQAXgNAYAJQAbAJAMAcQAOAggKAsQgIAigZAOQgOAIgPAAQgIAAgLgEgAgNhHQgLAFgGAcQgGAbACAcQAFBMArgUQAKgFAHgcQAGgbgBgdQgDg8gcAAQgHAAgLAFg");
	this.shape_53.setTransform(-145,-72.7,0.888,0.888);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#245B9F").s().p("AhFAeQAGgOAVgOIAhgVQgdgEgaASQgiAWgRAEQgbAHghgLQgjgLgOgVQAcALAZgCQATAAAjgKQAlgLAWAAQAPAAArAEQANABAZgGQAcgEAtAMQArALANABQAYACAigKQgRAVgeAKQgfALgbgGQgUgEgggVQgZgRggACIAiAWQAVAOAEAOQgOACgTgXQgUgWgQAAQgPgBgUAYQgRAUgOAAIgEAAg");
	this.shape_54.setTransform(-136.4,-46.8,0.888,0.888);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#245B9F").s().p("AAMAnQgXgBgDACIgKAUIgggCQgBgUgDgFQgCgFgYgEIgSARQgFABgTgNIAEgbQgXgOgCABQgOAJgGABIgVgWIAgglQAKAEAlAlQAvAhA9AAQAyABAmgTQAhgRAmgnIAjAfIgXAbIgXgLIgVANIAEAbQgeAQgGgJQgHgMgMADQgTAEACARQABAOgkABg");
	this.shape_55.setTransform(-136.3,-56.9,0.888,0.888);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#245B9F").s().p("AhmCDIAWgLIgDgcQgLgBgDgCQgGgBgDgGQgDgEABgJQAAgKACgEQAEgGANgBQAJAAAEgdIgVgOIALgcIAbAAIALgWIgOgSIARgbIAbALIASgOIgEgUQgCgIAHgIQAOgNALAJIAKAJQAHAFANgJQAIgFAAgHQgCgKADgJIAggEIAMArIg7AXQgrATghAwQgeAqgCAvQgBAbANBDIgwALg");
	this.shape_56.setTransform(-148,-79.2,0.888,0.888);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#245B9F").s().p("AAwCXQAAgKAJgiQAGgZgEgeQgMhVhEgtQgVgOgfgMIgkgOIAMguIAjAJIgBAYIAbALQAQgLADgEIAZANIgGAeQAPAQAGgBIAYgIQARASgBAGQgBAFgMAOQAIAUAGACQAGABATAAIAKAcIgUANQADAeARAAQANgBgDAjIgVAIQgEACgCAYQASAKACAIQADAIgNATg");
	this.shape_57.setTransform(-124.6,-79.4,0.888,0.888);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AgHAIIADgPIAMAAIgDAPg");
	this.shape_58.setTransform(232.7,184.9);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AATAwIANg8QADgMgFgHQgEgGgNAAQgFAAgEACIgIADIgHAGIgFAHIgEAJIgCAJIgLAxIgKAAIAUhcIAKAAIgCANQAIgHAHgFQAHgEAKAAQAJAAAGADQAHADAEAHQACAFgBAGIgBALIgNA8g");
	this.shape_59.setTransform(225.7,180.9);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AgSBBIAThdIAKAAIgSBdgAAEgwIAEgQIALAAIgDAQg");
	this.shape_60.setTransform(219.5,179.2);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AAtAwIAOhAIABgJQAAgDgCgDQgCgDgDgBQgEgCgFAAQgGAAgHADQgGACgFAGIgEAFIgDAGIgDAHIgBAHIgKAxIgLAAIANhAIABgJQAAgDgCgDQgBgDgDgBQgEgCgFAAQgGAAgGADQgHACgFAGIgEAFIgDAGIgDAHIgBAHIgLAxIgLAAIAUhcIALAAIgDAOIAAAAQAGgJAJgEQAIgEAKAAQAGAAADACIAHADIAEAGIACAHQAHgJAIgFQAJgEAKAAQAKAAAFADQAFAEACAFQACAFAAAGIgCAOIgNA6g");
	this.shape_61.setTransform(209.3,180.9);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AghA8QgHgGgDgJQgDgJABgLQABgMADgNQACgLAEgMQAEgLAHgJQAGgKAKgFQAJgGANAAQAKAAAGADQAGADAEAFQAEAFABAGQACAHAAAHIgBAPIgDAOIgHAYQgEAMgHAJQgGAIgKAGQgJAFgMAAQgNAAgIgFgAgEgxQgHAFgFAHQgFAIgEALQgDAKgCAJQgDAMAAAKQgBAKACAHQACAHAGAEQAFAEAJAAQAKAAAGgFQAIgFAFgIQAFgIADgKIAFgTQACgJABgKQABgKgCgIQgCgIgFgEQgFgFgKAAQgKAAgGAFg");
	this.shape_62.setTransform(192.4,179.4);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgcA/QgHgCgFgFQgEgEgCgIQgCgHACgLIALAAQgBAHABAFQABAGADADQAEAEAFACQAGACAHAAQAGAAAGgCQAGgDAGgEQAFgEADgFQADgFABgGQACgHgCgFQgDgGgFgDQgFgDgGAAQgIgBgIAAIADgIIAOgBQAGgBAGgDQAGgDADgFQAEgEACgIQAAgGAAgEQgCgEgDgDQgDgCgFgCIgKgBQgGAAgGACQgEACgFAEQgEADgDAFQgDAFgBAGIgMAAQADgIAEgHQAEgHAIgGQAHgGAHgBQAHgCAJAAQAHAAAGACQAGACAFAEQAEAEABAGQACAGgCAIQgCAKgGAHQgGAHgKAEQAGACAEADQADADACAEQACAFAAAEIgBAJQgCAIgEAHQgEAHgHAFQgFAFgJADQgIAEgJAAQgLAAgIgCg");
	this.shape_63.setTransform(182.2,179.4);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AgPAsIAEgQIALAAIgCAQgAAAgbIADgQIANAAIgDAQg");
	this.shape_64.setTransform(169.6,181.2);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgZAuQgHgDgFgGQgFgGgBgJQgCgKADgMQACgJAFgJQAEgJAHgHQAHgGAJgEQAIgEALAAQALAAAHAEQAHAEAEAGQAEAHABAJQABAJgDAJQgCALgFAJQgFAJgHAHQgHAGgJAEQgIADgJAAQgJAAgHgDgAgGgjQgGADgFAGQgFAFgEAHIgEAOQgCAJAAAHQABAHADAGQADAFAFADQAFADAIAAQAHAAAHgEQAHgDAFgGQAFgFADgHQADgHACgIQACgHgBgHQAAgIgCgFQgDgFgFgDQgGgDgIAAQgJAAgGADg");
	this.shape_65.setTransform(157.1,181);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AglA/QgHgFgDgGQgEgHAAgIQgBgKACgKQACgKAFgHQAEgKAHgGQAGgHAJgEQAJgEAJgBQAKABAIAEQAJAEADAJIALgzIALAAIgcCBIgLAAIAEgRIgBAAQgDAFgFAEQgEADgFACIgJAEIgJABQgLAAgIgDgAgNgSQgHADgFAGQgGAGgDAGQgDAIgCAJQgCAJABAHQABAGAEAFQADAFAFACQAGACAGAAQAIAAAGgDQAGgDAFgFQAFgFAEgIQADgHACgIQACgJgBgGQgBgHgCgFQgDgFgFgDQgGgEgHAAQgHABgHADg");
	this.shape_66.setTransform(147.2,179.3);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AgTBBIAUhdIAKAAIgSBdgAAEgwIADgQIANAAIgEAQg");
	this.shape_67.setTransform(139.6,179.2);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AgeAwIAUhcIAKAAIgDARQAGgLAIgFQAIgEAMAAIgDAMQgIAAgHADQgGADgGAFQgDAFgDAIQgDAHgCAGIgJAug");
	this.shape_68.setTransform(134.7,180.9);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AgbAuQgIgEgDgHQgEgGgBgJQAAgJACgKQACgKAFgJQAFgJAHgHQAHgGAIgEQAIgEAKAAQATAAAIANQAJANgGAZIhHAAQgCAHABAHQAAAHADAFQADAFAFADQAGADAHAAQAKAAAJgGQAIgGAFgKIAMAAIgIAMQgEAGgGAEQgGAFgHACQgGACgJAAQgLAAgHgDgAgDgkQgGADgFAFIgJAKQgDAGgCAHIA8AAQABgHgBgGQAAgGgDgFQgDgEgFgDQgFgCgHAAQgHAAgFACg");
	this.shape_69.setTransform(126.3,181);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AgfAuQgGgDgCgFQgDgEgBgHQAAgGACgIIANg8IAKAAIgNA8QgCAMAEAHQAEAGANAAQAHAAAFgCQAFgDAFgEQAFgFADgHQADgGACgIIALgyIAKAAIgTBdIgLAAIADgOIgGAGIgIAEIgIAFQgFABgGAAQgJAAgGgCg");
	this.shape_70.setTransform(116.5,181.1);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AALBCIAKgzQgHAIgKAFQgJAEgKAAQgKAAgHgEQgHgEgEgHQgEgGAAgJQgBgIACgKQACgKAFgJQAEgJAHgHQAGgGAJgEQAIgEALAAIAJABQAEABAEADIAHAGQADADABAFIABAAIADgRIALAAIgcCBgAgJg0QgHADgFAGQgGAGgDAIQgDAHgCAJQgCAJABAGQABAGAEAFQADAFAFACQAGADAGAAQAIAAAFgEQAHgDAFgFQAFgFAEgGQADgIACgIQACgIgBgHQgBgHgCgGQgDgFgFgDQgGgDgHAAQgHAAgHADg");
	this.shape_71.setTransform(106,182.7);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AgbAuQgIgEgDgHQgEgGgBgJQAAgJACgKQACgKAFgJQAFgJAHgHQAHgGAIgEQAIgEAKAAQATAAAIANQAJANgGAZIhHAAQgCAHABAHQAAAHADAFQADAFAFADQAGADAHAAQAKAAAJgGQAIgGAFgKIAMAAIgIAMQgEAGgGAEQgGAFgHACQgGACgJAAQgLAAgHgDgAgDgkQgGADgFAFIgJAKQgDAGgCAHIA8AAQABgHgBgGQAAgGgDgFQgDgEgFgDQgFgCgHAAQgHAAgFACg");
	this.shape_72.setTransform(95.3,181);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AgeAwIAUhcIAKAAIgDARQAGgLAIgFQAIgEAMAAIgDAMQgIAAgHADQgGADgGAFQgDAFgDAIQgDAHgBAGIgKAug");
	this.shape_73.setTransform(87.8,180.9);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("AgZAuQgHgDgFgGQgFgGgBgJQgCgKADgMQACgJAFgJQAEgJAHgHQAHgGAJgEQAIgEALAAQALAAAHAEQAHAEAEAGQAEAHABAJQABAJgDAJQgCALgFAJQgFAJgHAHQgHAGgJAEQgIADgJAAQgJAAgHgDgAgGgjQgGADgFAGQgFAFgEAHIgEAOQgCAJAAAHQABAHADAGQADAFAFADQAFADAIAAQAHAAAHgEQAHgDAFgGQAFgFADgHQADgHACgIQACgHgBgHQAAgIgCgFQgDgFgFgDQgGgDgIAAQgJAAgGADg");
	this.shape_74.setTransform(74.3,181);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AglA/QgHgFgDgGQgEgHAAgIQgBgKACgKQACgKAFgHQAEgKAHgGQAGgHAJgEQAJgEAJgBQAKABAIAEQAJAEADAJIALgzIALAAIgcCBIgLAAIAEgRIgBAAQgDAFgFAEQgEADgFACIgJAEIgJABQgLAAgIgDgAgNgSQgHADgFAGQgGAGgDAGQgDAIgCAJQgCAJABAHQABAGAEAFQADAFAFACQAGACAGAAQAIAAAGgDQAGgDAFgFQAFgFAEgIQADgHACgIQACgJgBgGQgBgHgCgFQgDgFgFgDQgGgEgHAAQgHABgHADg");
	this.shape_75.setTransform(64.5,179.3);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AgcAwQgFgCgEgDQgDgEgCgFQgBgFABgHQAFgWAigFIAOgCIAIgBIAHgCQADgBABgDIACgFIAAgIQAAgDgCgDQgDgCgEgCQgEgBgGAAQgLAAgIAEQgIAFgEAMIgLAAQADgJAEgFQAEgGAGgEQAGgEAGgBQAHgCAIAAQAHAAAGACQAGABAEADQADAEACAFQABAGgBAIIgLAyQgCAGACACQACABAIgCIgCAIIgEABIgFABIgFAAQgFgBgBgEIABgJQgJAHgJAEQgJAEgKAAQgGAAgGgBgAAOACIgOABQgMABgIAFQgIAEgCAKQgBAEABADQABADACADIAGADIAHABQAHAAAGgCQAGgCAFgDQAGgDAEgFQADgGACgHIADgNQgHACgHABg");
	this.shape_76.setTransform(53,181);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AAtAwIAOhAIABgJQAAgDgCgDQgCgDgDgBQgEgCgFAAQgGAAgHADQgGACgFAGIgEAFIgDAGIgDAHIgBAHIgKAxIgLAAIANhAIABgJQAAgDgCgDQgBgDgDgBQgEgCgFAAQgGAAgGADQgHACgFAGIgEAFIgDAGIgDAHIgBAHIgLAxIgLAAIAUhcIALAAIgDAOIAAAAQAGgJAJgEQAIgEAKAAQAGAAADACIAHADIAEAGIACAHQAHgJAIgFQAJgEAKAAQAKAAAFADQAFAEACAFQACAFAAAGIgCAOIgNA6g");
	this.shape_77.setTransform(40.7,180.9);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("AgSBBIAThdIAKAAIgTBdgAAEgwIAEgQIALAAIgDAQg");
	this.shape_78.setTransform(31.9,179.2);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFFFFF").s().p("AATAvIgTgpIglApIgNAAIAugwIgXgtIANAAIARAmIAigmIAOAAIgrAtIAYAwg");
	this.shape_79.setTransform(24.9,181);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFFFFF").s().p("AgZAuQgHgDgFgGQgFgGgBgJQgCgKADgMQACgJAFgJQAEgJAHgHQAHgGAJgEQAIgEALAAQALAAAHAEQAHAEAEAGQAEAHABAJQABAJgDAJQgCALgFAJQgFAJgHAHQgHAGgJAEQgIADgJAAQgJAAgHgDgAgGgjQgGADgFAGQgFAFgEAHIgEAOQgCAJAAAHQABAHADAGQADAFAFADQAFADAIAAQAHAAAHgEQAHgDAFgGQAFgFADgHQADgHACgIQACgHgBgHQAAgIgCgFQgDgFgFgDQgGgDgIAAQgJAAgGADg");
	this.shape_80.setTransform(15.5,181);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("AgeAwIAUhcIAKAAIgDARQAGgLAIgFQAIgEAMAAIgCAMQgJAAgHADQgGADgFAFQgEAFgDAIQgDAHgCAGIgJAug");
	this.shape_81.setTransform(8,180.9);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("Ag0BCIAciBIALAAIgEARIAIgIIAJgGIAJgEIAKgBQALAAAHAEQAIAEADAGQAEAHAAAJQABAJgCAKQgDAKgEAIQgEAJgHAGQgGAHgJAEQgJAEgKAAQgJAAgJgEQgIgFgDgIIgLAzgAABg0QgFADgFAFQgFAFgEAIQgEAHgBAJQgCAHABAIQAAAGADAFQADAFAFADQAGAEAHAAQAHAAAHgEQAHgDAFgGQAFgGAEgGQADgIACgJQADgRgHgKQgHgJgNAAQgIAAgHADg");
	this.shape_82.setTransform(-1.5,182.7);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AgcAwQgFgCgEgDQgDgEgCgFQgBgFABgHQAFgWAigFIAOgCIAIgBIAHgCQADgBABgDIACgFIAAgIQAAgDgCgDQgDgCgEgCQgEgBgGAAQgLAAgIAEQgIAFgEAMIgLAAQADgJAEgFQAEgGAGgEQAGgEAGgBQAHgCAIAAQAHAAAGACQAGABAEADQADAEACAFQABAGgBAIIgLAyQgCAGACACQACABAIgCIgCAIIgEABIgFABIgFAAQgFgBgBgEIABgJQgJAHgJAEQgJAEgKAAQgGAAgGgBgAAOACIgOABQgMABgIAFQgIAEgCAKQgBAEABADQABADACADIAGADIAHABQAHAAAGgCQAGgCAFgDQAGgDAEgFQADgGACgHIADgNQgHACgHABg");
	this.shape_83.setTransform(-11.8,181);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFFFFF").s().p("AgZAuQgHgDgFgGQgFgGgBgJQgCgKADgMQACgJAFgJQAEgJAHgHQAHgGAJgEQAIgEALAAQALAAAHAEQAHAEAEAGQAEAHABAJQABAJgDAJQgCALgFAJQgFAJgHAHQgHAGgJAEQgIADgJAAQgJAAgHgDgAgGgjQgGADgFAGQgFAFgEAHIgEAOQgCAJAAAHQABAHADAGQADAFAFADQAFADAIAAQAHAAAHgEQAHgDAFgGQAFgFADgHQADgHACgIQACgHgBgHQAAgIgCgFQgDgFgFgDQgGgDgIAAQgJAAgGADg");
	this.shape_84.setTransform(-26.4,181);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFFFFF").s().p("Ag0BCIAciBIALAAIgEARIAIgIIAJgGIAJgEIAKgBQALAAAHAEQAIAEADAGQAEAHAAAJQABAJgCAKQgDAKgEAIQgEAJgHAGQgGAHgJAEQgJAEgKAAQgJAAgJgEQgIgFgDgIIgLAzgAABg0QgFADgFAFQgFAFgEAIQgEAHgBAJQgCAHABAIQAAAGADAFQADAFAFADQAGAEAHAAQAHAAAHgEQAHgDAFgGQAFgGAEgGQADgIACgJQADgRgHgKQgHgJgNAAQgIAAgHADg");
	this.shape_85.setTransform(-37.4,182.7);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFFFFF").s().p("AAtAwIAOhAIABgJQAAgDgCgDQgCgDgDgBQgEgCgFAAQgGAAgHADQgGACgFAGIgEAFIgDAGIgDAHIgBAHIgKAxIgLAAIANhAIABgJQAAgDgCgDQgBgDgDgBQgEgCgFAAQgGAAgGADQgHACgFAGIgEAFIgDAGIgDAHIgBAHIgLAxIgLAAIAUhcIALAAIgDAOIAAAAQAGgJAJgEQAIgEAKAAQAGAAADACIAHADIAEAGIACAHQAHgJAIgFQAJgEAKAAQAKAAAFADQAFAEACAFQACAFAAAGIgCAOIgNA6g");
	this.shape_86.setTransform(-50.1,180.9);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFFFFF").s().p("AgbAuQgIgEgDgHQgEgGgBgJQAAgJACgKQACgKAFgJQAFgJAHgHQAHgGAIgEQAIgEAKAAQATAAAIANQAJANgGAZIhHAAQgCAHABAHQAAAHADAFQADAFAFADQAGADAHAAQAKAAAJgGQAIgGAFgKIAMAAIgIAMQgEAGgGAEQgGAFgHACQgGACgJAAQgLAAgHgDgAgDgkQgGADgFAFIgJAKQgDAGgCAHIA8AAQABgHgBgGQAAgGgDgFQgDgEgFgDQgFgCgHAAQgHAAgFACg");
	this.shape_87.setTransform(-62.3,181);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFFFFF").s().p("AgTBBIAUhdIAKAAIgSBdgAAEgwIADgQIANAAIgEAQg");
	this.shape_88.setTransform(-68.8,179.2);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFFFFF").s().p("AggBBIAah2IgpAAIACgLIBdAAIgCALIgpAAIgZB2g");
	this.shape_89.setTransform(-74.6,179.2);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f().s("#54BB6F").ss(2,1,1).p("AwUAAMAgpAAA");
	this.shape_90.setTransform(-0.6,80.4);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#54BB6F").s().p("Ag2BCQgVgXAAgrQAAgrAYgZQAUgWAgABQArgBAUAdQALAQABARIgkAAQgEgNgFgGQgKgMgSAAQgTAAgLAQQgLAQAAAcQgBAdAMAPQAMAOASAAQASAAAKgNQAFgGAEgNIAkAAQgGAcgSASQgUARgdAAQgkAAgVgYg");
	this.shape_91.setTransform(96.6,55.8);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#54BB6F").s().p("AgQBWIg7irIAnAAIAkCCIAmiCIAmAAIg7Crg");
	this.shape_92.setTransform(80,55.7);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#54BB6F").s().p("AgRBWIAAirIAjAAIAACrg");
	this.shape_93.setTransform(68.5,55.7);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#54BB6F").s().p("AgrA0QgTgQAAgjQAAggARgSQASgSAbAAQARAAANAGQANAGAJANQAIALACAPQACAJgBAQIhcAAQABATANAIQAHAFAKAAQAMAAAHgGQAEgEADgFIAiAAQgCALgLAMQgRATgfAAQgYAAgUgQgAAdgMQgBgOgIgGQgIgHgMAAQgMAAgHAHQgHAHgCANIA5AAIAAAAg");
	this.shape_94.setTransform(52.1,58);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#54BB6F").s().p("AguBGQgPgSAAgcQAAggAPgTQAPgTAZAAQALAAAJAGQAJAFAFAJIAAg9IAiAAIAACrIggAAIAAgRQgIALgJAFQgJAFgMAAQgXAAgPgSgAgUgFQgHAKAAAQQAAASAHALQAHAKANAAQAOAAAIgKQAHgLAAgRQAAgWgMgKQgHgGgKAAQgNAAgHALg");
	this.shape_95.setTransform(37.6,55.9);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#54BB6F").s().p("AgwAwQgQgUAAgcQAAgbAQgUQAQgUAgAAQAhAAAQAUQAQAUAAAbQAAAcgQAUQgQAUghAAQggAAgQgUgAgWgdQgHALAAASQAAATAHALQAIAKAOAAQAPAAAHgKQAIgLAAgTQAAgSgIgLQgHgKgPAAQgOAAgIAKg");
	this.shape_96.setTransform(16.6,57.9);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#54BB6F").s().p("AgrAxQgPgSAAgdQAAggAQgSQAQgTAcAAQAYAAAPALQAPALADAbIgiAAQgBgHgEgGQgGgHgMAAQgQAAgGAQQgDAJAAAOQAAAOADAJQAGAQAQAAQAMAAAFgHQAEgGACgKIAiAAQgCAPgJAOQgQAWgfAAQgeAAgOgTg");
	this.shape_97.setTransform(2.7,58);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#54BB6F").s().p("AgQBXIAAh+IAhAAIAAB+gAgQg3IAAgfIAhAAIAAAfg");
	this.shape_98.setTransform(-7.3,55.6);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#54BB6F").s().p("AgLBJQgGgFAAgMIAAhOIgSAAIAAgYIASAAIAAgjIAgAAIAAAjIAVAAIAAAYIgVAAIAABCQAAAIACACQACACAKAAIAEAAIADAAIAAAZIgQABIgDAAQgUAAgIgJg");
	this.shape_99.setTransform(-14.8,56.3);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#54BB6F").s().p("AgwBSQgLgKAAgSQAAgXASgLQALgGASgDIAMgBIANgCQAGgDABgGQAAgIgGgDQgFgDgKAAQgMAAgFAGQgEAFgBAFIggAAQABgPAIgLQAOgRAgAAQAUAAARAIQAQAJAAAXIAAA5IAAAPQABAHABACIAFAEIAAAFIglAAIgBgIIgBgHQgIAHgJAGQgJAGgOAAQgSAAgLgKgAANAdIgJADIgHABQgKABgFADQgIAFABAKQgBAIAFAEQAFAEAHAAQAKgBAJgGQAKgGAAgRIAAgMgAgTg4IAWgjIAlAAIgjAjg");
	this.shape_100.setTransform(-25.2,55.5);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#54BB6F").s().p("AA5BBIAAhQQAAgHgCgFQgFgJgMAAQgNAAgGAMQgCAGAAAIIAABLIghAAIAAhLQAAgLgCgFQgFgKgMAAQgOAAgFAKQgDAFAAAKIAABMIgiAAIAAh+IAhAAIAAASQAGgKAFgEQAKgHAQAAQAOAAAIAGQAIAGADAKQAHgMAKgFQAKgFANAAQAIAAAIADQAIADAHAIQAFAHACAKQACAGAAANIgBBPg");
	this.shape_101.setTransform(-42.5,57.8);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#54BB6F").s().p("AgkBBIAAh+IAgAAIAAAWQAHgNAFgEQAKgJAPABIABAAIADAAIAAAiIgFAAIgEAAQgVAAgHAMQgCAIAAAPIAAA8g");
	this.shape_102.setTransform(-57.3,57.8);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#54BB6F").s().p("AgwAwQgRgUAAgcQAAgbARgUQAQgUAgAAQAhAAAQAUQARAUAAAbQAAAcgRAUQgQAUghAAQggAAgQgUgAgVgdQgJALAAASQAAATAJALQAHAKAOAAQAOAAAJgKQAHgLAAgTQAAgSgHgLQgJgKgOAAQgOAAgHAKg");
	this.shape_103.setTransform(-69.8,57.9);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#54BB6F").s().p("AgRBXIAAhmIgTAAIAAgXIASAAIAAgJQAAgUAHgIQAIgLAaAAIAGAAIAHAAIAAAcIgJgBQgHAAgCADQgDADAAAEIAAALIAWAAIAAAXIgWAAIAABmg");
	this.shape_104.setTransform(-81.1,55.6);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#54BB6F").s().p("AAXBBIAAhMQAAgKgDgGQgEgKgOAAQgQAAgGAPQgDAIgBAMIAABDIghAAIAAh+IAgAAIAAASQAHgJAGgEQAKgJAPABQATAAANAKQAMAKAAAZIAABUg");
	this.shape_105.setTransform(-92.4,57.8);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#54BB6F").s().p("AgRBWIAAirIAjAAIAACrg");
	this.shape_106.setTransform(-103.1,55.7);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#54BB6F").s().p("AgvA5QgMgKAAgSQAAgXASgKQALgFASgDIAMgBIANgDQAGgDABgGQAAgJgGgCQgGgEgJAAQgMAAgFAHQgDAEgCAHIggAAQABgRAIgLQAOgQAggBQAUAAAQAJQARAIAAAYIAAA5IAAAPQABAHABACIAFADIAAAGIglAAIgBgIIgBgIQgHAIgJAGQgKAFgOAAQgSABgKgLgAANAEIgJADIgGACQgLABgFADQgIAFAAAJQAAAJAFADQAFAEAHAAQAKAAAJgGQAKgGAAgRIAAgMg");
	this.shape_107.setTransform(169.1,34);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#54BB6F").s().p("AA5BBIAAhQQAAgHgCgFQgFgJgMAAQgNAAgGAMQgCAGAAAIIAABLIghAAIAAhLQAAgLgCgFQgFgKgMAAQgOAAgFAKQgDAFAAAKIAABMIgiAAIAAh+IAhAAIAAASQAGgKAFgEQAKgHAQAAQAOAAAIAGQAIAGADAKQAHgMAKgFQAKgFANAAQAIAAAIADQAIADAHAIQAFAHACAKQACAGAAANIgBBPg");
	this.shape_108.setTransform(151.8,33.8);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#54BB6F").s().p("AgrA0QgTgQAAgjQAAggARgSQASgSAbAAQARAAANAGQANAGAJANQAIALACAPQACAJgBAQIhcAAQABATANAIQAHAFAKAAQAMAAAHgGQAEgEADgFIAiAAQgCALgLAMQgRATgfAAQgYAAgUgQgAAdgMQgBgOgIgGQgIgHgMAAQgMAAgHAHQgHAHgCANIA5AAIAAAAg");
	this.shape_109.setTransform(134.7,34);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#54BB6F").s().p("AgLBJQgGgFAAgMIAAhOIgSAAIAAgYIASAAIAAgjIAgAAIAAAjIAVAAIAAAYIgVAAIAABCQAAAIACACQACACAKAAIAEAAIADAAIAAAZIgQABIgDAAQgUAAgIgJg");
	this.shape_110.setTransform(123.7,32.3);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#54BB6F").s().p("AgrA3QgPgMAAgVIAiAAQACAJADAFQAGAGAQAAQAKAAAGgDQAGgDgBgGQABgFgFgDQgEgDgegIQgWgEgIgIQgJgIAAgPQAAgRANgNQAOgNAaAAQAWAAAQAJQAOAKADAYIgiAAQAAgHgDgDQgFgIgNAAQgLABgEADQgFADAAAFQAAAGAFACQAFADAdAGQATAGAKAIQALAKgBAOQABATgOAMQgPAMgdAAQgdAAgPgNg");
	this.shape_111.setTransform(113.2,34);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#54BB6F").s().p("AgQBXIAAh/IAhAAIAAB/gAgQg3IAAgfIAhAAIAAAfg");
	this.shape_112.setTransform(103.2,31.6);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#54BB6F").s().p("AgzBMQgTgQgBgbIAjAAQACAMAFAGQAJALAUAAQANAAAJgDQAOgFAAgPQABgJgIgEQgHgFgRgDIgRgEQgcgHgKgHQgSgLAAgZQAAgXASgQQARgPAgAAQAbAAATAPQAUAOABAbIgiAAQgBgPgOgHQgIgEgMAAQgPAAgIAGQgJAFAAAKQAAAKAJAEQAFADASAEIAdAHQAUAFAKAHQAPAMAAAXQABAYgTAQQgSAPgiAAQghAAgTgPg");
	this.shape_113.setTransform(92.1,31.6);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#54BB6F").s().p("AgPBWIAAirIAfAAIAACrg");
	this.shape_114.setTransform(73.9,31.7);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#54BB6F").s().p("AgrA0QgTgQAAgjQAAggARgSQASgSAbAAQARAAANAGQANAGAJANQAIALACAPQACAJgBAQIhcAAQABATANAIQAHAFAKAAQAMAAAHgGQAEgEADgFIAiAAQgCALgLAMQgRATgfAAQgYAAgUgQgAAdgMQgBgOgIgGQgIgHgMAAQgMAAgHAHQgHAHgCANIA5AAIAAAAg");
	this.shape_115.setTransform(64.2,34);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#54BB6F").s().p("AguBGQgPgSAAgcQAAggAPgTQAPgTAZAAQALAAAJAGQAJAFAFAJIAAg9IAiAAIAACrIggAAIAAgRQgIALgJAFQgJAFgMAAQgXAAgPgSgAgUgFQgHAKAAAQQAAASAHALQAHAKANAAQAOAAAIgKQAHgLAAgRQAAgWgMgKQgHgGgKAAQgNAAgHALg");
	this.shape_116.setTransform(49.6,31.9);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#54BB6F").s().p("AgwAwQgQgUAAgcQAAgbAQgUQAQgUAgAAQAhAAAQAUQAQAUAAAbQAAAcgQAUQgQAUghAAQggAAgQgUgAgWgdQgHALgBASQABATAHALQAIAKAOAAQAPAAAHgKQAIgLAAgTQAAgSgIgLQgHgKgPAAQgOAAgIAKg");
	this.shape_117.setTransform(28.7,33.9);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#54BB6F").s().p("AgQBXIAAh/IAhAAIAAB/gAgQg3IAAgfIAhAAIAAAfg");
	this.shape_118.setTransform(18,31.6);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#54BB6F").s().p("AgkBCIAAh/IAgAAIAAAWQAHgNAFgFQAKgHAPgBIABABIADAAIAAAiIgFAAIgEAAQgVgBgHANQgCAIAAAPIAAA9g");
	this.shape_119.setTransform(10.6,33.8);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#54BB6F").s().p("AgvA5QgMgKAAgSQAAgXASgKQAKgFAUgDIALgBIANgDQAGgDAAgGQABgJgGgCQgFgEgKAAQgMAAgFAHQgEAEgBAHIggAAQABgRAIgLQAOgQAggBQAUAAAQAJQARAIAAAYIAAA5IAAAPQABAHABACIAFADIAAAGIgkAAIgCgIIgCgIQgHAIgIAGQgKAFgOAAQgRABgLgLgAANAEIgJADIgGACQgLABgFADQgIAFAAAJQABAJAEADQAFAEAHAAQAKAAAJgGQAJgGAAgRIAAgMg");
	this.shape_120.setTransform(-1.2,34);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#54BB6F").s().p("AgzAtQgGgMABgVIAAhMIAiAAIAABMQAAAKADAGQAEAKAOAAQAQAAAFgOQAEgIAAgMIAAhEIAhAAIAAB+IgfAAIAAgSIgDAEIgFAFQgIAHgHACQgGADgKAAQgcAAgKgUg");
	this.shape_121.setTransform(-15.3,34.1);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#54BB6F").s().p("AgrA3QgPgMAAgVIAjAAQABAJADAFQAGAGAQAAQAKAAAGgDQAGgDgBgGQABgFgFgDQgEgDgegIQgVgEgKgIQgIgIgBgPQAAgRAOgNQAOgNAZAAQAYAAAPAJQAPAKACAYIghAAQgBgHgDgDQgFgIgNAAQgLABgEADQgFADAAAFQAAAGAFACQAFADAdAGQATAGALAIQAKAKgBAOQAAATgOAMQgOAMgdAAQgeAAgOgNg");
	this.shape_122.setTransform(-29.2,34);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#54BB6F").s().p("Ag8A9QgIgQAAgbIAAhqIAkAAIAABqQAAARAEAJQAHAOAVAAQAWAAAGgOQAEgJAAgRIAAhqIAlAAIAABqQAAAbgIAQQgQAcgtAAQgsAAgQgcg");
	this.shape_123.setTransform(-44.4,31.9);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#54BB6F").s().p("AgrA0QgTgQAAgjQAAggARgSQASgSAbAAQARAAANAGQANAGAJANQAIALACAPQACAJgBAQIhcAAQABATANAIQAHAFAKAAQAMAAAHgGQAEgEADgFIAiAAQgCALgLAMQgRATgfAAQgYAAgUgQgAAdgMQgBgOgIgGQgIgHgMAAQgMAAgHAHQgHAHgCANIA5AAIAAAAg");
	this.shape_124.setTransform(-66.2,34);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#54BB6F").s().p("AguBGQgPgSAAgcQAAggAPgTQAPgTAZAAQALAAAJAGQAJAFAFAJIAAg9IAiAAIAACrIggAAIAAgRQgIALgJAFQgJAFgMAAQgXAAgPgSgAgUgFQgHAKAAAQQAAASAHALQAHAKANAAQAOAAAIgKQAHgLAAgRQAAgWgMgKQgHgGgKAAQgNAAgHALg");
	this.shape_125.setTransform(-80.8,31.9);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#54BB6F").s().p("AgPBWIAAirIAfAAIAACrg");
	this.shape_126.setTransform(-97.7,31.7);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#54BB6F").s().p("AgwA5QgLgKAAgSQAAgXASgKQALgFASgDIAMgBIANgDQAGgDABgGQAAgJgGgCQgGgEgJAAQgMAAgFAHQgDAEgCAHIggAAQABgRAIgLQAOgQAggBQAUAAAQAJQARAIAAAYIAAA5IAAAPQABAHABACIAFADIAAAGIglAAIgBgIIgBgIQgHAIgJAGQgKAFgOAAQgSABgLgLgAANAEIgJADIgHACQgKABgFADQgIAFAAAJQAAAJAFADQAFAEAHAAQAKAAAJgGQAKgGAAgRIAAgMg");
	this.shape_127.setTransform(-107.6,34);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#54BB6F").s().p("AgzAtQgGgMABgVIAAhMIAiAAIAABMQAAAKADAGQAEAKAOAAQAPAAAGgOQAEgIAAgMIAAhEIAiAAIAAB+IghAAIAAgSIgCAEIgFAFQgHAHgHACQgHADgKAAQgcAAgKgUg");
	this.shape_128.setTransform(-121.8,34.1);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#54BB6F").s().p("AAYBCIAAhNQAAgKgEgGQgEgJgOgBQgQAAgGAPQgEAHAAAMIAABFIggAAIAAh/IAfAAIAAATQAHgKAGgFQAKgHAPgBQATAAANALQAMAKAAAYIAABWg");
	this.shape_129.setTransform(-136.3,33.8);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#54BB6F").s().p("AgvA5QgMgKAAgSQAAgXASgKQAKgFAUgDIALgBIAMgDQAIgDgBgGQABgJgGgCQgFgEgKAAQgMAAgFAHQgEAEgBAHIggAAQABgRAJgLQANgQAggBQAUAAAQAJQARAIAAAYIAAA5IAAAPQAAAHACACIAFADIAAAGIgkAAIgDgIIgBgIQgHAIgIAGQgKAFgOAAQgSABgKgLgAANAEIgJADIgGACQgLABgFADQgHAFgBAJQABAJAEADQAFAEAHAAQAKAAAJgGQAJgGAAgRIAAgMg");
	this.shape_130.setTransform(-150.2,34);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#54BB6F").s().p("AAyBWIAAh0IAAgOIAAgOIghCQIgiAAIggiQIAAAOIAAAOIAAB0IgiAAIAAirIA1AAIAeCGIAgiGIA0AAIAACrg");
	this.shape_131.setTransform(-166.8,31.7);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#373435").s().p("AglA7IAYhyIANAAIgEAWQAIgOAKgFQAKgGAOABIgDAOQgLAAgIADQgIAEgGAHQgEAGgEAIQgEAJgCAIIgMA5g");
	this.shape_132.setTransform(82.8,105.9);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#373435").s().p("AgfA4QgJgEgGgHQgFgHgCgMQgCgLADgPQADgMAFgLQAGgLAIgIQAJgHALgFQAKgFANAAQANAAAJAFQAJAFAFAHQAFAIABALQAAALgCAMQgDANgGAMQgGAKgJAJQgJAHgKAFQgKAEgMAAQgLAAgIgEgAgHgrQgJAEgGAGQgGAIgEAIQgEAJgCAIQgCAKABAKQAAAJAEAGQADAGAHAEQAGADAKAAQAJAAAIgEQAJgEAGgGQAGgIAEgIQAEgIACgKQACgJAAgJQgBgIgDgHQgDgHgHgDQgGgEgLAAQgKAAgHAEg");
	this.shape_133.setTransform(70.5,106.1);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#373435").s().p("AgdA5QgHgDgFgFQgEgGgCgGQgBgIABgIIANAAQABAPAGAGQAHAGAPAAQAGAAAGgCQAFgBAEgEQAFgDACgDQADgEAAgEQACgIgEgFQgFgEgIgCIgPgEIgRgEQgHgDgEgGQgEgGACgLQADgPAMgJQANgJASAAQASAAAKAIQAKAIgCAUIgNAAQABgNgHgGQgHgFgNAAQgLAAgIAFQgIAFgCAKQgBAFACAEQADADAFADIAKAEIAMADIAMAEQAHACAEADQAEADACAGQACAGgCAIQgBAIgFAGQgFAGgHAFQgHAEgJACQgIACgKAAQgKAAgHgDg");
	this.shape_134.setTransform(56.8,106.1);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#373435").s().p("AgXBPIAXhxIANAAIgXBxgAAFg7IAEgTIAPAAIgFATg");
	this.shape_135.setTransform(47.5,103.8);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#373435").s().p("AghA3QgJgEgFgIQgFgIAAgLQgBgLADgMQACgMAGgLQAGgKAIgJQAIgIALgFQAKgFANAAQAUAAAKAJQAKAKgBAUIgOAAQAAgNgHgHQgHgHgOAAQgKAAgHAEQgIAEgGAHQgGAHgEAJQgEAIgCAJQgCAJAAAIQAAAIAEAIQADAGAGAEQAHAEAKAAQAOAAAKgIQALgIAFgOIAOAAQgDAJgGAIQgFAIgIAFQgHAGgJADQgIADgLAAQgNAAgJgFg");
	this.shape_136.setTransform(36.5,106.1);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#373435").s().p("AghA3QgJgEgFgIQgFgIAAgLQgBgLADgMQACgMAGgLQAGgLAJgJQAIgHALgFQAKgFAMAAQAXAAAKAQQAKAQgGAeIhYAAQgCAKABAHQABAJADAGQAEAGAGAEQAHADAJAAQAMAAALgHQAKgGAGgNIAPAAQgEAIgGAHQgFAHgHAFQgHAFgJADQgIADgLAAQgNAAgJgFgAgEgsQgHAEgGAFQgGAGgEAGQgFAIgCAIIBJAAQACgIgBgIQgBgHgDgFQgDgGgHgDQgGgDgJAAQgIAAgHADg");
	this.shape_137.setTransform(22.3,106.1);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("#373435").s().p("AhJBPIAhidIA1AAIANAAIAMACQAGABAGACQAFADAFAEQAGAFAEAJQADAIABAJQABAJgBAKIgCAQIgFAQIgGAQQgEAIgFAHQgGAHgHAGQgGAGgHAEQgIAEgIACQgHACgHAAIgPABgAg4BCIAkAAIATgBQAIgBAKgFQAKgEAGgHQAHgGAEgIQAFgIADgJIAEgRIADgRQABgJgBgIQgCgIgEgGQgDgHgIgEQgGgEgKgBIgTgBIgiAAg");
	this.shape_138.setTransform(6.4,103.8);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#373435").s().p("AgSA2IADgTIAPAAIgCATgAAAgiIADgTIAQAAIgDATg");
	this.shape_139.setTransform(-14.9,106.3);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#373435").s().p("AgWBPIAgidIANAAIggCdg");
	this.shape_140.setTransform(-29.9,103.8);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#373435").s().p("AgXBPIAXhxIANAAIgXBxgAAFg7IAEgTIAPAAIgFATg");
	this.shape_141.setTransform(-36.7,103.8);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#373435").s().p("AgiBRIAWhmIgRAAIACgLIARAAIAFgSQACgOAHgIQAGgIAOAAIAKABIgCAMIgIgBQgEAAgEACQgDACgCADIgDAHIgCAIIgDAOIAWAAIgCALIgXAAIgUBmg");
	this.shape_142.setTransform(-43.4,103.7);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("#373435").s().p("AglA7IAYhyIANAAIgEAWQAIgOAKgFQAKgGAOABIgDAOQgLAAgIADQgIAEgGAHQgEAGgEAIQgEAJgCAIIgMA5g");
	this.shape_143.setTransform(-52.9,105.9);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#373435").s().p("AghA3QgJgEgFgIQgFgIAAgLQgBgLADgMQACgMAGgLQAGgLAJgJQAIgHALgFQAKgFAMAAQAXAAAKAQQAKAQgGAeIhYAAQgCAKABAHQABAJADAGQAEAGAGAEQAHADAJAAQAMAAALgHQAKgGAGgNIAPAAQgEAIgGAHQgFAHgHAFQgHAFgJADQgIADgLAAQgNAAgJgFgAgEgsQgHAEgGAFQgGAGgEAGQgFAIgCAIIBJAAQACgIgBgIQgBgHgDgFQgDgGgHgDQgGgDgJAAQgIAAgHADg");
	this.shape_144.setTransform(-65.2,106.1);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("#373435").s().p("AhDBPIAiidIA5AAQAPAAAKADQAJAEAEAGQAFAGAAAIQABAIgCAJQgDANgHAJQgHAIgKAEQgJAFgMACQgLACgKAAIgiAAIgPBGgAgigCIAiAAQAKAAAIgCQAJgBAHgEQAGgDAFgHQAEgGACgKQAEgPgIgIQgJgHgVAAIgmAAg");
	this.shape_145.setTransform(-79.4,103.8);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("#FEFEFE").s().p("EgkdAZfMAAAgy9MBI7AAAMAAAAy9g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_146},{t:this.shape_145},{t:this.shape_144},{t:this.shape_143},{t:this.shape_142},{t:this.shape_141},{t:this.shape_140},{t:this.shape_139},{t:this.shape_138},{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.logooit, new cjs.Rectangle(-233.4,-163,470.5,354.9), null);


(lib.titulo2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgbA9IgDgBIgCgBIAAgBIAAhxIAAgCIACgBIACgBIAFAAIAFAAIADABIABABIABACIAAAQIAIgLIAHgHIAHgDIAIgBIADABIAFAAIAEABIADACIABAAIAAACIAAACIAAAFIAAAFIAAADIgBABIgCABIgCgBIgEAAIgEgBIgFgBIgGABIgGAEIgGAIIgIAMIAABKIgBABIgCABIgDABIgFABg");
	this.shape.setTransform(144.1,20.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgZA6QgKgEgIgIQgHgIgDgLQgDgLAAgPQAAgNADgMQAEgLAHgJQAIgIAKgFQAMgFANAAQAOAAALAFQAKAEAHAIQAHAIADALQAEALAAAPQAAANgEAMQgEALgGAJQgIAIgLAFQgLAFgOAAQgNAAgLgFgAgPgpQgHADgEAGQgEAGgDAJQgCAIAAAJQAAAJACAIQACAJAEAGQAEAGAHADQAGAEAKAAQAJAAAHgDQAGgEAFgFQAEgGACgJQADgIgBgKQAAgIgBgIQgCgJgEgGQgEgGgHgDQgHgEgKAAQgIAAgHADg");
	this.shape_1.setTransform(132,20.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgPA+IgKgDIgHgDIgFgDIgCgDIAAgGIAAgEIAAgDIACgBIABgBIAEACIAHADIAJAEQAGACAHAAIAJgBIAHgEQADgCACgDQACgEAAgEQAAgFgDgDQgCgEgEgCIgKgFIgJgEIgKgFQgGgBgEgEQgEgEgCgGQgDgFAAgIQAAgHADgGQACgGAGgEQAFgFAIgDQAHgDAKAAIAJABIAIACIAHADIADACIACACIABABIAAADIAAADIAAAEIAAACIgCACIgBAAIgDgBIgGgDIgIgDIgKgBIgJABQgEABgCACIgEAFIgCAHQAAAFADADQACAEAEACIAJAFIAKAEIALAFQAFABAEAEQAEAEADAFQACAGAAAHQAAAJgDAHQgDAGgGAFQgGAFgIACQgJADgJAAg");
	this.shape_2.setTransform(120.1,20.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgEBUIgDgBIgCgBIAAgCIAAhwIAAgDIACgBIADgBIAEAAIAFAAIADABIACABIABADIAABwIgBACIgCABIgDABIgFABgAgJg9QgDgDAAgHQAAgIADgCQADgCAGgBQAIABACACQADACAAAIQAAAHgDADQgDACgHABQgGgBgDgCg");
	this.shape_3.setTransform(112.1,18.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgPA7QgKgEgGgIQgHgIgDgLQgDgMAAgPQAAgPAEgMQAEgNAIgHQAGgIAKgEQAKgEAKAAIALABIAJADIAIAEIAEAEIADACIABACIAAADIABAEQgBAFgBACQAAAAgBABQAAAAAAAAQAAABgBAAQAAAAgBAAQgBAAgDgDIgGgEIgKgFQgFgCgHAAQgOAAgIAMQgJALAAAVQABALACAJQACAIAEAGQAEAFAGADQAFADAIAAQAGAAAGgDIAJgFIAHgFIAEgCIACABIABABIABADIAAAFIAAAEIAAACIgBACIgDACIgFAEIgIAFIgLADIgMABQgKAAgKgEg");
	this.shape_4.setTransform(103.8,20.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgTA7QgLgEgHgIQgHgIgEgLQgDgMAAgPQAAgOADgMQAEgMAHgIQAHgIALgEQAKgFALAAQAOAAAJAFQAKAEAGAHQAGAIADAJQACAKAAALIAAAEQAAAEgCACQgCACgEAAIhKAAQAAAKACAHQACAIAEAFQAFAGAHADQAHADAJAAIAPgCIALgDIAIgDIAEgBIACABIABABIAAACIABAEIAAADIgBACIAAACIgCABIgEADIgJADIgMADIgPABQgOAAgKgEgAgLgqQgGADgEAEQgEAFgCAHQgDAGAAAHIA9AAQABgRgIgJQgIgJgPAAQgHAAgFADg");
	this.shape_5.setTransform(91.7,20.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("Ag2BSQgDAAgDgCQgCgCAAgFIAAiRQAAgFACgCQADgCADAAIAlAAQAUAAAPAGQAPAFAKAKQAJALAGAOQAFAPAAATQAAAUgGAQQgFAQgKAKQgLALgPAFQgQAFgTAAgAgpBAIAXAAQAPAAALgEQAKgEAIgIQAGgJAEgMQAEgMAAgPQAAgOgEgLQgDgLgGgJQgIgIgKgFQgKgFgRAAIgXAAg");
	this.shape_6.setTransform(77.8,18.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgGA6IgDgDIgCgEIgCgHIACgHIACgEIADgDIAGAAIAHAAIADADIADAEIAAAHIAAAHIgDAEIgDADIgHABgAgGgdIgDgCIgCgFIgCgHIACgHIACgFIADgCIAGAAIAHAAIADACIADAFIAAAHIAAAHIgDAFIgDACIgHABg");
	this.shape_7.setTransform(60.3,21);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgFBYIgDgBIgBgBIAAgBIAAipIAAgBIABgCIADgBIAFAAIAFAAIADABIACACIAAABIAACpIAAABIgCABIgDABIgFABg");
	this.shape_8.setTransform(53.6,17.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgEBUIgDgBIgCgBIAAgCIAAhwIAAgDIACgBIADgBIAEAAIAFAAIADABIACABIABADIAABwIgBACIgCABIgDABIgFABgAgJg9QgDgDAAgHQAAgIADgCQADgCAGgBQAIABACACQADACAAAIQAAAHgDADQgDACgHABQgGgBgDgCg");
	this.shape_9.setTransform(47.6,18.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgMBZIgDgBIgCgBIAAgCIAAhjIgRAAQAAAAgBAAQAAAAAAAAQgBgBAAAAQgBAAAAgBIgBgHIABgEIAAgCIACgCIABAAIARAAIAAgMQAAgMACgIQACgJAFgFQAEgGAGgCQAHgDAJAAIAJABIAGACIADABIACACIAAADIABAEIgBAEIAAACIgBACIgBAAIgCgBIgEgBIgEgBIgGgBQgFAAgDACQgEABgCAEQgCADAAAFIgBAMIAAANIAZAAIACAAIABACIABACIAAAEIgBAHQAAABgBAAQAAAAAAABQgBAAAAAAQAAAAgBAAIgZAAIAABjIgBACIgCABIgCABIgFAAg");
	this.shape_10.setTransform(41.1,17.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgbA9IgDgBIgCgBIAAgBIAAhxIAAgCIACgBIACgBIAFAAIAFAAIADABIABABIABACIAAAQIAIgLIAHgHIAHgDIAIgBIADABIAFAAIAEABIADACIABAAIAAACIAAACIAAAFIAAAFIAAADIgBABIgCABIgCgBIgEAAIgEgBIgFgBIgGABIgGAEIgGAIIgIAMIAABKIgBABIgCABIgDABIgFABg");
	this.shape_11.setTransform(33,20.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgTA7QgLgEgHgIQgHgIgEgLQgDgMAAgPQAAgOADgMQAEgMAHgIQAHgIALgEQAKgFALAAQAOAAAJAFQAKAEAGAHQAGAIADAJQACAKAAALIAAAEQAAAEgCACQgCACgEAAIhKAAQAAAKACAHQACAIAEAFQAFAGAHADQAHADAJAAIAPgCIALgDIAIgDIAEgBIACABIABABIAAACIABAEIAAADIgBACIAAACIgCABIgEADIgJADIgMADIgPABQgOAAgKgEgAgLgqQgGADgEAEQgEAFgCAHQgDAGAAAHIA9AAQABgRgIgJQgIgJgPAAQgHAAgFADg");
	this.shape_12.setTransform(21.3,20.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgsBSIgDgBIgCgBIAAgCIAAiWQAAgFACgCQADgCADgBIAjAAIAKABIALACQAHABAHAEQAGAEAFAGQAFAGADAIQACAHAAAKQAAAMgEAJQgEALgHAFQgIAIgLADQgLAEgOAAIgTAAIAAA7IAAACIgCABIgDABIgGABgAgcACIAUAAQAJgBAGgBQAHgDAEgEQAFgFACgGQACgGAAgHQAAgKgDgIQgEgGgGgEQgFgCgGgBIgLgCIgUAAg");
	this.shape_13.setTransform(9.2,18.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.titulo2, new cjs.Rectangle(0,0,150,35.7), null);


(lib.titulo1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgSBPQgNgGgJgKQgJgLgFgPQgEgPgBgUQAAgTAGgQQAFgQAJgLQAKgMANgFQANgGAPAAIAOACIAMACIALAGIAGAEIACADIABACIAAACIAAAEIAAAFIgBACIgBADIgBAAIgFgDIgJgFIgMgGQgHgCgKAAQgLAAgIAEQgJAFgGAJQgHAHgDANQgEAMABAPQgBAPAEAMQADANAGAHQAHAIAJAFQAIAEALAAQALAAAGgCIAOgGIAIgFQADgDACAAIABABIABABIABADIAAAFIAAAEIAAACIgBACIgCACIgFAEIgKAFIgOAEQgIACgJAAQgPAAgNgFg");
	this.shape.setTransform(521.9,18.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgEBTIgDgBIgDAAIgCgBIgBgBIgBgCIg1iXIgBgFIABgDIADgBIAHAAIAGAAIADABIACABIABACIAuCJIAAAAIAuiIIABgDIABgBIAEgBIAGAAIAGAAIADACIABACIgBAFIg2CXIgBACIgCACIgFAAIgGABg");
	this.shape_1.setTransform(507.6,18.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgEBSIgEgBIgBgBIgBgCIAAicIABgCIACgBIADgBIAEAAIAFAAIAEABIABABIABACIAACcIgBACIgBABIgEABIgFABg");
	this.shape_2.setTransform(497,18.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgTA7QgLgEgHgIQgHgIgEgLQgDgMAAgPQAAgOADgMQAEgMAHgIQAHgIALgEQAKgFALAAQAOAAAJAFQAKAEAGAHQAGAIADAJQACAKAAALIAAAEQAAAEgCACQgCACgEAAIhKAAQAAAKACAHQACAIAEAFQAFAGAHADQAHADAJAAIAPgCIALgDIAIgDIAEgBIACABIABABIAAACIABAEIAAADIgBACIAAACIgCABIgEADIgJADIgMADIgPABQgOAAgKgEgAgLgqQgGADgEAEQgEAFgCAHQgDAGAAAHIA9AAQABgRgIgJQgIgJgPAAQgHAAgFADg");
	this.shape_3.setTransform(481.4,20.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgbBUQgIgFgFgIQgHgIgCgLQgDgMAAgMQAAgPAEgMQADgKAGgJQAHgIAJgFQAJgEAMAAQAJAAAIAEQAIAFAJAIIAAhCIAAgCIABgBIAEgBIAFAAIAFAAIAEABIABABIAAACIAACoIAAABIgBACIgDAAIgFABIgEgBIgDAAIgCgCIAAgBIAAgPQgJAJgJAGQgLAFgKAAQgMAAgKgFgAgOgNQgGADgDAHQgDAFgCAIQgCAIABAIIABARQABAIAEAGQADAHAFADQAFAEAIAAIAIgBIAIgEIAIgGIAKgLIAAguQgJgJgIgGQgIgFgIAAQgHAAgGAEg");
	this.shape_4.setTransform(467.8,18);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgZA6QgKgEgIgIQgGgIgEgLQgDgLAAgPQAAgNADgMQAEgLAHgJQAIgIAKgFQAMgFANAAQAOAAALAFQAKAEAHAIQAHAIADALQAEALAAAPQAAANgEAMQgEALgGAJQgIAIgLAFQgLAFgNAAQgPAAgKgFgAgPgpQgHADgEAGQgEAGgDAJQgCAIAAAJQAAAJACAIQACAJAEAGQAEAGAHADQAGAEAKAAQAJAAAHgDQAGgEAFgFQAEgGACgJQACgIABgKQAAgIgCgIQgCgJgEgGQgEgGgHgDQgGgEgLAAQgIAAgHADg");
	this.shape_5.setTransform(448.7,20.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgPA7QgJgEgHgIQgGgIgDgLQgEgMAAgPQAAgPAEgMQAEgNAHgHQAIgIAJgEQAKgEAKAAIALABIAJADIAIAEIAFAEIACACIAAACIABADIAAAEQABAFgCACQAAAAgBABQAAAAAAAAQAAABgBAAQAAAAAAAAQgCAAgDgDIgGgEIgJgFQgGgCgHAAQgOAAgIAMQgJALABAVQAAALACAJQACAIAEAGQAEAFAGADQAFADAHAAQAIAAAFgDIAKgFIAGgFIAFgCIABABIABABIAAADIABAFIgBAEIAAACIgBACIgBACIgGAEIgJAFIgKADIgLABQgLAAgKgEg");
	this.shape_6.setTransform(436.8,20.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgEBUIgDgBIgCgBIAAgCIAAhwIAAgDIACgBIADgBIAEAAIAFAAIADABIACABIABADIAABwIgBACIgCABIgDABIgFABgAgJg9QgDgDAAgHQAAgIADgCQADgCAGgBQAIABACACQADACAAAIQAAAHgDADQgDACgHABQgGgBgDgCg");
	this.shape_7.setTransform(428.1,18.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAQBNQgIAAgHgCQgFgDgEgEQgEgFgCgIQgCgHAAgKIAAhBIgQAAQAAAAgBAAQAAgBgBAAQAAAAAAAAQAAgBgBAAQgBgCAAgFIAAgEIABgDIABgBIACgBIAQAAIAAgbIABgCIABgBIADgBIAFAAIAGAAIACABIABABIABACIAAAbIAdAAIACABIABABIABADIAAAEIgBAHQAAAAAAABQgBAAAAAAQAAAAgBABQAAAAgBAAIgdAAIAAA+QAAAMAEAGQADAGAJAAIAFAAIAEgCIADgBIADgBIABABIABABIAAACIAAAEIAAAGIgCADIgDACIgEACIgGABg");
	this.shape_8.setTransform(420.6,19.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgWBYQgIgDgFgEQgFgEgDgHQgDgHAAgIQAAgJAEgIQAEgGAIgFQAGgFAMgDQAKgBAMAAIAQAAIAAgJQAAgHgCgEQgBgFgDgDQgDgEgFgCQgFgBgHAAQgHAAgGABIgKAFIgIAEIgFABIgCAAIgBgBIgBgDIAAgEIAAgEIADgEIAFgEIAKgEIAMgDIAMgCQAMAAAJADQAIADAFAFQAGAGACAIQADAIAAAKIAABMIgBADIgDABIgFABIgFgBIgDgBIgBgDIAAgLQgIAIgJAEQgJAFgKAAQgIAAgHgCgAgGAiIgLAEQgDADgCAEQgCAEgBAFQABAJAFAGQAGAEAKAAQAHAAAIgDQAGgFAIgJIAAgXIgSAAQgIAAgGABgAgCgyIgCgBIgBgCIABgCIASgeIABgCIADgBIAEgBIAEAAIAHAAIACABIABACIgBADIgXAdIgCACIgCABIgCABIgFAAg");
	this.shape_9.setTransform(410.2,18);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("ABFA9IgDgBIgCgBIAAgBIAAhFIgCgMQgBgHgDgEQgDgEgEgCQgEgDgGAAQgIAAgGAGQgIAGgIAKIAABPIgBABIgBABIgEABIgFABIgEgBIgDgBIgCgBIAAgBIAAhFIgCgMIgEgLQgDgEgEgCQgFgDgFAAQgIAAgGAGQgIAGgIAKIAABPIgBABIgCABIgDABIgFABIgFgBIgDgBIgCgBIgBgBIAAhxIABgCIACgBIACgBIAFAAIAEAAIADABIACABIABACIAAAPQAJgLAJgFQAJgFAKAAQAGAAAGACIAKAEQAFADABAEIAGAJIALgKIAJgHIAKgDIAJgCQALAAAHAEQAHAEAFAGQAEAHACAJQACAIAAAKIAABHIAAABIgCABIgDABIgFABg");
	this.shape_10.setTransform(394.1,20.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgbA9IgDgBIgCgBIAAgBIAAhxIAAgCIACgBIACgBIAFAAIAFAAIADABIABABIABACIAAAQIAIgLIAHgHIAHgDIAIgBIADABIAFAAIAEABIADACIABAAIAAACIAAACIAAAFIAAAFIAAADIgBABIgCABIgCgBIgEAAIgEgBIgFgBIgGABIgGAEIgGAIIgIAMIAABKIgBABIgCABIgDABIgFABg");
	this.shape_11.setTransform(379.9,20.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgZA6QgKgEgHgIQgIgIgCgLQgEgLAAgPQAAgNAEgMQADgLAIgJQAHgIAKgFQAMgFANAAQAOAAALAFQAKAEAHAIQAHAIAEALQADALAAAPQAAANgDAMQgFALgHAJQgHAIgLAFQgLAFgNAAQgOAAgLgFgAgPgpQgGADgFAGQgEAGgDAJQgBAIAAAJQAAAJABAIQACAJAEAGQAEAGAHADQAHAEAJAAQAJAAAHgDQAGgEAFgFQAEgGACgJQADgIAAgKQAAgIgDgIQgBgJgEgGQgEgGgHgDQgHgEgJAAQgJAAgHADg");
	this.shape_12.setTransform(367.9,20.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgMBZIgDgBIgCgBIAAgCIAAhjIgRAAQAAAAgBAAQAAAAAAAAQgBgBAAAAQAAAAgBgBIgBgHIABgEIAAgCIACgCIABAAIARAAIAAgMQAAgMACgIQACgJAFgFQAEgGAGgCQAHgDAJAAIAJABIAGACIADABIACACIAAADIABAEIgBAEIAAACIgBACIgBAAIgCgBIgEgBIgEgBIgGgBQgFAAgDACQgEABgCAEQgCADAAAFIgBAMIAAANIAZAAIACAAIABACIABACIAAAEIgBAHQAAABgBAAQAAAAAAABQgBAAAAAAQAAAAgBAAIgZAAIAABjIgBACIgCABIgCABIgFAAg");
	this.shape_13.setTransform(358,17.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAhA9IgCgBIgCgBIgBgBIAAhCQAAgKgBgFQgCgHgDgEQgDgEgFgCQgEgDgGAAQgHAAgIAGQgHAGgJAKIAABPIgBABIgCABIgCABIgGABIgFgBIgDgBIgCgBIAAgBIAAhxIAAgCIACgBIADgBIAEAAIAFAAIADABIABABIABACIAAAPQAJgLAKgFQAKgFAJAAQAKAAAJAEQAHAEAFAGQAFAHACAJQACAIAAAMIAABFIgBABIgBABIgDABIgFABg");
	this.shape_14.setTransform(347.1,20.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgEBSIgEgBIgBgBIgBgCIAAicIABgCIACgBIADgBIAEAAIAFAAIAEABIABABIABACIAACcIgBACIgBABIgEABIgFABg");
	this.shape_15.setTransform(337,18.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgWA8QgIgCgFgEQgFgFgDgGQgDgHAAgIQAAgKAEgHQAEgHAHgEQAIgEAKgDQALgCANAAIAPAAIAAgJQAAgGgCgFQgBgGgDgDQgDgDgFgCQgFgCgHAAQgHAAgGACIgKAEIgIAEIgFACIgCAAIgBgCIgBgCIAAgEIAAgFIACgDIAGgEIAKgEIAMgDIAMgCQAMAAAIADQAJADAGAFQAFAGACAIQADAIAAAKIAABMIgBACIgDACIgFAAIgGAAIgCgCIgBgCIAAgLQgHAIgKAEQgJAFgJAAQgKAAgGgDgAgGAHIgKAEQgFADgBAEQgCAEAAAFQAAAJAFAFQAGAFAKAAQAHAAAIgEQAGgEAIgJIAAgYIgSAAQgIAAgGACg");
	this.shape_16.setTransform(321.3,20.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("ABFA9IgDgBIgCgBIgBgBIAAhFIgBgMQgBgHgDgEQgCgEgFgCQgEgDgGAAQgHAAgIAGQgHAGgJAKIAABPIAAABIgBABIgEABIgFABIgEgBIgEgBIgBgBIAAgBIAAhFIgCgMIgEgLQgDgEgEgCQgFgDgFAAQgIAAgGAGQgIAGgJAKIAABPIAAABIgCABIgDABIgFABIgGgBIgCgBIgCgBIAAgBIAAhxIAAgCIACgBIACgBIAFAAIAEAAIAEABIABABIABACIAAAPQAJgLAJgFQAJgFAKAAQAGAAAGACIAKAEQAEADACAEIAGAJIAKgKIAKgHIAJgDIAJgCQALAAAIAEQAHAEAFAGQAEAHACAJQACAIAAAKIAABHIAAABIgCABIgDABIgFABg");
	this.shape_17.setTransform(305.2,20.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgTA7QgLgEgHgIQgHgIgEgLQgDgMAAgPQAAgOADgMQAEgMAHgIQAHgIALgEQAKgFALAAQAOAAAJAFQAKAEAGAHQAGAIADAJQACAKAAALIAAAEQAAAEgCACQgCACgEAAIhKAAQAAAKACAHQACAIAEAFQAFAGAHADQAHADAJAAIAPgCIALgDIAIgDIAEgBIACABIABABIAAACIABAEIAAADIgBACIAAACIgCABIgEADIgJADIgMADIgPABQgOAAgKgEgAgLgqQgGADgEAEQgEAFgCAHQgDAGAAAHIA9AAQABgRgIgJQgIgJgPAAQgHAAgFADg");
	this.shape_18.setTransform(288.3,20.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAQBNQgIAAgHgCQgFgDgEgEQgEgFgCgIQgCgHAAgKIAAhBIgQAAQAAAAgBAAQAAgBAAAAQgBAAAAAAQgBgBAAAAQgBgCAAgFIAAgEIABgDIABgBIACgBIAQAAIAAgbIABgCIABgBIADgBIAFAAIAGAAIACABIABABIABACIAAAbIAdAAIACABIABABIABADIAAAEIgBAHQAAAAAAABQgBAAAAAAQAAAAgBABQAAAAgBAAIgdAAIAAA+QAAAMAEAGQADAGAJAAIAFAAIAEgCIADgBIADgBIABABIABABIAAACIAAAEIAAAGIgCADIgDACIgEACIgGABg");
	this.shape_19.setTransform(277.6,19.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgPA+IgKgDIgHgDIgFgDIgCgDIAAgGIAAgEIAAgDIACgBIABgBIAEACIAHADIAJAEQAGACAHAAIAJgBIAHgEQADgCACgDQACgEAAgEQAAgFgDgDQgCgEgEgCIgKgFIgJgEIgKgFQgGgBgEgEQgEgEgCgGQgDgFAAgIQAAgHADgGQACgGAGgEQAFgFAIgDQAHgDAKAAIAJABIAIACIAHADIADACIACACIABABIAAADIAAADIAAAEIAAACIgCACIgBAAIgDgBIgGgDIgIgDIgKgBIgJABQgEABgCACIgEAFIgCAHQAAAFADADQACAEAEACIAJAFIAKAEIALAFQAFABAEAEQAEAEADAFQACAGAAAHQAAAJgDAHQgDAGgGAFQgGAFgIACQgJADgJAAg");
	this.shape_20.setTransform(268.7,20.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgEBUIgDgBIgCgBIAAgCIAAhwIAAgDIACgBIADgBIAEAAIAFAAIADABIACABIABADIAABwIgBACIgCABIgDABIgFABgAgJg9QgDgDAAgHQAAgIADgCQADgCAGgBQAIABACACQADACAAAIQAAAHgDADQgDACgHABQgGgBgDgCg");
	this.shape_21.setTransform(260.6,18.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgTBTIgNgDIgKgFIgEgEIgCgDIgBgGIAAgFIABgDIABgBIACgBIAFACIAIAFIAMAEQAHADAKAAQAGAAAFgCQAGgCAFgDQAEgEADgFQABgFAAgHQAAgHgCgFQgEgEgFgEIgMgHIgNgGIgNgGQgGgFgGgFQgFgFgDgHQgDgIAAgKQgBgJAEgJQAEgIAHgFQAHgGAIgDQAKgCAKAAIALABIAKACIAJADIAEAEIACACIABABIAAADIAAAEIAAAEIAAADIgBACIgDABIgDgCIgIgFIgKgDQgGgCgIAAQgGAAgFACQgFABgDADQgDAEgBADQgCAFgBAEQAAAHAEAGQADAFAGADIALAHIANAGIANAIQAIACAEAGQAGAFADAHQADAHABAKQgBALgEAJQgEAJgIAHQgHAGgKADQgLADgKAAg");
	this.shape_22.setTransform(251.7,18.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgEBYIgEgBIgBgBIgBgBIAAipIABgBIABgCIAEgBIAEAAIAFAAIADABIACACIAAABIAACpIAAABIgCABIgDABIgFABg");
	this.shape_23.setTransform(237,17.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgTA7QgLgEgHgIQgHgIgEgLQgDgMAAgPQAAgOADgMQAEgMAHgIQAHgIALgEQAKgFALAAQAOAAAJAFQAKAEAGAHQAGAIADAJQACAKAAALIAAAEQAAAEgCACQgCACgEAAIhKAAQAAAKACAHQACAIAEAFQAFAGAHADQAHADAJAAIAPgCIALgDIAIgDIAEgBIACABIABABIAAACIABAEIAAADIgBACIAAACIgCABIgEADIgJADIgMADIgPABQgOAAgKgEgAgLgqQgGADgEAEQgEAFgCAHQgDAGAAAHIA9AAQABgRgIgJQgIgJgPAAQgHAAgFADg");
	this.shape_24.setTransform(227.5,20.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgbBUQgIgFgFgIQgHgIgCgLQgDgMAAgMQAAgPAEgMQADgKAGgJQAHgIAIgFQAKgEAMAAQAJAAAIAEQAIAFAJAIIAAhCIAAgCIACgBIADgBIAFAAIAFAAIAEABIABABIAAACIAACoIAAABIgBACIgDAAIgFABIgEgBIgDAAIgCgCIAAgBIAAgPQgJAJgJAGQgLAFgKAAQgMAAgKgFgAgOgNQgFADgEAHQgDAFgCAIIgBAQIABARQABAIADAGQADAHAGADQAFAEAJAAIAHgBIAIgEIAIgGIAKgLIAAguQgJgJgIgGQgIgFgIAAQgIAAgFAEg");
	this.shape_25.setTransform(213.9,18);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgZA6QgKgEgIgIQgGgIgEgLQgDgLAAgPQAAgNADgMQAEgLAHgJQAHgIALgFQALgFAOAAQAOAAALAFQAKAEAHAIQAHAIAEALQADALAAAPQAAANgDAMQgEALgHAJQgIAIgLAFQgLAFgNAAQgPAAgKgFgAgPgpQgGADgFAGQgEAGgDAJQgBAIgBAJQABAJABAIQACAJAEAGQAEAGAHADQAGAEAKAAQAJAAAHgDQAHgEAEgFQAEgGACgJQACgIABgKQAAgIgCgIQgCgJgEgGQgEgGgHgDQgGgEgLAAQgIAAgHADg");
	this.shape_26.setTransform(194.8,20.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgEBUIgDgBIgCgBIAAgCIAAhwIAAgDIACgBIADgBIAEAAIAFAAIADABIACABIABADIAABwIgBACIgCABIgDABIgFABgAgJg9QgDgDAAgHQAAgIADgCQADgCAGgBQAIABACACQADACAAAIQAAAHgDADQgDACgHABQgGgBgDgCg");
	this.shape_27.setTransform(185,18.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgbA9IgDgBIgCgBIAAgBIAAhxIAAgCIACgBIACgBIAFAAIAFAAIADABIABABIABACIAAAQIAIgLIAHgHIAHgDIAIgBIADABIAFAAIAEABIADACIABAAIAAACIAAACIAAAFIAAAFIAAADIgBABIgCABIgCgBIgEAAIgEgBIgFgBIgGABIgGAEIgGAIIgIAMIAABKIgBABIgCABIgDABIgFABg");
	this.shape_28.setTransform(178.3,20.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgXA8QgHgCgFgEQgFgFgDgGQgDgHAAgIQAAgKAEgHQAEgHAHgEQAIgEAKgDQALgCAMAAIAQAAIAAgJQAAgGgBgFQgCgGgDgDQgDgDgFgCQgFgCgHAAQgHAAgGACIgLAEIgIAEIgEACIgCAAIgBgCIgBgCIgBgEIABgFIADgDIAFgEIAKgEIAMgDIAMgCQAMAAAIADQAKADAEAFQAGAGADAIQACAIAAAKIAABMIgBACIgDACIgFAAIgFAAIgEgCIAAgCIAAgLQgIAIgJAEQgJAFgKAAQgJAAgHgDgAgGAHIgLAEQgEADgCAEQgCAEAAAFQAAAJAHAFQAFAFAKAAQAHAAAHgEQAIgEAHgJIAAgYIgSAAQgIAAgGACg");
	this.shape_29.setTransform(166.5,20.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgbA6QgHgEgFgGQgFgHgCgJQgCgIAAgNIAAhEIAAgCIACgBIADgBIAFAAIAGAAIACABIACABIABACIAABBQAAAKABAHQACAFADAFQADAEAEACQAFADAGAAQAGAAAJgGQAHgFAJgLIAAhPIABgCIABgBIADgBIAGAAIAEAAIAEABIACABIAAACIAABxIAAACIgCABIgDABIgEAAIgFAAIgDgBIgBgBIgBgCIAAgPQgJALgKAFQgKAFgJAAQgLAAgIgEg");
	this.shape_30.setTransform(153.8,20.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgPA+IgKgDIgHgDIgFgDIgCgDIAAgGIAAgEIAAgDIACgBIABgBIAEACIAHADIAJAEQAGACAHAAIAJgBIAHgEQADgCACgDQACgEAAgEQAAgFgDgDQgCgEgEgCIgKgFIgJgEIgKgFQgGgBgEgEQgEgEgCgGQgDgFAAgIQAAgHADgGQACgGAGgEQAFgFAIgDQAHgDAKAAIAJABIAIACIAHADIADACIACACIABABIAAADIAAADIAAAEIAAACIgCACIgBAAIgDgBIgGgDIgIgDIgKgBIgJABQgEABgCACIgEAFIgCAHQAAAFADADQACAEAEACIAJAFIAKAEIALAFQAFABAEAEQAEAEADAFQACAGAAAHQAAAJgDAHQgDAGgGAFQgGAFgIACQgJADgJAAg");
	this.shape_31.setTransform(142,20.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgZBPQgLgDgIgIQgIgIgEgLQgFgMAAgPIAAhlIABgCIACgBIADAAIAFAAIAFAAIAEAAIABABIABACIAABiQAAAMADAJQADAIAFAFQAFAGAIADQAHADAIAAQAKAAAHgDQAHgDAGgGQAFgFADgIQACgJAAgLIAAhjIABgCIABgBIAEAAIAFAAIAFAAIAEAAIABABIABACIAABjQAAAQgEALQgFAMgIAIQgIAIgMAEQgLAFgOgBQgOAAgLgEg");
	this.shape_32.setTransform(128.6,18.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgTA7QgLgEgHgIQgHgIgEgLQgDgMAAgPQAAgOADgMQAEgMAHgIQAHgIALgEQAKgFALAAQAOAAAJAFQAKAEAGAHQAGAIADAJQACAKAAALIAAAEQAAAEgCACQgCACgEAAIhKAAQAAAKACAHQACAIAEAFQAFAGAHADQAHADAJAAIAPgCIALgDIAIgDIAEgBIACABIABABIAAACIABAEIAAADIgBACIAAACIgCABIgEADIgJADIgMADIgPABQgOAAgKgEgAgLgqQgGADgEAEQgEAFgCAHQgDAGAAAHIA9AAQABgRgIgJQgIgJgPAAQgHAAgFADg");
	this.shape_33.setTransform(108,20.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgbBUQgIgFgFgIQgGgIgDgLQgCgMAAgMQAAgPADgMQADgKAGgJQAHgIAJgFQAJgEAMAAQAJAAAIAEQAIAFAJAIIAAhCIAAgCIABgBIAEgBIAFAAIAFAAIADABIACABIAAACIAACoIAAABIgCACIgCAAIgFABIgEgBIgDAAIgCgCIAAgBIAAgPQgJAJgKAGQgKAFgKAAQgNAAgJgFgAgOgNQgGADgDAHQgDAFgCAIIgCAQIABARQACAIAEAGQADAHAFADQAGAEAHAAIAIgBIAIgEIAIgGIAKgLIAAguQgJgJgIgGQgIgFgIAAQgHAAgGAEg");
	this.shape_34.setTransform(94.4,18);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgFBYIgDgBIgBgBIAAgBIAAipIAAgBIABgCIADgBIAFAAIAFAAIADABIACACIAAABIAACpIAAABIgCABIgDABIgFABg");
	this.shape_35.setTransform(79.2,17.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgXA8QgHgCgFgEQgFgFgDgGQgDgHAAgIQAAgKAEgHQAEgHAHgEQAIgEAKgDQALgCANAAIAPAAIAAgJQAAgGgBgFQgCgGgDgDQgDgDgFgCQgFgCgHAAQgHAAgGACIgLAEIgIAEIgEACIgCAAIgBgCIgBgCIgBgEIABgFIADgDIAFgEIAKgEIAMgDIAMgCQAMAAAIADQAKADAFAFQAFAGADAIQACAIAAAKIAABMIgBACIgDACIgFAAIgGAAIgDgCIAAgCIAAgLQgIAIgJAEQgJAFgKAAQgJAAgHgDgAgGAHIgKAEQgEADgDAEQgCAEAAAFQAAAJAHAFQAFAFAKAAQAHAAAHgEQAIgEAHgJIAAgYIgSAAQgIAAgGACg");
	this.shape_36.setTransform(69.6,20.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgbA6QgHgEgFgGQgFgHgCgJQgCgIAAgNIAAhEIABgCIABgBIADgBIAFAAIAGAAIADABIABABIABACIAABBQAAAKABAHQACAFADAFQADAEAEACQAFADAGAAQAHAAAIgGQAHgFAJgLIAAhPIAAgCIACgBIADgBIAGAAIAEAAIAEABIACABIAAACIAABxIAAACIgCABIgDABIgEAAIgFAAIgDgBIgBgBIgBgCIAAgPQgKALgJAFQgKAFgJAAQgLAAgIgEg");
	this.shape_37.setTransform(57,20.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AAhA9IgDgBIgBgBIgBgBIAAhCQAAgKgBgFQgCgHgDgEQgDgEgFgCQgEgDgGAAQgHAAgIAGQgHAGgJAKIAABPIAAABIgDABIgDABIgFABIgFgBIgDgBIgCgBIAAgBIAAhxIAAgCIACgBIADgBIAEAAIAFAAIADABIABABIABACIAAAPQAKgLAJgFQAKgFAJAAQAKAAAJAEQAHAEAFAGQAFAHACAJQACAIAAAMIAABFIgBABIgBABIgDABIgFABg");
	this.shape_38.setTransform(43.4,20.6);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgXA8QgHgCgFgEQgFgFgDgGQgDgHAAgIQAAgKAEgHQAEgHAHgEQAIgEAKgDQALgCAMAAIAQAAIAAgJQAAgGgBgFQgCgGgDgDQgDgDgFgCQgFgCgHAAQgHAAgGACIgLAEIgIAEIgEACIgCAAIgBgCIgBgCIgBgEIABgFIADgDIAFgEIAKgEIAMgDIAMgCQAMAAAIADQAKADAFAFQAFAGADAIQACAIAAAKIAABMIgBACIgDACIgFAAIgGAAIgDgCIAAgCIAAgLQgIAIgJAEQgJAFgKAAQgJAAgHgDgAgGAHIgKAEQgEADgDAEQgCAEAAAFQAAAJAHAFQAFAFAKAAQAHAAAHgEQAIgEAHgJIAAgYIgSAAQgIAAgGACg");
	this.shape_39.setTransform(30,20.7);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("ABJBSIgDgBIgCgBIAAgCIAAiOIgBAAIg6CPIgBACIgCABIgDAAIgDABIgFgBIgDAAIgCgCIgBgBIg3iPIAACOIgBACIgCABIgDABIgGABIgFgBIgDgBIgCgBIAAgCIAAiVQAAgGACgCQADgDAEAAIAOAAIAGACIAGACIAEAEIADAGIAuB1IAAAAIAxh1IADgGIADgEIAFgCIAFgCIAPAAIADABIAEACIACADIAAAFIAACVIAAACIgCABIgEABIgFABg");
	this.shape_40.setTransform(13.1,18.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.titulo1, new cjs.Rectangle(0,0,530.7,35.7), null);


(lib.cabecera = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#54BB6F").s().p("EhKPAG0IAAtnMCUfAAAIAANng");
	this.shape.setTransform(475.2,43.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.cabecera, new cjs.Rectangle(0,0,950.4,87.3), null);


(lib.btnizq_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AghBSIgugtQgVgUAAgRQABgQAUgUIAuguQAHgHALAAQALAAAGAHQAIAJAAAKQAAALgIAHIgUAVQAOgCAbABIA0ACQAKAAAIAHQAIAIAAAKQAAALgIAHQgHAIgLAAIhTABIgFgBIAPARQAIAIAAALQAAALgIAHQgGAIgLAAQgLAAgHgIg");
	this.shape.setTransform(1.1,0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00A8AF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#66CCFF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#007074").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_2},{t:this.shape}]},1).to({state:[{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_3},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.8,-18.8,37.7,37.7);


(lib.btnizq_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AghBSIgugtQgVgUAAgRQABgQAUgUIAuguQAHgHALAAQALAAAGAHQAIAJAAAKQAAALgIAHIgUAVQAOgCAbABIA0ACQAKAAAIAHQAIAIAAAKQAAALgIAHQgHAIgLAAIhTABIgFgBIAPARQAIAIAAALQAAALgIAHQgGAIgLAAQgLAAgHgIg");
	this.shape.setTransform(1.1,0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00A8AF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#66CCFF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#007074").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_2},{t:this.shape}]},1).to({state:[{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_3},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.8,-18.8,37.7,37.7);


(lib.btnizq_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AghBSIgugtQgVgUAAgRQABgQAUgUIAuguQAHgHALAAQALAAAGAHQAIAJAAAKQAAALgIAHIgUAVQAOgCAbABIA0ACQAKAAAIAHQAIAIAAAKQAAALgIAHQgHAIgLAAIhTABIgFgBIAPARQAIAIAAALQAAALgIAHQgGAIgLAAQgLAAgHgIg");
	this.shape.setTransform(1.1,0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00A8AF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#66CCFF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#007074").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_2},{t:this.shape}]},1).to({state:[{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_3},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.8,-18.8,37.7,37.7);


(lib.btnizq_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AghBSIgugtQgVgUAAgRQABgQAUgUIAuguQAHgHALAAQALAAAGAHQAIAJAAAKQAAALgIAHIgUAVQAOgCAbABIA0ACQAKAAAIAHQAIAIAAAKQAAALgIAHQgHAIgLAAIhTABIgFgBIAPARQAIAIAAALQAAALgIAHQgGAIgLAAQgLAAgHgIg");
	this.shape.setTransform(1.1,0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00A8AF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#66CCFF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#007074").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_2},{t:this.shape}]},1).to({state:[{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_3},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.8,-18.8,37.7,37.7);


(lib.btnizq_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AghBSIgugtQgVgUAAgRQABgQAUgUIAuguQAHgHALAAQALAAAGAHQAIAJAAAKQAAALgIAHIgUAVQAOgCAbABIA0ACQAKAAAIAHQAIAIAAAKQAAALgIAHQgHAIgLAAIhTABIgFgBIAPARQAIAIAAALQAAALgIAHQgGAIgLAAQgLAAgHgIg");
	this.shape.setTransform(1.1,0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00A8AF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#66CCFF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#007074").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_2},{t:this.shape}]},1).to({state:[{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_3},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.8,-18.8,37.7,37.7);


(lib.btnder_5copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgBBSQgIgHAAgLQAAgLAIgIIAPgRIgFABIhTgBQgLAAgHgIQgIgHAAgLQAAgKAIgIQAIgHAKAAIA0gCQAbgBAOACIgUgVQgIgHAAgLQAAgKAIgJQAGgHALAAQALAAAHAHIAuAuQAUAUABAQQAAARgVAUIguAtQgHAIgLAAQgLAAgGgIg");
	this.shape.setTransform(-1.1,0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#235B9F").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#66CCFF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA4A3AABNQAABOg4A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#007074").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA4A3AABNQAABOg4A3Qg3A3hOAAQhNAAg3g3g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_2},{t:this.shape}]},1).to({state:[{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_3},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.8,-18.8,37.7,37.7);


(lib.btnder_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgBBSQgIgHAAgLQAAgLAIgIIAPgRIgFABIhTgBQgLAAgHgIQgIgHAAgLQAAgKAIgIQAIgHAKAAIA0gCQAbgBAOACIgUgVQgIgHAAgLQAAgKAIgJQAGgHALAAQALAAAHAHIAuAuQAUAUABAQQAAARgVAUIguAtQgHAIgLAAQgLAAgGgIg");
	this.shape.setTransform(-1.1,0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00A8AF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#66CCFF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA4A3AABNQAABOg4A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#007074").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA4A3AABNQAABOg4A3Qg3A3hOAAQhNAAg3g3g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_2},{t:this.shape}]},1).to({state:[{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_3},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.8,-18.8,37.7,37.7);


(lib.btnder_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgBBSQgIgHAAgLQAAgLAIgIIAPgRIgFABIhTgBQgLAAgHgIQgIgHAAgLQAAgKAIgIQAIgHAKAAIA0gCQAbgBAOACIgUgVQgIgHAAgLQAAgKAIgJQAGgHALAAQALAAAHAHIAuAuQAUAUABAQQAAARgVAUIguAtQgHAIgLAAQgLAAgGgIg");
	this.shape.setTransform(-1.1,0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00A8AF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#66CCFF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA4A3AABNQAABOg4A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#007074").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA4A3AABNQAABOg4A3Qg3A3hOAAQhNAAg3g3g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_2},{t:this.shape}]},1).to({state:[{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_3},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.8,-18.8,37.7,37.7);


(lib.btnder_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgBBSQgIgHAAgLQAAgLAIgIIAPgRIgFABIhTgBQgLAAgHgIQgIgHAAgLQAAgKAIgIQAIgHAKAAIA0gCQAbgBAOACIgUgVQgIgHAAgLQAAgKAIgJQAGgHALAAQALAAAHAHIAuAuQAUAUABAQQAAARgVAUIguAtQgHAIgLAAQgLAAgGgIg");
	this.shape.setTransform(-1.1,0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00A8AF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#66CCFF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA4A3AABNQAABOg4A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#007074").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA4A3AABNQAABOg4A3Qg3A3hOAAQhNAAg3g3g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_2},{t:this.shape}]},1).to({state:[{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_3},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.8,-18.8,37.7,37.7);


(lib.btnder_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgBBSQgIgHAAgLQAAgLAIgIIAPgRIgFABIhTgBQgLAAgHgIQgIgHAAgLQAAgKAIgIQAIgHAKAAIA0gCQAbgBAOACIgUgVQgIgHAAgLQAAgKAIgJQAGgHALAAQALAAAHAHIAuAuQAUAUABAQQAAARgVAUIguAtQgHAIgLAAQgLAAgGgIg");
	this.shape.setTransform(-1.1,0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00A8AF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#66CCFF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA4A3AABNQAABOg4A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#007074").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA4A3AABNQAABOg4A3Qg3A3hOAAQhNAAg3g3g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_2},{t:this.shape}]},1).to({state:[{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_3},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.8,-18.8,37.7,37.7);


(lib.btnder_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgBBSQgIgHAAgLQAAgLAIgIIAPgRIgFABIhTgBQgLAAgHgIQgIgHAAgLQAAgKAIgIQAIgHAKAAIA0gCQAbgBAOACIgUgVQgIgHAAgLQAAgKAIgJQAGgHALAAQALAAAHAHIAuAuQAUAUABAQQAAARgVAUIguAtQgHAIgLAAQgLAAgGgIg");
	this.shape.setTransform(-1.1,0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00A8AF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA4A3AABNQAABOg4A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#66CCFF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA4A3AABNQAABOg4A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00A8AF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#007074").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA4A3AABNQAABOg4A3Qg3A3hOAAQhNAAg3g3g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_2},{t:this.shape}]},1).to({state:[{t:this.shape_3},{t:this.shape}]},1).to({state:[{t:this.shape_4},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.8,-18.8,37.7,37.7);


(lib.bregresarmenu = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#54BB6F").s().p("AATCBIAAg5IglAAIAAA5IhfAAIAAhfIgmAAIAAgKICXiYIA5A6IAAgwIAmAAIAABWIA6A4IAAAKIgnAAIAABfg");
	this.shape.setTransform(-0.2,-1.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AATCBIAAg5IglAAIAAA5IhfAAIAAhfIgmAAIAAgKICXiYIA5A6IAAgwIAmAAIAABWIA6A4IAAAKIgnAAIAABfg");
	this.shape_1.setTransform(-0.2,-1.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAPBoIAAguIgeAAIAAAuIhMAAIAAhNIgfAAIAAgHIB6h7IAuAvIAAgnIAfAAIAABFIAuAuIAAAHIgeAAIAABNg");
	this.shape_2.setTransform(-0.2,-1.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).wait(2));

	// Capa 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#66CCFF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#54BB6F").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#00A8AF").s().p("AiECFQg3g3AAhOQAAhNA3g3QA3g3BNAAQBOAAA3A3QA3A3AABNQAABOg3A3Qg3A3hOAAQhNAAg3g3g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.8,-18.8,37.7,37.7);


(lib.bg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A7B8C6").s().p("EgpUAATIAAglMBSpAAAIAAAlg");
	this.shape.setTransform(738.6,235.1,1.195,1.196);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A7B8C6").s().p("EgpUAATIAAglMBSpAAAIAAAlg");
	this.shape_1.setTransform(738.6,72.8,1.195,1.196);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#A7B8C6").s().p("AgRZPMAAAgyeIAjAAMAAAAyeg");
	this.shape_2.setTransform(633.9,180.7,1.195,1.312);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#A7B8C6").s().p("AgRZRMAAAgyhIAjAAMAAAAyhg");
	this.shape_3.setTransform(843.2,182.4,1.195,1.324);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#A7B8C6").s().p("EgpUAATIAAglMBSpAAAIAAAlg");
	this.shape_4.setTransform(235.1,235.1,1.195,1.196);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#A7B8C6").s().p("EgpUAATIAAglMBSpAAAIAAAlg");
	this.shape_5.setTransform(235.1,72.8,1.195,1.196);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#A7B8C6").s().p("AgRb5MAAAg3xIAjAAMAAAA3xg");
	this.shape_6.setTransform(338.7,182.3,1.195,1.189);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#A7B8C6").s().p("AgRZRMAAAgyhIAjAAMAAAAyhg");
	this.shape_7.setTransform(130.5,179.9,1.195,1.298);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#BCCDDD").s().p("EhKQAUuIAAhsIEEAAIAAhSIkEAAIAAhUIEEAAIAAhTIkEAAIAAmfIEEAAIAAhSIkEAAIAAkkIFNAAIAAyyIMXAAIAALqIIVlxIEMCNIAAlEILPnxILPHxIAAIaIEKljIIXFxIAADfIBUAAIAAibIF8AAIAAhwQAAgQADgOIAjhPQAmgvA3gFIAAhRQgIgDgHgKQgFgJgBgNQAAgPAKgLQAJgLANAAQAPAAAJALQAKALAAAPQAAANgHAJQgFAKgKADIAABRQA3AFAnAvIAkBPQACAOAAAQIAABwIF7AAIAACbIBVAAIAAjfIIWlxIEMFjIAAoaILPnxILOHxIAAFEIELiNIIWFxIAArqIMXAAIAASyIEnAAIAAEkIjeAAIAABSIDeAAIAAGfIjeAAIAABTIDeAAIAABUIjeAAIAABSIDeAAIAABsgEBDUATCIA/AAIAAisIg/AAgEA6pATCIHfAAIAAisInfAAgAC3TCIBhAAIAAisIhhAAgAjzTCIBiAAIAAisIhiAAgEhBiATCIHfAAIAAisInfAAgEhDuATCIA/AAIAAisIg/AAgAKsQPIAACxIAVgBIAWgBIAAi0gAqxS+IAqACIAAixIgqgFgEA2QAS5IAVABIAWABIAAi3IgrgGgAMLQEIAAC3IAVgBIAVgBIAAi7gAsPS5IAVABIAWABIAAi3IgrgGgEg2VAQEIAAC3IAVgBIAVgBIAAi7gEAzUASwIAVABIAWABIAAjFIgrgFgEgzZAPtIAADFIAqgCIAAjIgEAx3ASsIAqACIAAjMIgqgFgAQkPiIAADMIAqgCIAAjPgAwoSsIAqACIAAjMIgqgFgEgx7APiIAADMIAqgCIAAjPgEAqpASWIA6ADIAAjtIg6gHgAXiOsIAADtIA6gDIAAjxgA32SWIA6ADIAAjtIg6gHgEgq9AOsIAADtIA5gDIAAjxgEAmtASLIA5ACIAAj/Ig5gHgAbeOOIAAD/IA6gCIAAkEgA7ySLIA6ACIAAj/Ig6gHgEgnAAOOIAAD/IA5gCIAAkEgEAixAR/IA5ADIAAkTIg5gGgAfaNvIAAETIA6gDIAAkWgA/uR/IA5ADIAAkTIg5gGgEgjEANvIAAETIA5gDIAAkWgEA+/APMIFUAAIAAisIlUAAgEA6pAPMIA/AAIAAisIg/AAgAGMPMIBiAAIAAisIhiAAgAnIPMIBhAAIAAisIhhAAgEg7CAPMIA/AAIAAisIg/AAgEhDuAPMIFVAAIAAisIlVAAgEA3tAO8IArAHIAAixIgrgJgAKsMSIAACxIArgHIAAizgAqxO8IAqAHIAAixIgqgJgEg3zAMSIAACxIArgHIAAizgEA2QAOuIArAGIAAi3IgrgJgEg2VAL9IAAC3IAqgGIAAi6gANoLoIAAC+IArgGIAAjCgAtsOgIAqAGIAAi+IgqgKgEAzUAORIArAHIAAjFIgrgKgAPFLTIAADFIArgHIAAjIgAvLORIArAHIAAjFIgrgKgEgzZALTIAADFIAqgHIAAjIgEAx3AODIAqAGIAAjMIgqgJgAQkK9IAADMIAqgGIAAjPgAwoODIAqAGIAAjMIgqgJgEgx7AK9IAADMIAqgGIAAjPgEAqpAM8IA6AJIAAjtIg6gNgAXiJYIAADtIA6gJIAAjxgA32M8IA6AJIAAjtIg6gNgEgq9AJYIAADtIA5gJIAAjxgEAorAMpIA6AJIAAj3Ig6gMgAZgI7IAAD3IA6gJIAAj6gA50MpIA6AJIAAj3Ig6gMgEgo/AI7IAAD3IA6gJIAAj6gAbeIfIAAD/IA6gIIAAkEgA7yMWIA6AIIAAj/Ig6gNgEAkvAMCIA6AJIAAkIIg6gOgEglCAIDIAAEIIA5gJIAAkNgEBBJALUIA/AAIAAirIg/AAgEA6pALUIDKAAIAAirIjKAAgAC3LUIBhAAIAAirIhhAAgAgdLUIBgAAIAAirIhgAAgAjzLUIBiAAIAAirIhiAAgEg9NALUIDKAAIAAirIjKAAgEhBiALUIA+AAIAAirIg+AAgAKsIWIAACwIArgLIAAi0gAqxK7IAqALIAAiwIgqgPgEA2QAKjIArALIAAi3IgrgOgAMLH3IAAC3IAqgLIAAi6gAsPKjIArALIAAi3IgrgOgEg2VAH3IAAC3IAqgLIAAi6gEA0yAKKIAqALIAAi+IgqgOgEg03AHXIAAC+IArgLIAAjBgEAzUAJyIArALIAAjFIgrgOgEgzZAG4IAADFIAqgLIAAjIgEAx3AJaIAqALIAAjMIgqgOgAQkGZIAADMIAqgLIAAjPgAwoJaIAqALIAAjMIgqgOgEgx7AGZIAADMIAqgLIAAjPgEAqpAHiIA6AQIAAjuIg6gSgAXiEEIAADuIA6gQIAAjwgA32HiIA6AQIAAjuIg6gSgEgq9AEEIAADuIA5gQIAAjwgEBDUAHeIA/AAIAAirIg/AAgEA80AHeIFUAAIAAirIlUAAgAC3HeIBhAAIAAirIhhAAgAjzHeIBiAAIAAirIhiAAgEhBiAHeIFUAAIAAirIlUAAgEhDuAHeIA/AAIAAirIg/AAgEA3tAG6IArAPIAAiwIgrgSgAKsEZIAACwIArgPIAAizgAqxG6IAqAPIAAiwIgqgSgEg3zAEZIAACwIArgPIAAizgAbeCwIAAEAIA6gPIAAkEgA7yGhIA6APIAAkAIg6gTgAMLDwIAAC3IAqgPIAAi7gAsPGYIArAPIAAi3IgrgTgEAkvAGAIA6APIAAkJIg6gSgAdcCGIAAEJIA6gPIAAkMgA9wGAIA5APIAAkJIg5gSgEglCACGIAAEJIA5gPIAAkMgEA0yAF1IAqAQIAAi+IgqgTgANoDHIAAC+IArgQIAAjBgAtsF1IAqAQIAAi+IgqgTgEg03ADHIAAC+IArgQIAAjBgEAixAFgIA5AOIAAkRIg5gUgAfaBdIAAERIA6gOIAAkXgA/uFgIA5AOIAAkRIg5gUgEgjEABdIAAERIA5gOIAAkXgAPFCdIAADFIArgQIAAjHgAvLFSIArAQIAAjFIgrgSgEAx3AExIAqAPIAAjLIgqgTgEgx7AB1IAADLIAqgPIAAjPgEBDUADnIA/AAIAAirIg/AAgEA6pADnIFVAAIAAirIlVAAgAGMDnIBiAAIAAirIhiAAgAnIDnIBhAAIAAirIhhAAgEg/YADnIFVAAIAAirIlVAAgEhDuADnIA/AAIAAirIg/AAgEA3tAC4IArAUIAAiwIgrgXgEg3zAAcIAACwIArgUIAAizgAMLgWIAAC2IAqgUIAAi5gAsPCMIArAUIAAi2IgrgXgEAqpACJIA6AUIAAjrIg6gagAXihOIAADrIA6gUIAAjxgA32CJIA6AUIAAjrIg6gagEgq9gBOIAADrIA5gUIAAjxgANohJIAAC9IArgUIAAjAgAtsBgIAqAUIAAi9IgqgXgEAorABaIA6AVIAAj1Ig6gZgEgo/gCGIAAD1IA6gVIAAj5gEAzUAAzIArAVIAAjEIgrgYgAPFh8IAADEIArgVIAAjHgAvLAzIArAVIAAjEIgrgYgEgzZgB8IAADEIAqgVIAAjHgEAx3AAIIAqAUIAAjMIgqgWgAQkiwIAADMIAqgUIAAjOgAwoAIIAqAUIAAjMIgqgWgEgx7gCwIAADMIAqgUIAAjOgAdcj1IAAEIIA6gUIAAkMgA9wgBIA5AUIAAkIIg5gYgEA80gAOIHfAAIAAisInfAAgEA6pgAOIA/AAIAAisIg/AAgEg7CgAOIA/AAIAAisIg/AAgEhDugAOIHgAAIAAisIngAAgEAixgAvIA5AVIAAkSIg5gagEgjEgEsIAAESIA5gVIAAkXgEA3tgBIIArAYIAAivIgrgcgAKsjfIAACvIArgYIAAizgAqxhIIAqAYIAAivIgqgcgEg3zgDfIAACvIArgYIAAizgEA0ygC0IAqAYIAAi+IgqgcgEg03gFaIAAC+IArgYIAAjCgEAqpgDRIA6AbIAAjtIg6gfgEgq9gGjIAADtIA5gbIAAjxgAgalzQgSAVAAAdIAABxIB+AAIAAhxQAAgdgTgVQgSgUgbgBQgYABgUAUgAPFmXIAADFIArgYIAAjJgAvLjqIArAYIAAjFIgrgcgAZgnoIAAD3IA6gbIAAj6gA50kMIA6AbIAAj3Ig6gegEA+/gEEIA/AAIAAisIg/AAgEA6pgEEIDKAAIAAisIjKAAgEg9NgEEIDKAAIAAisIjKAAgEg/YgEEIA/AAIAAisIg/AAgEAx3gEgIAqAYIAAjMIgqgbgEgx7gHUIAADMIAqgYIAAjPgEAmtgFIIA5AbIAAj/Ig5gggAbeosIAAD/IA6gbIAAkEgA7ylIIA6AbIAAj/Ig6gggEgnAgIsIAAD/IA5gbIAAkEgEAkvgGDIA6AbIAAkKIg6gegEglCgJyIAAEKIA5gbIAAkNgAfaq2IAAESIA6gbIAAkWgA/um/IA5AbIAAkSIg5gfgEBDUgH7IA/AAIAAisIg/AAgEA80gH7IEVAAIAAisIkVAAgEA6pgH7IA/AAIAAisIg/AAgEg7CgH7IA/AAIAAisIg/AAgEhAkgH7IEWAAIAAisIkWAAgEhDugH7IA/AAIAAisIg/AAgEAqpgIrIA6AiIAAjuIg6glgAXir3IAADuIA6giIAAjxgA32orIA6AiIAAjuIg6glgEgq9gL3IAADuIA5giIAAjxgEAmtgK8IA5AgIAAj/Ig5glgEgnAgObIAAD/IA5ggIAAkEgAdcvtIAAEIIA6ggIAAkNgA9wsFIA5AgIAAkIIg5glgEBDUgLxIA/AAIAAisIg/AAgEA6pgLxIFVAAIAAisIlVAAgEg/YgLxIFVAAIAAisIlVAAgEhDugLxIA/AAIAAisIg/AAg");
	this.shape_8.setTransform(487.3,236.7,1.196,1.196);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#7E90A1").s().p("EgpUABfIAAi9MBSpAAAIAAC9g");
	this.shape_9.setTransform(487.6,396.4,2.149,1.196);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#D3E2F2").s().p("EhY5A4GMAAAhwLMCxzAAAMAAABwLg");
	this.shape_10.setTransform(486.5,328.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.bg, new cjs.Rectangle(-82.5,-31.7,1138.6,719.6), null);


// stage content:
(lib.menu2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{letrero2:1,uno:2,dos:3,tres:4,cuatro:5,cinco:6,seis:7});

	// timeline functions:
	this.frame_0 = function() {
		/*TweenLite.from(this.logooit, 1, {x:1500, ease:Back.easeOut, delay:0.1});*/
		this.root= this
		this.stop();
		
			this.logooit.y = 1500;
			createjs.Tween.get(this.logooit).wait(1).to({y:322.75}, 1000, createjs.Ease.getPowInOut(4));
			
			createjs.Tween.get(this).wait(4000).call(irasiguiente);
			function irasiguiente(e){
			this.gotoAndStop("letrero2");
			}
	}
	this.frame_1 = function() {
		/*TweenLite.to(this.logooit, 1, {y:332, alpha:0, ease:Back.easeOut, delay:0.1});*/
		this.stop();
		
			this.logooit.y = 322.75;
			createjs.Tween.get(this.logooit).wait(1).to({y:1500}, 1000, createjs.Ease.getPowInOut(4));
			
			createjs.Tween.get(this).wait(1000).call(irasiguiente2);
			function irasiguiente2(e){
			
			this.gotoAndStop("uno");
				
			}
	}
	this.frame_2 = function() {
		this.stop();
		root = this
		
		
		
		this.bregresarmenu1.addEventListener("click", CargarContenido.bind(this));
		this.bregresarmenu1.addEventListener("click", detenervideo1.bind(this));
		
		
		this.btnder_1.addEventListener("click", fl_ClickToGoToAndStopAtFrame_143.bind(this));
		function fl_ClickToGoToAndStopAtFrame_143(root) {
			this.gotoAndStop("dos");
		}
	}
	this.frame_3 = function() {
		this.stop();
		
		/*TweenLite.from(this.texto2, 1, {y:347, alpha:0, ease:Back.easeOut, delay:0.2});*/
		
		
		this.btnizq_2.addEventListener("click", fl_ClickToGoToAndStopAtFrame_1422.bind(this));
		function fl_ClickToGoToAndStopAtFrame_1422()
		{
			this.gotoAndStop("uno");
		}
		
		
		this.btnder_2.addEventListener("click", fl_ClickToGoToAndStopAtFrame_1432.bind(this));
		function fl_ClickToGoToAndStopAtFrame_1432()
		{
			this.gotoAndStop("tres");
		}
		
		
		this.bregresarmenu2.addEventListener("click", CargarContenido.bind(this));
		this.bregresarmenu2.addEventListener("click", detenervideo2.bind(this));
	}
	this.frame_4 = function() {
		this.btnizq_3.addEventListener("click", fl_ClickToGoToAndStopAtFrame_1423.bind(this));
		function fl_ClickToGoToAndStopAtFrame_1423()
		{
			this.gotoAndStop("dos");
		}
		
		
		this.btnder_3.addEventListener("click", fl_ClickToGoToAndStopAtFrame_1433.bind(this));
		function fl_ClickToGoToAndStopAtFrame_1433()
		{
			this.gotoAndStop("cuatro");
		}
		
		this.stop();
		root = this
		
		this.bregresarmenu3.addEventListener("click", CargarContenido.bind(this));
		this.bregresarmenu3.addEventListener("click", detenervideo3.bind(this));
	}
	this.frame_5 = function() {
		this.btnizq_4.addEventListener("click", fl_ClickToGoToAndStopAtFrame_1424.bind(this));
		function fl_ClickToGoToAndStopAtFrame_1424()
		{
			this.gotoAndStop("tres");
		}
		
		
		this.btnder_4.addEventListener("click", fl_ClickToGoToAndStopAtFrame_1434.bind(this));
		function fl_ClickToGoToAndStopAtFrame_1434()
		{
			this.gotoAndStop("cinco");
		}
		
		
		this.stop();
		root = this
		
		this.bregresarmenu4.addEventListener("click", CargarContenido.bind(this));
		this.bregresarmenu4.addEventListener("click", detenervideo4.bind(this));
	}
	this.frame_6 = function() {
		this.btnizq_5.addEventListener("click", fl_ClickToGoToAndStopAtFrame_1425.bind(this));
		function fl_ClickToGoToAndStopAtFrame_1425()
		{
			this.gotoAndStop("cuatro");
		}
		
		
		this.btnder_5.addEventListener("click", fl_ClickToGoToAndStopAtFrame_1435.bind(this));
		function fl_ClickToGoToAndStopAtFrame_1435()
		{
			this.gotoAndStop("seis");
		}
		
		
		this.stop();
		root = this
		
		this.bregresarmenu5.addEventListener("click", CargarContenido.bind(this));
		this.bregresarmenu5.addEventListener("click", detenervideo5.bind(this));
	}
	this.frame_7 = function() {
		this.btnizq_6.addEventListener("click", fl_ClickToGoToAndStopAtFrame_1426.bind(this));
		function fl_ClickToGoToAndStopAtFrame_1426()
		{
			this.gotoAndStop("cinco");
		}
		
		
		
		this.btnder_6.addEventListener("click", CargarContenido.bind(this));	
		this.btnder_6.addEventListener("click", detenervideo6.bind(this));	
		
		
		this.stop();
		root = this
		
		
		this.bregresarmenu6.addEventListener("click", CargarContenido.bind(this));
		this.bregresarmenu6.addEventListener("click", detenervideo6.bind(this));
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1).call(this.frame_2).wait(1).call(this.frame_3).wait(1).call(this.frame_4).wait(1).call(this.frame_5).wait(1).call(this.frame_6).wait(1).call(this.frame_7).wait(1));

	// Capa 1
	this.instance = new lib.logo_ministerio_1();
	this.instance.parent = this;
	this.instance.setTransform(225,-167);

	this.logooit = new lib.logooit();
	this.logooit.name = "logooit";
	this.logooit.parent = this;
	this.logooit.setTransform(475.2,322.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.logooit},{t:this.instance}]}).to({state:[]},2).wait(6));

	// Capa_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#54BB6F").s().p("EhKPAu4MAAAhdvMCUfAAAMAAABdvg");
	this.shape.setTransform(475.2,300);

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},2).wait(6));

	// Layer 1 copia
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(84,187,111,0.4)").s().p("AAJEIIAAlrIh7AAIAAhHQAxgCAUgEQAfgHASgVQAOgOAHgYIAEgVIBWAAIAAIPg");
	this.shape_1.setTransform(822.6,39.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AizEJQABg4AWgwQAXg1BTg6QBGgyAVgXQAggiABgqQAAghgTgWQgSgWgjAAQgugBgRAkQgLAUgBAtIhlAAQAChDAXgqQAphPBqAAQBTAAAxAuQAyAvAABMQAAA6gjAtQgXAeg1AmIgpAdIg0AoQgPAMgKARIDmAAIAABbg");
	this.shape_2.setTransform(826.3,39.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AicDHQgXgngChAIBmAAQAAAhAKAUQATAmAxAAQAeAAAXgUQAWgWAAgnQAAg0gqgRQgYgKgyAAIAAhKQAxAAAUgJQAjgPAAgvQAAgegSgTQgSgTgfAAQgkAAgSAXQgRAYABAmIhhAAQACgnAMgjQANgfAbgaQAUgTAcgKQAcgJApgBQBKAAAvAoQAuAmAABCQAAAvgcAgQgRAUgTAHQAOABAbAXQAnAlAAA/QAABDguAzQguAyhaABQhwAAgshJg");
	this.shape_3.setTransform(826.1,40.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAYEGIAAhyIjQAAIAAhaIDBk/IB0AAIAAFIIA8AAIAABRIg8AAIAABygAhrBDICDAAIAAjig");
	this.shape_4.setTransform(826.4,39.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ah/DjQgxgpgFhKIBnAAQAGAiASASQARATAiAAQAlAAAUgbQAUgcAAgpQAAgogTgcQgTgbgmAAQgTAAgOAFQgYAIgNAXIhdgEIAlkiIEiAAIAABYIjXAAIgTBzIAmgVQAWgIAgAAQBBAAAxAsQAwAsAABTQAABJgvA6QguA5hdAAQhLAAgwgog");
	this.shape_5.setTransform(826.1,40.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AiRC+QgkhBAAhnQAAg8AFglQAJhEAagtQAWgmAlgXQAlgXAyAAQBJAAAsAmQAsAlAFA/IhnAAQAAgNgKgPQgRgZgiAAQgxAAgWA5QgLAfgFA9QAUgXAZgKQAagLAfAAQBGAAAuAwQAsAuAABKQAABKgsA5QgsA4hdAAQhiAAgwhTgAguAQQgfAYAAA0QAAArAWAbQAXAbAjAAQAjAAAUgaQATgaAAgqQAAgugXgYQgWgZggAAQgbAAgTAQg");
	this.shape_6.setTransform(826.5,40);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_1}]},2).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).wait(1));

	// Layer 1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(27,170,176,0.4)").s().p("AAjQRIAA2bInoAAIAAkXQDBgIBOgRQB6gbBNhTQA1g4AbhdIAQhTIFVAAMAAAAghg");
	this.shape_7.setTransform(57.5,489.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(84,187,111,0.4)").s().p("ArIQWQAGjgBai5QBYjRFGjoQEajJBThYQCAiIAAiiQAAiEhJhZQhKhXiIAAQi6AAhDCMQgnBQgHCvImOAAQAKkKBWijQCkk4GiAAQFKAADEC4QDDC2AAEtQAADoiKCzQhaB3jQCSIikB0IjRCeQg5AxgnBAIOMAAIAAFpg");
	this.shape_8.setTransform(72.1,489.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(84,187,111,0.4)").s().p("ApqMPQhaiZgKj5IGSAAQAAB9AoBSQBLCXDDAAQB4AABahSQBZhTAAibQAAjOinhFQhdgmjLAAIAAkkQDHgDBNgkQCJg8AAi4QAAh3hFhLQhHhLh9AAQiPAAhFBcQhEBcADCaIl+AAQAGibAviMQAyh6BshnQBRhJBugnQBvgnCgAAQEpAAC3CbQC3CaAAEEQAAC4htB+QhFBPhLAdQA4AABpBfQCcCRAAD7QAAEIi2DJQi4DIlkAAQm6AAitkhg");
	this.shape_9.setTransform(71.3,492);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(84,187,111,0.4)").s().p("ABeQLIAAnBIs3AAIAAllIL9zvIHLAAIAAUVIDrAAIAAE/IjrAAIAAHBgAmoELIIGAAIAAuBg");
	this.shape_10.setTransform(72.4,490.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("rgba(84,187,111,0.4)").s().p("An6N/Qi/ifgVklIGYAAQAYCFBFBKQBFBICEAAQCWAABQhrQBPhsAAihQAAighLhtQhKhuicAAQhKAAg3ATQhgAigxBdIlwgSICTx9IR5AAIAAFcItRAAIhLHHICUhSQBZghB/AAQEBAADACuQDACuAAFKQAAEhi5DiQi5DklvAAQkpAAi/igg");
	this.shape_11.setTransform(71.3,494);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(84,187,111,0.4)").s().p("Ao9LuQiRkBAAmXQAAjtAUiVQAkkKBnixQBZiWCRhcQCQhcDIAAQEiAACsCVQCsCUAWD3ImYAAQAAgygng9QhChhiFAAQjHAAhVDgQguB7gRDxQBMhaBlgqQBkgpCBAAQEVAACyC8QCwC7AAElQAAEjitDfQiuDeluAAQmJAAi6lIgAi7A/Qh5BeAADQQAACoBbBqQBaBqCKAAQCJAABPhmQBNhnAAikQAAi2hZhgQhZhiiAAAQhpAAhQA/g");
	this.shape_12.setTransform(72.8,491.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_7}]},2).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).wait(1));

	// Capa_7
	this.bregresarmenu1 = new lib.bregresarmenu();
	this.bregresarmenu1.name = "bregresarmenu1";
	this.bregresarmenu1.parent = this;
	this.bregresarmenu1.setTransform(915.2,42.5);
	new cjs.ButtonHelper(this.bregresarmenu1, 0, 1, 2, false, new lib.bregresarmenu(), 3);

	this.bregresarmenu2 = new lib.bregresarmenu();
	this.bregresarmenu2.name = "bregresarmenu2";
	this.bregresarmenu2.parent = this;
	this.bregresarmenu2.setTransform(915.2,42.5);
	new cjs.ButtonHelper(this.bregresarmenu2, 0, 1, 2, false, new lib.bregresarmenu(), 3);

	this.bregresarmenu3 = new lib.bregresarmenu();
	this.bregresarmenu3.name = "bregresarmenu3";
	this.bregresarmenu3.parent = this;
	this.bregresarmenu3.setTransform(915.2,42.5);
	new cjs.ButtonHelper(this.bregresarmenu3, 0, 1, 2, false, new lib.bregresarmenu(), 3);

	this.bregresarmenu4 = new lib.bregresarmenu();
	this.bregresarmenu4.name = "bregresarmenu4";
	this.bregresarmenu4.parent = this;
	this.bregresarmenu4.setTransform(915.2,42.5);
	new cjs.ButtonHelper(this.bregresarmenu4, 0, 1, 2, false, new lib.bregresarmenu(), 3);

	this.bregresarmenu5 = new lib.bregresarmenu();
	this.bregresarmenu5.name = "bregresarmenu5";
	this.bregresarmenu5.parent = this;
	this.bregresarmenu5.setTransform(915.2,42.5);
	new cjs.ButtonHelper(this.bregresarmenu5, 0, 1, 2, false, new lib.bregresarmenu(), 3);

	this.bregresarmenu6 = new lib.bregresarmenu();
	this.bregresarmenu6.name = "bregresarmenu6";
	this.bregresarmenu6.parent = this;
	this.bregresarmenu6.setTransform(915.2,42.5);
	new cjs.ButtonHelper(this.bregresarmenu6, 0, 1, 2, false, new lib.bregresarmenu(), 3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.bregresarmenu1}]},2).to({state:[{t:this.bregresarmenu2}]},1).to({state:[{t:this.bregresarmenu3}]},1).to({state:[{t:this.bregresarmenu4}]},1).to({state:[{t:this.bregresarmenu5}]},1).to({state:[{t:this.bregresarmenu6}]},1).wait(1));

	// Capa_6
	this.titulo2 = new lib.titulo2();
	this.titulo2.name = "titulo2";
	this.titulo2.parent = this;
	this.titulo2.setTransform(108.3,59.5,1,1,0,0,0,88.8,17.8);

	this.titulo1 = new lib.titulo1();
	this.titulo1.name = "titulo1";
	this.titulo1.parent = this;
	this.titulo1.setTransform(285.4,32.5,1,1,0,0,0,265.9,17.8);

	this.cabecera = new lib.cabecera();
	this.cabecera.name = "cabecera";
	this.cabecera.parent = this;
	this.cabecera.setTransform(475.2,43.6,1,1,0,0,0,475.2,43.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.cabecera},{t:this.titulo1},{t:this.titulo2}]},2).wait(6));

	// Capa_9
	this.btnder_1 = new lib.btnder_1();
	this.btnder_1.name = "btnder_1";
	this.btnder_1.parent = this;
	this.btnder_1.setTransform(922.3,575.4,0.828,0.827);
	this.btnder_1.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(39, -9, 1, -70))];
	this.btnder_1.cache(-21,-21,42,42);
	new cjs.ButtonHelper(this.btnder_1, 0, 1, 2, false, new lib.btnder_1(), 3);

	this.btnizq_2 = new lib.btnizq_2();
	this.btnizq_2.name = "btnizq_2";
	this.btnizq_2.parent = this;
	this.btnizq_2.setTransform(886.9,575.6,0.829,0.829);
	this.btnizq_2.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(39, -9, 1, -70))];
	this.btnizq_2.cache(-21,-21,42,42);
	new cjs.ButtonHelper(this.btnizq_2, 0, 1, 2, false, new lib.btnizq_2(), 3);

	this.btnder_2 = new lib.btnder_2();
	this.btnder_2.name = "btnder_2";
	this.btnder_2.parent = this;
	this.btnder_2.setTransform(922.3,575.6,0.828,0.827);
	this.btnder_2.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(39, -9, 1, -70))];
	this.btnder_2.cache(-21,-21,42,42);
	new cjs.ButtonHelper(this.btnder_2, 0, 1, 2, false, new lib.btnder_2(), 3);

	this.btnizq_3 = new lib.btnizq_3();
	this.btnizq_3.name = "btnizq_3";
	this.btnizq_3.parent = this;
	this.btnizq_3.setTransform(886.9,575.6,0.829,0.829);
	this.btnizq_3.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(39, -9, 1, -70))];
	this.btnizq_3.cache(-21,-21,42,42);
	new cjs.ButtonHelper(this.btnizq_3, 0, 1, 2, false, new lib.btnizq_3(), 3);

	this.btnder_3 = new lib.btnder_3();
	this.btnder_3.name = "btnder_3";
	this.btnder_3.parent = this;
	this.btnder_3.setTransform(922.3,575.6,0.828,0.827);
	this.btnder_3.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(39, -9, 1, -70))];
	this.btnder_3.cache(-21,-21,42,42);
	new cjs.ButtonHelper(this.btnder_3, 0, 1, 2, false, new lib.btnder_3(), 3);

	this.btnizq_4 = new lib.btnizq_4();
	this.btnizq_4.name = "btnizq_4";
	this.btnizq_4.parent = this;
	this.btnizq_4.setTransform(886.9,575.6,0.829,0.829);
	this.btnizq_4.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(39, -9, 1, -70))];
	this.btnizq_4.cache(-21,-21,42,42);
	new cjs.ButtonHelper(this.btnizq_4, 0, 1, 2, false, new lib.btnizq_4(), 3);

	this.btnder_4 = new lib.btnder_4();
	this.btnder_4.name = "btnder_4";
	this.btnder_4.parent = this;
	this.btnder_4.setTransform(922.3,575.6,0.828,0.827);
	this.btnder_4.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(39, -9, 1, -70))];
	this.btnder_4.cache(-21,-21,42,42);
	new cjs.ButtonHelper(this.btnder_4, 0, 1, 2, false, new lib.btnder_4(), 3);

	this.btnizq_5 = new lib.btnizq_5();
	this.btnizq_5.name = "btnizq_5";
	this.btnizq_5.parent = this;
	this.btnizq_5.setTransform(886.9,575.6,0.829,0.829);
	this.btnizq_5.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(39, -9, 1, -70))];
	this.btnizq_5.cache(-21,-21,42,42);
	new cjs.ButtonHelper(this.btnizq_5, 0, 1, 2, false, new lib.btnizq_5(), 3);

	this.btnder_5 = new lib.btnder_5();
	this.btnder_5.name = "btnder_5";
	this.btnder_5.parent = this;
	this.btnder_5.setTransform(922.3,575.6,0.828,0.827);
	this.btnder_5.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(39, -9, 1, -70))];
	this.btnder_5.cache(-21,-21,42,42);
	new cjs.ButtonHelper(this.btnder_5, 0, 1, 2, false, new lib.btnder_5(), 3);

	this.btnder_6 = new lib.btnder_5copy();
	this.btnder_6.name = "btnder_6";
	this.btnder_6.parent = this;
	this.btnder_6.setTransform(922.3,575.6,1.227,1.227,0,0,0,0.1,0.1);
	this.btnder_6.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(39, -9, 1, -70)), new cjs.ColorMatrixFilter(new cjs.ColorMatrix(39, -9, 1, -70))];
	this.btnder_6.cache(-21,-21,42,42);
	new cjs.ButtonHelper(this.btnder_6, 0, 1, 2, false, new lib.btnder_5copy(), 3);

	this.btnizq_6 = new lib.btnizq_6();
	this.btnizq_6.name = "btnizq_6";
	this.btnizq_6.parent = this;
	this.btnizq_6.setTransform(886.5,575.2,0.829,0.829);
	this.btnizq_6.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(39, -9, 1, -70)), new cjs.ColorMatrixFilter(new cjs.ColorMatrix(39, -9, 1, -70))];
	this.btnizq_6.cache(-21,-21,42,42);
	new cjs.ButtonHelper(this.btnizq_6, 0, 1, 2, false, new lib.btnizq_6(), 3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.btnder_1}]},2).to({state:[{t:this.btnder_2},{t:this.btnizq_2}]},1).to({state:[{t:this.btnder_3},{t:this.btnizq_3}]},1).to({state:[{t:this.btnder_4},{t:this.btnizq_4}]},1).to({state:[{t:this.btnder_5},{t:this.btnizq_5}]},1).to({state:[{t:this.btnizq_6},{t:this.btnder_6}]},1).wait(1));

	// player
	this.player1 = new lib.an_Video({'id': 'player1', 'src':'videos/video01.mp4', 'autoplay':true, 'controls':true, 'muted':false, 'loop':false, 'poster':'images/', 'preload':true, 'class':'video'});

	this.player1.setTransform(477,336.9,2.211,1.576,0,0,0,201.6,150.8);

	this.player2 = new lib.an_Video({'id': 'player2', 'src':'videos/video02.mp4', 'autoplay':true, 'controls':true, 'muted':false, 'loop':false, 'poster':'images/', 'preload':true, 'class':'video'});

	this.player2.setTransform(477,336.9,2.211,1.576,0,0,0,201.6,150.8);

	this.player3 = new lib.an_Video({'id': 'player3', 'src':'videos/video03.mp4', 'autoplay':true, 'controls':true, 'muted':false, 'loop':false, 'poster':'images/', 'preload':true, 'class':'video'});

	this.player3.setTransform(477,336.9,2.211,1.576,0,0,0,201.6,150.8);

	this.player4 = new lib.an_Video({'id': 'player4', 'src':'videos/video04.mp4', 'autoplay':true, 'controls':true, 'muted':false, 'loop':false, 'poster':'images/', 'preload':true, 'class':'video'});

	this.player4.setTransform(477,336.9,2.211,1.576,0,0,0,201.6,150.8);

	this.player5 = new lib.an_Video({'id': 'player5', 'src':'videos/video05.mp4', 'autoplay':true, 'controls':true, 'muted':false, 'loop':false, 'poster':'images/', 'preload':true, 'class':'video'});

	this.player5.setTransform(477,336.9,2.211,1.576,0,0,0,201.6,150.8);

	this.player6 = new lib.an_Video({'id': 'player6', 'src':'videos/video06.mp4', 'autoplay':true, 'controls':true, 'muted':false, 'loop':false, 'poster':'images/', 'preload':true, 'class':'video'});

	this.player6.setTransform(477,336.9,2.211,1.576,0,0,0,201.6,150.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.player1}]},2).to({state:[{t:this.player2}]},1).to({state:[{t:this.player3}]},1).to({state:[{t:this.player4}]},1).to({state:[{t:this.player5}]},1).to({state:[{t:this.player6}]},1).wait(1));

	// Capa_2
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("rgba(147,149,152,0.498)").s().p("EhOWAxoMAAAhjPMCctAAAMAAABjPg");
	this.shape_13.setTransform(474.1,297.7);
	this.shape_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_13).wait(2).to({_off:false},0).wait(6));

	// Capa_13
	this.bg = new lib.bg();
	this.bg.name = "bg";
	this.bg.parent = this;
	this.bg.setTransform(476.2,301.2,1,1,0,0,0,475.8,301.2);
	this.bg._off = true;

	this.timeline.addTween(cjs.Tween.get(this.bg).wait(2).to({_off:false},0).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(475,133,950.4,767);
// library properties:
lib.properties = {
	id: '7810736C09954229ACDF52B452E767AB',
	width: 950,
	height: 600,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/menu_atlas_.png?1530819035316", id:"menu_atlas_"},
		{src:"https://code.jquery.com/jquery-2.2.4.min.js?1530819035392", id:"lib/jquery-2.2.4.min.js"},
		{src:"components/sdk/anwidget.js?1530819035392", id:"sdk/anwidget.js"},
		{src:"components/video/src/video.js?1530819035392", id:"an.Video"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['7810736C09954229ACDF52B452E767AB'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}

function _updateVisibility(evt) {
	if((this.getStage() == null || this._off || this._lastAddedFrame != this.parent.currentFrame) && this._element) {
		this._element.detach();
		stage.removeEventListener('drawstart', this._updateVisibilityCbk);
		this._updateVisibilityCbk = false;
	}
}
function _handleDrawEnd(evt) {
	var props = this.getConcatenatedDisplayProps(this._props), mat = props.matrix;
	var tx1 = mat.decompose(); var sx = tx1.scaleX; var sy = tx1.scaleY;
	var dp = window.devicePixelRatio || 1; var w = this.nominalBounds.width * sx; var h = this.nominalBounds.height * sy;
	mat.tx/=dp;mat.ty/=dp; mat.a/=(dp*sx);mat.b/=(dp*sx);mat.c/=(dp*sy);mat.d/=(dp*sy);
	this._element.setProperty('transform-origin', this.regX + 'px ' + this.regY + 'px');
	var x = (mat.tx + this.regX*mat.a + this.regY*mat.c - this.regX);
	var y = (mat.ty + this.regX*mat.b + this.regY*mat.d - this.regY);
	var tx = 'matrix(' + mat.a + ',' + mat.b + ',' + mat.c + ',' + mat.d + ',' + x + ',' + y + ')';
	this._element.setProperty('transform', tx);
	this._element.setProperty('width', w);
	this._element.setProperty('height', h);
	this._element.update();
}

function _tick(evt) {
	var stage = this.getStage();
	stage&&stage.on('drawend', this._handleDrawEnd, this, true);
	if(!this._updateVisibilityCbk) {
		this._updateVisibilityCbk = stage.on('drawstart', this._updateVisibility, this, false);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;