var H5P = H5P || {};
 
H5P.GreetingCard = (function ($) {
  /**
   * Constructor function.
   */
  function C(options, id) {
    this.options = $.extend(true, {}, {
      text: 'Tema!',
      image: null
    }, options);
    this.id = id;
  };
 
  /**
   * Attach function called by H5P framework to insert H5P content into
   * page
   *
   * @param {jQuery} $container
   */
  C.prototype.attach = function ($container) {
    $container.addClass("oit-banner");
    if (this.options.image && this.options.image.path) {
      $container.append('<img class="oit-banner-image" src="' + H5P.getPath(this.options.image.path, this.id) + '">');
    }
    $container.append('<div class="oit-banner-text">' + this.options.greeting + '</div>');
  };
 
  return C;
})(H5P.jQuery);