<?php
require_once(__DIR__ . '/../config.php');
require_once($CFG->dirroot . '/my/lib.php');
require_once($CFG->dirroot . '/user/profile/lib.php');
require_once($CFG->dirroot . '/user/lib.php');
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->dirroot."/oit/lib/table.php");

$userid         = optional_param('id', 0, PARAM_INT);
$edit           = optional_param('edit', null, PARAM_BOOL);    // Turn editing on and off.
$reset          = optional_param('reset', null, PARAM_BOOL);

$PAGE->set_url('/user/profile.php', array('id' => $userid));

if (!empty($CFG->forceloginforprofiles)) {
	require_login();
	if (isguestuser()) {
		$PAGE->set_context(context_system::instance());
		echo $OUTPUT->header();
		echo $OUTPUT->confirm(get_string('guestcantaccessprofiles', 'error'),
			get_login_url(),
			$CFG->wwwroot);
		echo $OUTPUT->footer();
		die;
	}
} else if (!empty($CFG->forcelogin)) {
	require_login();
}

$userid = $userid ? $userid : $USER->id;
if ((!$user = $DB->get_record('user', array('id' => $userid))) || ($user->deleted)) {
	$PAGE->set_context(context_system::instance());
	echo $OUTPUT->header();
	if (!$user) {
		echo $OUTPUT->notification(get_string('invaliduser', 'error'));
	} else {
		echo $OUTPUT->notification(get_string('userdeleted'));
	}
	echo $OUTPUT->footer();
	die;
}


$currentuser = ($user->id == $USER->id);
$context = $usercontext = context_user::instance($userid, MUST_EXIST);

if (!user_can_view_profile($user, null, $context)) {

    // Course managers can be browsed at site level. If not forceloginforprofiles, allow access (bug #4366).
	$struser = get_string('user');
	$PAGE->set_context(context_system::instance());
    // $PAGE->set_title("$SITE->shortname: $struser");  // Do not leak the name.
	$title="$SITE->shortname: Perfil";
	$PAGE->set_title($title);
	$PAGE->set_heading($struser);
	$PAGE->set_pagelayout('mypublic');
	$PAGE->set_url('/user/profile.php', array('id' => $userid));
	$PAGE->navbar->add($struser);
	echo $OUTPUT->header();
	echo $OUTPUT->notification(get_string('usernotavailable', 'error'));
	echo $OUTPUT->footer();
	exit;
}
// Get the profile page.  Should always return something unless the database is broken.
if (!$currentpage = my_get_page($userid, MY_PAGE_PUBLIC)) {
	print_error('mymoodlesetup');
}

// $PAGE->set_context($context);
$PAGE->set_pagelayout('mypublic');
$PAGE->set_pagetype('user-profile');

// Start setting up the page.
$strpublicprofile = get_string('publicprofile');
$title="$SITE->shortname: Perfil";
$PAGE->set_title($title);

// TODO WORK OUT WHERE THE NAV BAR IS!
$renderer = $PAGE->get_renderer('core_user', 'myprofile');
echo html_writer::tag('style ',
	"div#nav-drawer{display:none!important;}
	#page.container-fluid{margin-left: 112px!important;}
	#region-main{margin: 18px 0;}
	#page-header{display:none;}");
	echo $OUTPUT->header();

	echo html_writer::start_tag('div',array('id'=>'perfil_top'));
	echo $renderer->user_picture($user,array('size'=>100));
	echo html_writer::start_tag('span',array('class'=>'oit-info'));
	echo html_writer::tag('h1',fullname($user));
	echo html_writer::tag('h4',$user->email);
	echo html_writer::tag('h4',$user->city);

	if($USER->id==$userid){
		echo html_writer::start_tag('span');
		echo html_writer::tag('a','Editar Perfil',array('href'=>"/user/edit.php?id=$user->id",'class'=>'oit-boton'));
		echo html_writer::tag('a','Cambiar contraseña',array('href'=>"/login/change_password.php?id=1",'class'=>'oit-boton'));
		echo html_writer::end_tag('span');
	}
	echo html_writer::end_tag('span');
	echo html_writer::end_tag('div');

	echo html_writer::start_tag('div',array('class'=>'userprofile'));
	$table = new grades_table('uniqueid');

	$table->set_sql("c.fullname as curso,
		ROUND(50*SUM(
		CASE WHEN gi.itemmodule='hvp'
		THEN gg.finalgrade
		ELSE 0
		END
		)/(SELECT SUM(_gi.grademax) FROM `mdl_grade_items` _gi WHERE _gi.itemmodule='hvp' AND _gi.courseid=c.id),2)  as actividades,
		ROUND(SUM(
		CASE WHEN gi.itemmodule='quiz' AND gi.itemname='Evaluación'
		THEN 50*gg.rawgrade
		ELSE 0
		END
		)) as evaluacion,
		ROUND(50*SUM(
		CASE WHEN gi.itemmodule='hvp'
		THEN gg.finalgrade
		ELSE 0
		END
		)/(SELECT SUM(_gi.grademax) FROM `mdl_grade_items` _gi WHERE _gi.itemmodule='hvp' AND _gi.courseid=c.id)+
		SUM(
		CASE WHEN gi.itemmodule='quiz' AND gi.itemname='Evaluación'
		THEN 50*gg.rawgrade
		ELSE 0
		END
	)) as puntaje", 
	"{grade_grades} gg INNER JOIN {grade_items} gi ON gi.id=gg.itemid INNER JOIN {course} c ON c.id=gi.courseid", 
	"gg.userid=$userid GROUP BY gi.courseid");

	$table->sortable(true,'puntaje',DESC);
	$table->define_baseurl("$CFG->wwwroot/oit/profile.php?id=$user->id");
	$table->out(100, true);
	echo html_writer::end_tag('div');


	echo $OUTPUT->footer();
