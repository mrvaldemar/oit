<?php

require "../config.php";
require_once($CFG->dirroot."/oit/lib/utils.php");
require_once($CFG->dirroot."/oit/lib/forms.php");

//Se obtienen los parametros necesarios
$eliminar = optional_param('erase',0, PARAM_INT);
$confirmarEliminar = optional_param('confirmerase',0, PARAM_INT);

$curso = optional_param('courseid',0, PARAM_INT);

//Configuracion de titulos y url de la pagina 
require_login();
$title="Recursos";
$PAGE->set_url('/oit/recursos.php');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$recursos=array();

//Se cargan plantillas utilizadas
$plantilla=file_get_contents("$CFG->dirroot/oit/plantillas/recursos/fila.html");
$plantillaEdit=file_get_contents("$CFG->dirroot/oit/plantillas/recursos/editar.html");
$plantillaTitulo=file_get_contents("$CFG->dirroot/oit/plantillas/recursos/titulo.html");

//Se cargan los tipos de tipos de documentos
$recursos=array();
$tiposArchivos=$DB->get_records('recurso_tipo');
foreach ($tiposArchivos as $tipoArchivo) {
	$recursos[$tipoArchivo]=array();
}

//Si se quiere eliminar un documento se elimina el registro primero, luego el archivo
if($confirmarEliminar){
	if($DB->record_exists('recurso',array('id'=>$confirmarEliminar))){
		$recurso=$DB->get_record('recurso',array('id'=>$confirmarEliminar));
		$DB->delete_records('recurso',array('id'=>$confirmarEliminar));
		$realpath=realpath("$CFG->dirroot/oit/recursos/$recurso->nombre");
	}
	if(is_writeable($realpath)){
		unlink($realpath);
	}
	header('Location: /oit/recursos.php',true,301);
	die;
}

//Por cada archivo de la carpeta de recursos se crea el objeto para renderizar segun plantilla
foreach (glob($CFG->dirroot."/oit/recursos/*") as $nombreArchivo){
	$filesize=OITUtils::formatFilesize($nombreArchivo);
	$nombreArchivo=end(explode('/', $nombreArchivo));

	$metainfo=$DB->get_record('recurso',array('nombre'=>$nombreArchivo));
	// var_dump(['id_curso'=>$curso,'id_recurso'=>$metainfo->id]);
	if(!$curso||$DB->record_exists('recurso_curso',['id_curso'=>$curso,'id_recurso'=>$metainfo->id])){
		$recursos[$metainfo!==false?$metainfo->tipoid:"sintipo"][]=array(
			'url'=>"/oit/recursos/$nombreArchivo",
			'editar'=>is_siteadmin()?OITUtils::plantillarender($plantillaEdit,array('filename'=>$nombreArchivo,'id'=>$metainfo->id)):'',
			'descripcion'=>$metainfo!==false?$metainfo->descripcion:'',
			'titulo'=>$metainfo!==false?$metainfo->titulo:explode('.', $nombreArchivo)[0],
			'filesize'=>$filesize,
			'anchor'=>$metainfo!==false?OITUtils::normalize($metainfo->titulo):'' 
		);
	}
}

//Si llega un request de archivo guardarlo en la carpeta
if ($_FILES['archivo']) {
	$target = "$CFG->dirroot/oit/recursos/".$_FILES['archivo']['name'];
	if(move_uploaded_file($_FILES['archivo']['tmp_name'], $target)){
		header('Location: /oit/recursos.php',true,301);
	}
}

$select=new recursos_form();

$select->set_data(['courseid'=>$curso]);


//Renderizar cabezal con estilos de primer nivel
echo $OUTPUT->header();
echo file_get_contents("$CFG->dirroot/oit/plantillas/primer_nivel.html");
echo $select->display();
if($eliminar){
	echo OITUtils::plantillarender(
		file_get_contents("$CFG->dirroot/oit/plantillas/recursos/confirmar.html"),
		array('id'=>$eliminar,'filtro'=>$filtro)
	);

}else{	
	//Si es administrador mostrar formulario de carga de archivo
	if(is_siteadmin()){
		echo file_get_contents("$CFG->dirroot/oit/plantillas/recursos/formulario_subida.html");
	}

	//Renderizar archivos sin tipo 
	foreach ($recursos['sintipo'] as $recurso) {
		echo OITUtils::plantillarender($plantilla,$recurso);
	}
	unset($recursos['sintipo']);
	if(!count($recursos)){
		echo html_writer::tag('h2',"No se existen recursos para este curso");
	}
	//Por cada tipo renderizar titulo luego listado de archivos
	foreach ($tiposArchivos as $tipoArchivo) {
		$titulo=array(
			'titulo'=>$tipoArchivo->nombre,
			'nombre'=>OITUtils::normalize($tipoArchivo->nombre)
		);

		if (count($recursos[$tipoArchivo->id])) {
			echo OITUtils::plantillarender($plantillaTitulo,$titulo);
			foreach ($recursos[$tipoArchivo->id] as $recurso) {
				echo OITUtils::plantillarender($plantilla,$recurso);
			}
		}
	}
}

//Renderizar footer
echo $OUTPUT->footer();