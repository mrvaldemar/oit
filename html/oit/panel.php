<?php

require "../config.php";
require_once($CFG->dirroot."/oit/lib/forms.php");
require_once($CFG->dirroot."/oit/lib/utils.php");

//Evitar cacheo de la pagina
// $PAGE->set_cacheable(false);

//Configuracion de titulos y url de la pagina 
$PAGE->set_url('/oit/panel.php');
$title="Panel";
$PAGE->set_title($title);
$PAGE->set_heading($title);

//Ids de cursos en el reporte, ordenados como se muestran en la tabla
$cursos = array(14,3,12,6,7,8,9,10,15,16);

//Se carga listado de departamentos
$departamentos=json_decode(file_get_contents($CFG->dirroot.'/oit/js/json/departamentos.json'),true);


$formularioReporte = new reportes_form($url,null,'pre','',array('id'=>'ranking_form'));

//Si hay un request, setear el filtro de departamento con el departamento buscado,
//en caso contrario usar el departamento del usuario logueado
$filtroDepartamento = optional_param('departamento', null, PARAM_TEXT);
if($filtroDepartamento===null){
	$filtroDepartamento=($request=$formularioReporte->get_data())?$departamentos[$request->tipo]:$USER->profile['departamento'];
}
$formularioReporte->set_data((object)array('tipo'=>array_search($filtroDepartamento, $departamentos)));

$tablaHTML= "";

//Se cargan html de las plantillas de la cabeza
$cabeza=file_get_contents($CFG->dirroot.'/oit/plantillas/panel/cabeza.html');

//Se cargan html de las plantillas una fila de la tabla
$plantilla=file_get_contents($CFG->dirroot.'/oit/plantillas/panel/fila.html');

//Se cargan html de la plantilla del mapa
$mapadata=json_decode(file_get_contents($CFG->dirroot.'/oit/plantillas/panel/mapadata.json'),true);

$usuariosContados=array();
$totalEstudiantes=$DB->count_records('user',array('deleted'=>0));
foreach ($departamentos as $departamento) {
	$mapadata[$departamento]['total']=count(OITUtils::getusers($departamento));
}

//Obtener reporte
$reportes=new Reporte($cursos);
$reporte=$reportes();

foreach ($cursos as $courseid) {
	//Se combinan los array con las condiciones de actividades
	$activos=array_merge($reporte[$courseid]['encurso'],$reporte[$courseid]['terminaron']['mal'],$reporte[$courseid]['terminaron']['bien']);
	foreach ($activos as $usuario) {
		//Se verifica que el usuario no halla sido contado
		if (!in_array($usuario->id,$usuariosContados)) {
			//Se suma el usuario activo
			if($mapadata[$usuario->departamento]['total']){
				$mapadata[$usuario->departamento]['value']+=100/$mapadata[$usuario->departamento]['total'];
			}
			$usuariosContados[]=$usuario->id;
		}
	}
}
$mapaPlantilla=file_get_contents($CFG->dirroot.'/oit/plantillas/panel/mapa.html');
// unset($mapadata['Nivel Central']);
// var_dump(json_encode(array_values($mapadata)),json_last_error_msg());
$mapaArray=array(
	'data'=>json_encode(array_values($mapadata)),
	'maxActivos'=>$totalEstudiantes
);

$mapa=OITUtils::plantillarender($mapaPlantilla,$mapaArray);

$reporte=$filtroDepartamento=='Todos'?$reportes():$reportes($filtroDepartamento);
foreach ($cursos as $courseid) {
	//Obtener reporte filtrado por curso y departamento, si la opcion es 'todos' enviar sin departamento

	$activos=count($reporte[$courseid]['terminaron']['bien'])+count($reporte[$courseid]['terminaron']['mal'])+count($reporte[$courseid]['encurso']);
	$inactivos=count($reporte[$courseid]['pendientes']);

	//Armar objeto de tokens para la plantilla
	$curso=array(
		'idcurso'=>$courseid,
		'nombrecurso'=>$DB->get_record('course',array('id'=>$courseid),'fullname')->fullname,
		'departamento'=>$filtroDepartamento?$filtroDepartamento:'Todos',
		'activos'=>$activos,
		'inactivos'=>$inactivos,
		'porcentajeactivos'=>intval(100*($activos/($activos+$inactivos))),
		'porcentajeinactivos'=>100-intval(100*($activos/($activos+$inactivos)))
	);

	//Crear html de tabla con la plantilla y el objeto
	$tablaHTML.=OITUtils::plantillarender($plantilla,$curso);
}


//Mostrar encabezado
echo $OUTPUT->header();
echo file_get_contents("$CFG->dirroot/oit/plantillas/primer_nivel.html");
echo html_writer::start_tag('div',array('class'=>'m-t-1'));
// Mostrar formulario
$formularioReporte->display();
echo html_writer::end_tag('div');

echo $mapa;

// Mostar tabla
echo html_writer::start_tag('table',array('id'=>'reporte','class'=>'generaltable'));
echo $cabeza;
echo html_writer::start_tag('tbody');
echo $tablaHTML;
echo html_writer::end_tag('tbody');
echo html_writer::end_tag('table');

echo file_get_contents("$CFG->dirroot/oit/plantillas/busquedausuario/buscador.html");

//Mostrar pie
echo $OUTPUT->footer();