<?php

require "../config.php";
require_once($CFG->dirroot."/oit/lib/table.php");
require_once($CFG->dirroot. '/course/lib.php');
require_once($CFG->libdir. '/coursecatlib.php');

$courseid = optional_param('id', 0, PARAM_INT);

$PAGE->set_cacheable(false);
if (empty($courseid)) {
	print_error('unspecifycourseid', 'error');
	die();
}else{
	$params = array('id' => $courseid);
}
$course = $DB->get_record('course', $params, '*', MUST_EXIST);

$PAGE->set_pagelayout('coursedescription');
$PAGE->set_course($course);

$PAGE->set_url('/oit/gradereport.php');
$title="Calificaciones";
$PAGE->set_title($title.": ".$course->fullname);
$PAGE->set_heading($title);
$download = optional_param('download', '', PARAM_ALPHA);

$table = new grades_table('uniqueid',$courseid);

if (!$table->is_downloading()) {
	echo $OUTPUT->header();
}


$table->set_sql("gi.itemname AS section,FLOOR(gg.rawgrade) as puntaje", 
	"{grade_grades} gg JOIN {grade_items} gi ON gi.id=gg.itemid", 
	"gi.courseid=$courseid AND gg.userid=$USER->id");

$table->sortable(true,'puntaje',DESC);
$table->define_baseurl("$CFG->wwwroot/oit/rankingtable.php?id=$courseid");

$table->out(10, true);

if (!$table->is_downloading()) {
	echo $OUTPUT->footer();
}