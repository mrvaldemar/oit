<?php
require "../config.php";
require_once($CFG->dirroot."/oit/lib/table.php");
require_once($CFG->dirroot."/oit/lib/utils.php");

require_login();

$userid         = optional_param('id', 0, PARAM_INT);
$userid = $userid ? $userid : $USER->id;
$context = $usercontext = context_user::instance($userid, MUST_EXIST);

$usuario=$DB->get_record('user',array('id'=>$userid,'deleted'=>0));
$usuario=OITUtils::getusers(null,$userid)[$userid];

$nombreCompleto=$usuario->firstname." ".$usuario->lastname;

$title="Perfil: $nombreCompleto";

$PAGE->set_url('/oit/perfil.php');
$PAGE->set_title($title);
// $PAGE->set_heading($title);


//Se carga el html de la plantilla del heading
$plantillaHeading=file_get_contents($CFG->dirroot.'/oit/plantillas/perfil/heading.html');

$renderer = $PAGE->get_renderer('core_user', 'myprofile');

$usuarioCabeza=array(
	'nombre'=>$nombreCompleto,
	'imagen'=>$renderer->user_picture($usuario,array('size'=>100)),
	'correo'=>$usuario->email,
	'departamento'=>$usuario->departamento,
	'ultimoAcceso'=>$usuario->lastaccess!=0?userdate($usuario->lastaccess,'%e de %B del %Y <br>%X'):
						userdate(1552608000,'%e de %B del %Y <br>%X')
);

//Crear html de heading con la plantilla y el array
$headingHTML=OITUtils::plantillarender($plantillaHeading,$usuarioCabeza);

//Armar array con los datos de los cursos
$cursos=array();
$notas=OITUtils::getgrades($userid);
foreach ($DB->get_records('course',array('format'=>'topics')) as $key => $value) {
	$diagnostico=round($notas[$key]['puntaje_diagnostico']);
	
	$lecciones=round(50*$notas[$key]['puntaje']/$notas[$key]['puntaje_max']);
	$lecciones=is_nan($lecciones)?0:$lecciones;

	$evaluacion=5*round($notas[$key]['puntaje_evaluacion']);

	$total=round(50*$notas[$key]['puntaje']/$notas[$key]['puntaje_max']+$notas[$key]['puntaje_evaluacion']);
	$total=is_nan($lecciones)?$evaluacion:$total;

	$aprobadas=$notas[$key]['aprobadas'];
	$reprobadas=$notas[$key]['reprobadas'];

	$color='gris';
	if($notas[$key]['diagnostico']){
		$color='amarillo';
		if($notas[$key]['evaluacion']&&($lecciones+$evaluacion>=60)){
			$color='verde';
		}elseif($notas[$key]['evaluacion']){
			$color='rojo';
		}
	}

	$curso=array(
		'idcurso'=>$key,
		'sortorder'=>$value->sortorder,
		'nombre'=>$value->fullname,
		'diagnostico'=>$diagnostico,
		'lecciones'=>$lecciones,
		'evaluacion'=>$evaluacion,
		'total'=>$lecciones+$evaluacion,
		'realizadas'=>$aprobadas+$reprobadas,
		'totales'=>OITUtils::getLeccionesTotales($key),
		'color'=>$color
	);

	$cursos[]=$curso;
}

$tabla=new OITTabla();
$tabla->data=$cursos;
$tabla->sort('sortorder','ASC');

$url=new moodle_url($GFC->wwwroot);
$url->param('id',$userid);

$numeroItems=20;
$cabezaCabeza=file_get_contents($CFG->dirroot.'/oit/plantillas/perfil/cabeza.html');
$plantillaCuerpo=file_get_contents($CFG->dirroot.'/oit/plantillas/perfil/fila.html');
$plantillaPaginacion=file_get_contents($CFG->dirroot.'/oit/plantillas/perfil/itempaginacion.html');

echo $OUTPUT->header();
echo html_writer::tag('style ',
	"div#nav-drawer{display:none!important;}
	#page.container-fluid{margin-left: 112px!important;}
	#page-header{display:none;}");
echo $headingHTML;

echo $tabla->getHTML($numeroItems,$url,array('class'=>'generaltable','id'=>'tabla_perfil'),$cabezaCabeza,$plantillaCuerpo,$plantillaPaginacion);

echo $OUTPUT->footer();