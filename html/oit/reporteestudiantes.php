<?php
require_once(__DIR__ . '/../config.php');
require_once($CFG->dirroot . '/lib/filestorage/file_storage.php');
require_once($CFG->dirroot."/oit/lib/utils.php");

$PAGE->set_url('/user/profile.php', array('id' => $userid));

$PAGE->set_context(context_system::instance());

$PAGE->set_title("$SITE->shortname");

$PAGE->set_heading('Reporte de estudiantes');

$PAGE->set_pagelayout('mypublic');

echo $OUTPUT->header();
$usuarios=OITUtils::getreporteusuarios();
$out="";
$plantilla=file_get_contents($CFG->dirroot.'/oit/plantillas/filareportesusuarios.html');
foreach ($usuarios as $value) {
	$out.=OITUtils::plantillarender($plantilla,$value);
}

echo html_writer::start_tag('div',array("id"=>"oit-reporte-usuarios"));
echo "<div class='oit-reporte-usuarios-item titulo'>
<div>
Usuario
</div>
<div>Ultimo acceso</div>
<div>Lecciones aprobadas</div>
<div>Cursos terminados</div>
<div>Estado</div>
</div>";
echo $out;
echo html_writer::end_tag('div');

echo $OUTPUT->footer();

// echo json_encode($usuarios,JSON_PRETTY_PRINT);

