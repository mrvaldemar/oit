<?php

require "../config.php";
require_once($CFG->dirroot."/oit/lib/rankingrenderer.php");

$courseid = optional_param('id', 0, PARAM_INT);

$PAGE->set_cacheable(false);
if (empty($courseid)) {
	print_error('unspecifycourseid', 'error');
	die();
}else{
	$params = array('id' => $courseid);
}

$course = $DB->get_record('course', $params, '*', MUST_EXIST);

$PAGE->set_pagelayout('coursedescription');
$PAGE->set_course($course);

$title="Ranking";
$PAGE->set_url('/oit/ranking.php');
$PAGE->set_title($course->fullname.": ".$title);
$PAGE->set_heading($title);

$a=new RankingRenderer();

echo $OUTPUT->header();

$a->out($courseid);

echo $OUTPUT->footer().$javascript;