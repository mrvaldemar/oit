<?php

require_once("../config.php");
require_once($CFG->dirroot. '/course/lib.php');
require_once($CFG->libdir. '/coursecatlib.php');

$courseid = optional_param('id', 0, PARAM_INT);

$PAGE->set_cacheable(false);
if (empty($courseid)) {
	print_error('unspecifycourseid', 'error');
	die();
}else{
	$params = array('id' => $courseid);
}
$course = $DB->get_record('course', $params, '*', MUST_EXIST);

$PAGE->set_pagelayout('coursedescription');
$PAGE->set_course($course);

$title="Documentos";

$PAGE->set_title($title.": ".$course->fullname);
$PAGE->set_heading($title);

$content=$course->summary;

echo $OUTPUT->header();

echo $content;
echo $OUTPUT->footer();