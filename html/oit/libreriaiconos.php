<?php
require "../config.php";


$title="Libreria de iconos";
$PAGE->set_url('/oit/libreriaiconos.php');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$iconos=json_decode(file_get_contents("$CFG->dirroot/oit/js/json/libreriaiconos.json"));
echo $OUTPUT->header();
$plantilla=file_get_contents("$CFG->dirroot/oit/plantillas/libreriaiconos/icono.html");
// var_dump("$CFG->dirroot/oit/js/json/libreriaiconos.json",file_exists("$CFG->dirroot/oit/js/json/libreriaiconos.json"));
echo html_writer::start_tag('div',['class'=>'libreria']);
foreach ($iconos as $clase => $src) {
	echo OITUtils::plantillarender($plantilla,['clase'=>$clase,'src'=>$src]);
}
echo html_writer::end_tag('div');
echo "
<style>
.libreria{
	display:flex;
	flex-wrap:wrap;
}
.libreria span {
	border:lightgray solid 1px;
    width: 175px;
    padding:15px;
    text-align: center;
	margin: 5px;
}
</style>
";
echo $OUTPUT->footer();