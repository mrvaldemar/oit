<?php

require "../config.php";
require_once($CFG->dirroot."/oit/lib/utils.php");
require_once($CFG->dirroot."/oit/lib/table.php");

//Obtener parametros necesarios
$courseid = required_param('courseid', PARAM_INT);
$filtroDepartamento = required_param('departamento', PARAM_TEXT);

//Obtener nombre del curso actual
$coursename=$DB->get_record('course',array('id'=>$courseid),'fullname')->fullname;

//Configuracion de titulos y url de la pagina 
$PAGE->set_url('/oit/paneldetalle.php');
$titulo="$coursename";
$PAGE->set_title($titulo);
$PAGE->set_heading($titulo);

//Obtener parametros GET
$PAGE->navbar->add('Panel', new moodle_url('/oit/panel.php'));
$PAGE->navbar->add('Panel', new moodle_url('/oit/panel.php'));

//Obtener reporte filtrado por curso y departamento, si la opcion es 'todos' enviar sin departamento
$reporte=$filtroDepartamento=='Todos'?OITUtils::getreporte($courseid):OITUtils::getreporte($courseid,$filtroDepartamento);

$usuarios=array();

//Objeto para renderizar imagen de perfil
$renderer = $PAGE->get_renderer('core_user', 'myprofile');

//Iterar sobre la lista de usuarios pendientes
foreach ($reporte['pendientes'] as $id=>$valores) {
// foreach ($DB->get_records_list('user','id',$reporte['pendientes']) as $id=>$valores) {
	//Armar array de tokens para la plantilla de tabla de usuarios
	$usuario=array(
		'imagen'=>$renderer->user_picture($valores,array('size'=>40)),
		'ultimo_acceso'=>array('tipo'=>'date','valor'=>$valores->lastaccess)
	);
	$usuario=array_merge($usuario,(array)$valores);

	//Insertar array en listado de usuarios
	$usuarios[]=$usuario;
}

$tabla=new OITTabla();
$tabla->data=$usuarios;

//Prepara url con parametros necesarios
$url=new moodle_url($GFC->wwwroot);
$url->param('courseid',$courseid);
$url->param('departamento',$filtroDepartamento);

//Cargar plantillas y numero de items de la tabla
$numeroItems=50;
$cabezaCabeza=file_get_contents($CFG->dirroot.'/oit/plantillas/reportedetalle/cabeza.html');
$plantillaCuerpo=file_get_contents($CFG->dirroot.'/oit/plantillas/reportedetalle/fila.html');
$plantillaPaginacion=file_get_contents($CFG->dirroot.'/oit/plantillas/reportedetalle/itempaginacion.html');

//Mostrar encabezado
echo $OUTPUT->header();

// Mostar tabla
echo $tabla->getHTML($numeroItems,$url,array('id'=>'reporte-detalle','class'=>'generaltable'),$cabezaCabeza,$plantillaCuerpo,$plantillaPaginacion);
echo html_writer::tag('small',"Mostrando usuarios no activos de $filtroDepartamento en este curso");

echo html_writer::tag('style',
	"div#nav-drawer{display:none!important;}
	#page.container-fluid{margin-left: 112px!important;}");
//Mostrar pie
echo $OUTPUT->footer();