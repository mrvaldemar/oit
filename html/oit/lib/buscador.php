<?php
// header("Content-type: application/json");
require_once('../../config.php');
require_once($CFG->dirroot."/oit/lib/utils.php");

$entrada=json_decode(file_get_contents("php://input"));
$searchstr=$entrada->termino;

$SQL="SELECT u.* ,
uid.data AS departamento
FROM {user} u
INNER JOIN {user_info_data} uid ON u.id = uid.userid  
WHERE (firstname LIKE '%$searchstr%' 
OR lastname LIKE '%$searchstr%'
OR CONCAT(firstname,' ',lastname) LIKE '%$searchstr%')
AND deleted=0";

$html='';
$renderer = $PAGE->get_renderer('core_user', 'myprofile');
$plantilla= file_get_contents("$CFG->dirroot/oit/plantillas/busquedausuario/fila.html");
$usuarios=$DB->get_records_sql($SQL);
if(!$usuarios || !$searchstr){
	$html="<tr><td><h2>No se encontraron resultados<h2></td></tr>";
}else{	
	foreach ($usuarios as $usuario) {
		try {
			$usuario=array_merge((array)$usuario,array('imagen'=>$renderer->user_picture($usuario,array('size'=>40))));
		} catch (Exception $e) {
			echo "<div class='alert alert-danger'> $e->getMessage() </div>";
		}

		$html.=OITUtils::plantillarender($plantilla,$usuario);

	} 
}

echo $html;