<?php

class OITStats{
	private $fecha;

	function __construct(){

		$this->fecha = new DateTime("now", new DateTimeZone('America/Bogota'));
	}
	private static function cloneRequest(&$data,$req){
		foreach ($req as $key => $value) {
			$data[$key]=$value;
		}
	}

	function procesarTipo3($req){
		global $USER,$DB;

		// function sendPost($data) {
		// 	$ch = curl_init();
  //   // you should put here url of your getinfo.php script
		// 	curl_setopt($ch, CURLOPT_URL, "getinfo.php");
		// 	curl_setopt($ch,  CURLOPT_RETURNTRANSFER, true); 
		// 	curl_setopt($ch, CURLOPT_POST, 1);
		// 	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		// 	$result = curl_exec ($ch); 
		// 	curl_close ($ch); 
		// 	return $result; 
		// }

		$data['xapidata']=$req['xapidata'];
		return $data;

	}

	function procesarTipo2($req){
		global $USER,$DB;
		$this::cloneRequest($data,$req);
		$cm = get_coursemodule_from_id('hvp', $req['leccionid']);
		if (!$cm) {
			$res['estado']='ERROR';
			$res['mensaje']='Modulo invalido';
			return $res;
		}

		try{
			$data['actividad']=$DB->get_record('course_sections',array('id'=>$cm->section),'name as nombre, section as id', MUST_EXIST);
		}catch(Exception $e){
			$res['estado']='ERROR';
			$res['mensaje']='Registro $cm->section no existe';
			return $res;
		}

		$data['fecha']=$this->fecha->format('m/d/Y, H:i:s');
		$data['estudiante']=array('nombre'=>$USER->firstname.' '.$USER->lastname,'id'=>$USER->id);

		return array('estado'=>'OK','data'=>$data);
	}
	function checkSolution($req){

	}
	function procesarStat($req){
		$nombreMetodo = isset($req['tipo']) ? "procesarTipo" . ucfirst($req['tipo']) : "";
		if(method_exists($this,$nombreMetodo)){
			$datos=$this->$nombreMetodo($req);
		}else{
			$datos['estado'] = "ERROR";
			$datos['data'] = "Metodo '" .$nombreMetodo."' no existe";
		}
		echo json_encode($datos);
	}
}


