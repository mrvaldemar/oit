<?php
require "$CFG->libdir/tablelib.php";

/**
 * 	Clase para manejar tablas con plantillas y arrays
 */
class OITTabla{
	
	//Array de Arrays Asociativos de la tabla, las llaves de los AA corresponden al nombre de la columna  
	public $data;

	//Numero de items de cada pagina
	private $numeroItems;
	private $columnaOrden;
	private $direccionOrden;

	function __construct($data=null,$sort=null){
		$this->data=$data;
		if(!is_null($sort)){
			$this->sort($sort);
		}
	}
	/**
	 * Compara dos elementos segun el campo valor o segun el elemento como tal dependiendo 
	 * de si es un array o no
	 * @param  any $item1 				Primer valor a comparar
	 * @param  any $item2 				Segundo valor a comparar
	 * 
	 * @return int       				-1, 0 o 1 si $item1 es respectivamente menor, igual o mayor a $item2 
	 * 
	 */
	static private function compararItems($item1,$item2){
		if(is_array($data)) return $item1['valor'] <=> $item2['valor'];
		return $item1 <=> $item2;
	}

	/**
	 * Ordena los datos de la tabla segun la columna y direccion especificada
	 * 
	 * @param  string $columna   		Nombre de la columna segun la que ordenar
	 * @param  string $direccion 		ASC para ascendente o DESC para descendente 
	 * 
	 */
	public function sort($columna,$direccion='DESC'){
		usort($this->data, function ($item1, $item2) use ($columna,$direccion) {
			if($direccion=='DESC'){
				return self::compararItems($item2[$columna],$item1[$columna]);
			}elseif ($direccion=='ASC') {
				return self::compararItems($item1[$columna],$item2[$columna]);
			}
		});
	}

	/**
	 * Setea el numero de items que van a ser visibles en cada pagina
	 * @param 	int $numeroItems 		Numero de items a mostrar
	 */
	public function setPaginacion(int $numeroItems=null){
		$this->numeroItems=$numeroItems;
	}

	/**
	 * Obtener HTML barra de paginacion
	 * @param  string 		$plantilla    	HTML con tokens {{url}}, {{num}}, {{pag}} y {{class}} de un 
	 *                                 		item de la barra de paginacion
	 * @param  int 			$paginaActual 	Pagina seleccionada actualmente
	 * @param  string 		$url          	URL de la pagina donde se ecuentra la tabla
	 * @return string               		HTML de todos los items de la barra de paginacion
	 */
	public function getPaginacionHTML($plantilla,$paginaActual,$url=""){
		if(count($this->data)<$this->numeroItems) return "";
		$numeroPaginas=ceil(count($this->data)/$this->numeroItems);
		
		$itemPaginacion=array(
			'url'=>$url,
			'num'=>"<",
			'pag'=>$paginaActual-1,
			'class'=>""
		);


		$htmlPaginacion=$paginaActual===0?"":OITUtils::plantillarender($plantilla,$itemPaginacion);
		for ($i=1; $i<=$numeroPaginas ; $i++) { 
			$itemPaginacion=array(
				'num'=>$i,
				'url'=>$url,
				'pag'=>$i-1,
				'class'=>$paginaActual+1==$i?"active":""
			);
			$htmlPaginacion.=OITUtils::plantillarender($plantilla,$itemPaginacion);	
		}

		$itemPaginacion=array(
			'url'=>$url,
			'num'=>">",
			'pag'=>$paginaActual+1,
			'class'=>""
		);

		$htmlPaginacion.=$paginaActual+1>=$numeroPaginas?"":OITUtils::plantillarender($plantilla,$itemPaginacion);

		
		return html_writer::tag('ul',$htmlPaginacion,array('class'=>'pagination'));
	}

	/**
	 * Obtener Array con los datos de la tabla paginado y ordenado
	 * 
	 * @param  integer $numeroPagina 		Numero de la pagina que se quiere obtener
	 * @return Array de Arrays asociativos  Datos de la tabla paginados y ordenados
	 */
	public function getDatos($numeroPagina=0){
		
		$paginacion=$this->numeroItems;

		$itemInicial=$numeroPagina*$this->numeroItems;

		if($itemInicial>count($this->data)){
			$itemInicial=0;

		}elseif ($itemInicial+$paginacion>count($this->data)) {
			$paginacion=count($this->data)-$itemInicial;

		}

		return array_slice($this->data,$itemInicial,$paginacion);
	}

	/**
	 * Obtener el HTML completo de la tabla
	 * 
	 * @param  int 			$numeroItems         	Numero de items a mostrar
	 * @param  string 		$url                 	URL de la pagina donde se ecuentra la tabla
	 * @param  string 		$plantillaCabeza     	HTML de plantilla para la cabeza con tokens
	 *                                        		{{sort_[nombre_campo]}} para cada uno de los campos de la 
	 *                                        		tabla
	 * @param  string 		$plantillaCuerpo     	HTML de plantilla para el cuerpo
	 * @param  string 		$plantillaPaginacion 	HTML con tokens {{url}}, {{num}}, {{pag}} y 
	 *                                        		{{class}} de un item de la barra de paginacion
	 *                                        		
	 * @return string        		             	HTML de la tabla con cabeza, cuerpo y paginacion
	 */
	public function getHTML($numeroItems,$url,$atributos,$plantillaCabeza,$plantillaCuerpo,$plantillaPaginacion){

		$sortType = optional_param('tsort','', PARAM_TEXT);
		$sortOrder = optional_param('osort','ASC', PARAM_TEXT);
		$page = optional_param('page', 0, PARAM_INT);

		if($sortType!=''){
			$this->sort($sortType,$sortOrder);
		}

		$this->setPaginacion($numeroItems);

		$url->param('tsort',$sortType);
		$url->param('osort',$sortOrder);
		$paginacion=$this->getPaginacionHTML($plantillaPaginacion,$page,(string) $url);

		$sortOrder=$sortOrder=='ASC'?'DESC':'ASC';
		$url->param('osort',$sortOrder);
		$url->param('page',$page);

		$urls=array();
		foreach (array_keys($this->data[0]) as $col) {
			$url->param('tsort',$col);
			$urls['sort_'.$col]=(string) $url;
			$urls['ico_sort_'.$col]='';
			if($sortType==$col){
				if($sortOrder=='ASC'){
					$urls['ico_sort_'.$col]=html_writer::tag('i','',array('class'=>'fa fa-caret-down'));
				}elseif($sortOrder=='DESC'){
					$urls['ico_sort_'.$col]=html_writer::tag('i','',array('class'=>'fa fa-caret-up'));
				}
			}
		}

		$cabeza=OITUtils::plantillarender($plantillaCabeza,$urls);

		$cuerpo= "";
		foreach ($this->getDatos($page) as $item) {
			foreach ($item as &$celda) {
				if (is_array($celda)) {
					switch ($celda['tipo']) {
						case 'time':
						$celda=format_time($celda['valor']);
						break;
						case 'date':
						$celda=$celda['valor']!=0?userdate($celda['valor'],'%e de %B del %Y <br>%X'):
						userdate(1552608000,'%e de %B del %Y <br>%X');
						break;

						default:
						break;
					}
				}
			}
			$cuerpo.=OITUtils::plantillarender($plantillaCuerpo,$item);
		}

		$tablaHTML= html_writer::start_tag('table',$atributos);
		$tablaHTML.= $cabeza;
		$tablaHTML.= html_writer::start_tag('tbody');
		$tablaHTML.= $cuerpo;
		$tablaHTML.= html_writer::end_tag('tbody');
		$tablaHTML.= html_writer::end_tag('table');


		$tablaHTML.= $paginacion;

		return $tablaHTML;
	} 
}