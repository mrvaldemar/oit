<?php
require_once($CFG->dirroot."/oit/lib/utils.php");
require_once($CFG->dirroot."/oit/lib/table.php");
require_once($CFG->dirroot."/oit/lib/forms.php");

require_once($CFG->dirroot. '/course/lib.php');
require_once($CFG->libdir. '/coursecatlib.php');

class RankingRenderer{

    public function out($courseid){
        global $CFG,$DB;
        $url=new moodle_url();
        $url->param('id',$courseid);
        $formularioRanking = new ranking_form($url,null,'pre','',array('id'=>'ranking_form'));
        //Se carga listado de departamentos

        //Se carga el html de la plantilla de la grafica
        $plantillaGrafica=file_get_contents($CFG->dirroot.'/oit/plantillas/ranking/pie.html');

        $estados=json_decode(file_get_contents($CFG->dirroot.'/oit/js/json/estados.json'),true);

        //Se carga listado de departamentos
        $departamentos=json_decode(file_get_contents($CFG->dirroot.'/oit/js/json/departamentos.json'),true);


        //Obtener parametro de departamento
        if($request=$formularioRanking->get_data()){
            $filtroEstado = $estados[$request->estado];
            $filtroDepartamento =$departamentos[$request->departamento];
        }else{
            $filtroEstado = optional_param('estado', null, PARAM_TEXT);
            $filtroDepartamento = optional_param('departamento', 'Todos', PARAM_TEXT);
        }


        $formularioRanking->set_data(
            array(
                'departamento'=>array_search($filtroDepartamento,$departamentos),
                'estado'=>array_search($filtroEstados,$estados)
            )
        );


        //Obtener reporte filtrado por curso y departamento, si la opcion es 'todos' enviar sin departamento
        $reportes=new Reporte($courseid);
        $reporte=$filtroDepartamento=='Todos'?$reportes()[$courseid]:$reportes($filtroDepartamento)[$courseid];
        
        //Armar objeto de datos para el json de highchart
        $dataPie=array(
            array('name'=>'Reprobaron','y'=>count($reporte['terminaron']['mal'])),
            array('name'=>'Aprobaron','y'=>count($reporte['terminaron']['bien'])),
            array('name'=>'En curso','y'=>count($reporte['encurso']))
        );

        $ranking=OITUtils::getRanking($courseid,$reporte,$filtroEstado,$filtroDepartamento);
        
        $totalEstudiantes=$DB->count_records('user',array('deleted'=>0))-1;
        $totalEstudiantes=count(OITUtils::getusers($filtroDepartamento=='Todos'?null:$filtroDepartamento));
        $activados=count($ranking);

        //Armar objeto de datos para el json de highchart
        $dataActivacion=array(
            array('name'=>'Activos','y'=>$activados),
            array('name'=>'Inactivos','y'=>$totalEstudiantes-$activados)
        );
        $url=new moodle_url($GFC->wwwroot);
        $url->param('id',$courseid);
        $url->param('departamento',$filtroDepartamento);

        //Armar array de tokens para la plantilla de grafica
        $grafica=array(
            'departamento'=>$filtroDepartamento,
            'courseid'=>$courseid,
            'data'=>json_encode($dataPie),
            'titulo'=>'"Estado de curso"',
            'estudiantes'=>$totalEstudiantes,
            'activacion'=>round(100*count($ranking)/$totalEstudiantes),
            'tituloActivacion'=>'"Activacion del curso"',
            'dataActivacion'=>json_encode($dataActivacion),
            'url'=>"?id=$courseid&departamento=$filtroDepartamento"
        );
        $url->param('estado',$filtroEstado);
        //Crear html de grafica con la plantilla y el array
        $graficaHTML.=OITUtils::plantillarender($plantillaGrafica,$grafica);


        $tablaRanking=new OITTabla();
        // var_dump($ranking);
        $tablaRanking->data=$ranking;
        $tablaRanking->sort('total');

        $numeroItems=50;

        $cabezaRanking=file_get_contents($CFG->dirroot.'/oit/plantillas/ranking/cabeza.html');
        $cuerpoRanking=file_get_contents($CFG->dirroot.'/oit/plantillas/ranking/fila.html');
        $paginacionRanking=file_get_contents($CFG->dirroot.'/oit/plantillas/ranking/itempaginacion.html');

        $tablaInactivos=new OITTabla();
        $tablaInactivos->data=OITUtils::getInactivos($reporte);
        $tablaInactivos->sort('total');

        $cabezaInactivos=file_get_contents($CFG->dirroot.'/oit/plantillas/reportedetalle/cabeza.html');
        $cuerpoInactivos=file_get_contents($CFG->dirroot.'/oit/plantillas/reportedetalle/fila.html');
        $paginacionInactivos=file_get_contents($CFG->dirroot.'/oit/plantillas/reportedetalle/itempaginacion.html');

        //Mostrar formulario
        $formularioRanking->display();

        if ($filtroEstado=='Por defecto'||$filtroEstado=='Activos'||$filtroEstado==null) {
            // Mostar grafica
            echo $graficaHTML;
        }


        if(count($ranking)!=0&&$filtroEstado!='Inactivos'){
            //Imprimir tabla
            echo $tablaRanking->getHTML($numeroItems,$url,array('id'=>'tabla_ranking','class'=>'generaltable flexible'),$cabezaRanking,$cuerpoRanking,$paginacionRanking);
        }elseif(count($tablaInactivos->data)!=0){
            echo $tablaInactivos->getHTML($numeroItems,$url,array('id'=>'reporte-detalle','class'=>'generaltable flexible'),$cabezaInactivos,$cuerpoInactivos,$paginacionInactivos);
        }
    }
}