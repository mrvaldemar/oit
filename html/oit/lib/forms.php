<?php
require_once("$CFG->libdir/formslib.php");

class reportes_form extends moodleform {

	public function definition() {
		global $CFG;

		$mform = $this->_form;


		//Cargar listado de departamentos
		$departamentos=json_decode(file_get_contents($CFG->dirroot.'/oit/js/json/departamentos.json'),true);
		//Adicionar select al formulario
		$mform->addElement('select', 'tipo', 'Departamento:',$departamentos);
		
		$buttonarray=array();
		$buttonarray[] = $mform->createElement('submit', 'submitbutton', 'Filtrar',array('id'=>7));
		$mform->addGroup($buttonarray, 'buttonar', '', ' ', false);
		unset($departamentos);

	}
}

class ranking_form extends moodleform {

	public function definition() {
		global $CFG;

		$mform = $this->_form;

		//Cargar listado de departamentos
		$departamentos=json_decode(file_get_contents($CFG->dirroot.'/oit/js/json/departamentos.json'),true);

		//Adicionar select al formulario
		$mform->addElement('select', 'departamento', 'Departamento:',$departamentos);
		unset($departamentos);

		//Cargar listado de departamentos
		$estados=json_decode(file_get_contents($CFG->dirroot.'/oit/js/json/estados.json'),true);

		$mform->addElement('select', 'estado', 'Estado:',$estados);
		unset($estados);
		$buttonarray=array();
		$buttonarray[] = $mform->createElement('submit', 'submitbutton', 'Filtrar',array('id'=>7));
		$mform->addGroup($buttonarray, 'buttonar', '', ' ', false);

	}
}

class recursos_form extends moodleform {

	public function definition() {
		global $DB;
		$mform = $this->_form; 

		$cursos=$DB->get_records('course',array('format'=>'topics'),'sortorder ASC','id,shortname');
		$opciones=[];
		$opciones[0]="Todos";
		
		foreach ($cursos as $curso) {
			$opciones[$curso->id]=$curso->shortname;
		}

		$select=$mform->addElement('select','courseid','Curso',$opciones,array('onchange'=>'this.form.submit()'));
	}
}

class recurso_item_form extends moodleform {

	public function definition() {
		global $CFG,$DB;

		$mform = $this->_form;

		$mform->addElement('text', 'titulo', 'Titulo:',$estados);

		//Cargar listado de tipos
		$tipos=array();
		foreach ($DB->get_records('recurso_tipo') as $tipo) {
			$tipos[$tipo->id]=$tipo->nombre;
		}

		//Adicionar select al formulario
		$mform->addElement('select', 'tipoid', 'Tipo:',$tipos);
		unset($tipo);
		$mform->addElement('textarea', 'descripcion', 'Descripcion:',$estados);
		
		$cursos=$DB->get_records('course',array('format'=>'topics'),'sortorder ASC','id,shortname');
		$opciones=[];
		foreach ($cursos as $curso) {
			$opciones[$curso->id]=$curso->shortname;
		}
		
		$select=$mform->addElement('select','tags','Etiquetas',$opciones,array('class'=>'select2'));
		$select->setMultiple(true);

        $mform->addRule('titulo', 'Campo requerido', 'required', null, 'client');
        // $mform->addRule('titulo', 'Campo requerido', 'required', null, 'client');
        // $mform->addRule('descripcion', 'Campo requerido', 'required', null, 'client');
		$this->add_action_buttons();

	}
}

class glosario_form extends moodleform {
	public function definition() {
		global $CFG,$DB;

		$mform = $this->_form;


		$mform->addElement('text', 'palabra', 'Palabra:',$estados);
		$mform->addElement('textarea', 'definicion', 'Definición:',$estados);
        $mform->addRule('palabra', 'Campo requerido', 'required', null, 'client');
        $mform->addRule('definicion', 'Campo requerido', 'required', null, 'client');

		$this->add_action_buttons();

	}
}

function local_filemanager_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $DB;
    if ($context->contextlevel != CONTEXT_SYSTEM) {
        return false;
    }
    require_login();
    if ($filearea != 'attachment') {
        return false;
    }
    $itemid = (int)array_shift($args);
    if ($itemid != 0) {
        return false;
    }
    $fs = get_file_storage();
    $filename = array_pop($args);
    if (empty($args)) {
        $filepath = '/';
    } else {
        $filepath = '/'.implode('/', $args).'/';
    }
    $file = $fs->get_file($context->id, 'local_filemanager', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false;
    }
    // finally send the file
    send_stored_file($file, 0, 0, true, $options); // download MUST be forced - security!
}