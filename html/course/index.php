<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lists the course categories
 *
 * @copyright 1999 Martin Dougiamas  http://dougiamas.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package course
 */
require_once("../config.php");
require_once($CFG->dirroot. '/course/lib.php');
require_once($CFG->libdir. '/coursecatlib.php');
require_once($CFG->dirroot."/oit/lib/utils.php");

$categoryid = optional_param('categoryid', 0, PARAM_INT); // Category id
$site = get_site();

if ($categoryid) {
    $PAGE->set_category_by_id($categoryid);
    $PAGE->set_url(new moodle_url('/course/index.php', array('categoryid' => $categoryid)));
    $PAGE->set_pagetype('course-index-category');
    // And the object has been loaded for us no need for another DB call
    $category = $PAGE->category;
} else {
    // Check if there is only one category, if so use that.
    if (coursecat::count_all() == 1) {
        $category = coursecat::get_default();

        $categoryid = $category->id;
        $PAGE->set_category_by_id($categoryid);
        $PAGE->set_pagetype('course-index-category');
    } else {
        $PAGE->set_context(context_system::instance());
    }

    $PAGE->set_url('/course/index.php');
}

$PAGE->set_pagelayout('coursecategory');
$courserenderer = $PAGE->get_renderer('core', 'course');

if ($CFG->forcelogin) {
    require_login();
}

if ($categoryid && !$category->visible && !has_capability('moodle/category:viewhiddencategories', $PAGE->context)) {
    throw new moodle_exception('unknowncategory');
}
$PAGE->set_title('Campus Virtual: Cursos');
$PAGE->set_heading('Inducción a Inspección, Vigilancia y Control');
$content =is_siteadmin()?$courserenderer->course_category($categoryid):null;

echo $OUTPUT->header();
echo file_get_contents("$CFG->dirroot/oit/plantillas/primer_nivel.html");

echo "<div>";
echo OITUtils::cursosrenderer();
echo "</div>";
echo "<style>";
echo ".breadcrumb-item a{pointer-events: none;cursor: default;text-decoration: none;color: black;}";
// echo ".oit-container .page-header-headings{background-color: #194575;}";

// if($USER->id!=2){

//     echo "@media(min-width:768px){";
//     echo "#page .action-menu{display:none;}";
//     echo ".region_main_settings_menu_proxy{display:none;}";
//     echo "#page.container-fluid{margin-left: 112px!important;}";
//     echo "div#nav-drawer{display:none;}";
//     echo "}";
// }
echo "</style>";
// Trigger event, course category viewed.
$eventparams = array('context' => $PAGE->context, 'objectid' => $categoryid);
$event = \core\event\course_category_viewed::create($eventparams);
$event->trigger();

echo $OUTPUT->footer();
